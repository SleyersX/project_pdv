<?php

namespace Source\Models;

use PDO;
use PDOException;


class Connection
{
    /** @var PDO */
    private static $instance;

    /** @var PDOException */
    private static $error;

    /**
     * @return PDO
     */

    public static function getInstance(): ?PDO
    {
        if (empty(self::$instance)) {
            try {
                self::$instance = new PDO(
                    DATA_CONFIG["driver"] . ":host=" . DATA_CONFIG["host"] . ";dbname=" . DATA_CONFIG["dbname"] . ";port=" . DATA_CONFIG["port"],
                    DATA_CONFIG["username"],
                    DATA_CONFIG["passwd"],
                    DATA_CONFIG["options"]
                );
            } catch (PDOException $exception) {
                self::$error = $exception;
            }
        }

        return self::$instance;
    }


    /**
     * @return PDOException|null
     */
    public static function getError(): ?PDOException
    {
        return self::$error;
    }

}