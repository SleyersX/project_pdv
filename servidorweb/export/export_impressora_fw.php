<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $fw = filter_input(INPUT_GET, 'fw', FILTER_SANITIZE_STRING);
    header( 'Content-type: application/csv' );   
    header( 'Content-Disposition: attachment; filename=export_impressora_fw'.$fw.'.csv' );   
    header( 'Content-Transfer-Encoding: binary' );
    header( 'Pragma: no-cache');

    $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
    $stmt = $pdo->prepare( 'SELECT Loja, PDV, Impressora, Firmware FROM tb_consolidado_equipamentos WHERE Firmware LIKE "%'.$fw.'%";' );   
    $stmt->execute();
    $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

    $out = fopen( 'php://output', 'w' );
    foreach ( $results as $result ) 
    {
        fputcsv( $out, $result );
    }
    fclose( $out );
?>