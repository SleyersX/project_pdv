<?php

   header( 'Content-type: application/csv' );   
   header( 'Content-Disposition: attachment; filename=export_sat_fw010000.csv' );   
   header( 'Content-Transfer-Encoding: binary' );
   header( 'Pragma: no-cache');

   $pdo = new PDO( 'mysql:host=localhost;dbname=tpvs', 'root', 'diabrasil' );
   $stmt = $pdo->prepare( 'SELECT * FROM tb_dados_loja' );   
   $stmt->execute();
   $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

   $out = fopen( 'php://output', 'w' );
   foreach ( $results as $result ) 
   {
      fputcsv( $out, $result );
   }
   fclose( $out );
 
?>