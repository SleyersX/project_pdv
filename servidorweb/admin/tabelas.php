<?php
    include_once("../security/seguranca.php");
  protegePagina();
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs | Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="admin.php">Admistrativo -
		<?php  
		  echo $_SESSION['usuarioNome'];
		  ?>
	  </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="../security/sair.php"><span data-feather="share"></span>
           Sair</a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="admin.php">
                  <span data-feather="home"></span>
                    Home<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="consultas.php">
                  <span data-feather="search"></span>
                  Consultas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="sats.php?pagina=1">
                  <span data-feather="camera"></span>
                  SATs
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="list_lojas.php?pagina=1">
                  <span data-feather="list"></span>
                  Cadastro de Lojas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="usuarios.php">
                  <span data-feather="users"></span>
                  Usuarios<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#?" data-toggle="modal" data-target="#editaSenha">
                  <span data-feather="lock"></span>
                  Alterar Senha
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="scripts.php">
                  <span data-feather="file-text"></span>
                  Scripts
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="nivel.php">
                  <span data-feather="layers"></span>
                  Niveis de Acesso
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="srv_chaves.php">
                  <span data-feather="server"></span>
                  Servidor de Chaves
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="tabelas2.php">
                  <span data-feather="grid"></span>
                  Tabelas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="control.php">
                  <span data-feather="settings"></span>
                  Painel de Controle
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <?php
              date_default_timezone_set("America/Sao_Paulo");
              setlocale(LC_ALL, 'pt_BR');
              $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
              //Obter a data atual
              $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
              $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
              
              $data['atual'] = date('Y-m-d H:i:s'); 

              //Diminuir 20 segundos 
              $data['online'] = strtotime($data['atual'] . " - 20 seconds");
              $data['online'] = date("Y-m-d H:i:s",$data['online']);
              
              //Pesquisar os ultimos usuarios online nos 20 segundo
              $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
              
              $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
              $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
              
              $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
        ?>  
            <!--<h1>Quantidade de usuários online: <span id='online'><?php echo $row_qnt_visitas['online']; ?></span></h1>-->
            
        <script src="../js/jquery-3.2.1.min.js"></script>
            
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
              //Incluir e enviar o POST para o arquivo responsável em fazer contagem
              $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
              });
            }, 10000);
        </script>
        <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
              <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                          <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                        </div>
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                          <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Confirma</button>
                    </div>
                  </div>
                </div>
              </div>
        </form> 
        <main role="main" class="col-md-4 ml-sm-auto col-lg-10 pt-4 px-4">
<?php
          //Contar o número de lojas
          $num_lojas = "SELECT count(ID) as lojas FROM tb_dados_loja";
          $result_lojas = mysqli_query($conn, $num_lojas);
          $row_qntd_lojas = mysqli_fetch_assoc($result_lojas);


          //Contar o número de versões
          $num_vesoes = "SELECT count(Versao) as versao FROM tb_dados_loja";
          $result_versao = mysqli_query($conn, $num_vesoes);
          $row_qntd_versao = mysqli_fetch_assoc($result_versao);


          $num_vesoes_3210 = "SELECT count(Versao) as versao_3210 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.32.001.10.BRA-0001' AND contador > 3";
          $result_versao_3210 = mysqli_query($conn, $num_vesoes_3210);
          $row_qntd_versao_3210 = mysqli_fetch_assoc($result_versao_3210);


          $num_vesoes_3222 = "SELECT count(Versao) as versao_3222 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.32.001.22.BRA-0001' AND contador > 3";
          $result_versao_3222 = mysqli_query($conn, $num_vesoes_3222);
          $row_qntd_versao_3222 = mysqli_fetch_assoc($result_versao_3222);


          $num_vesoes_3809 = "SELECT count(Versao) as versao_3809 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.09.BRA-0001' AND contador > 3";
          $result_versao_3809 = mysqli_query($conn, $num_vesoes_3809);
          $row_qntd_versao_3809 = mysqli_fetch_assoc($result_versao_3809);
              

          $num_vesoes_3810 = "SELECT count(Versao) as versao_3810 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.10.BRA-0001' AND contador > 3";
          $result_versao_3810 = mysqli_query($conn, $num_vesoes_3810);
          $row_qntd_versao_3810 = mysqli_fetch_assoc($result_versao_3810);

          $num_vesoes_3811 = "SELECT count(Versao) as versao_3811 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.11.BRA-0001' AND contador > 3";
          $result_versao_3811 = mysqli_query($conn, $num_vesoes_3811);
          $row_qntd_versao_3811 = mysqli_fetch_assoc($result_versao_3811);

            
          $num_vesoes_3812 = "SELECT count(Versao) as versao_3812 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.12.BRA-0001' AND contador > 3";
          $result_versao_3812 = mysqli_query($conn, $num_vesoes_3812);
          $row_qntd_versao_3812 = mysqli_fetch_assoc($result_versao_3812);

          $num_vesoes_3815 = "SELECT count(Versao) as versao_3815 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.15.BRA-0002' AND contador > 3";
          $result_versao_3815 = mysqli_query($conn, $num_vesoes_3815);
          $row_qntd_versao_3815 = mysqli_fetch_assoc($result_versao_3815);

          $num_vesoes_3816 = "SELECT count(Versao) as versao_3816 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.16.BRA-0001' AND contador > 3";
          $result_versao_3816 = mysqli_query($conn, $num_vesoes_3816);
          $row_qntd_versao_3816 = mysqli_fetch_assoc($result_versao_3816);

          $num_vesoes_4206 = "SELECT count(Versao) as versao_4206 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0006' AND contador > 3";
          $result_versao_4206 = mysqli_query($conn, $num_vesoes_4206);
          $row_qntd_versao_4206 = mysqli_fetch_assoc($result_versao_4206);

          $num_vesoes_4207 = "SELECT count(Versao) as versao_4207 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0007' AND contador > 3";
          $result_versao_4207 = mysqli_query($conn, $num_vesoes_4207);
          $row_qntd_versao_4207 = mysqli_fetch_assoc($result_versao_4207);

          $num_vesoes_4208 = "SELECT count(Versao) as versao_4208 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0008' AND contador > 3";
          $result_versao_4208 = mysqli_query($conn, $num_vesoes_4208);
          $row_qntd_versao_4208 = mysqli_fetch_assoc($result_versao_4208);

          $num_vesoes_4210 = "SELECT count(Versao) as versao_4210 FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0010' AND contador > 3";
          $result_versao_4210 = mysqli_query($conn, $num_vesoes_4210);
          $row_qntd_versao_4210 = mysqli_fetch_assoc($result_versao_4210);

          $num_k2 = "SELECT count(CPU) AS K2 FROM tb_consolidado_equipamentos WHERE CPU LIKE '%K2%'";
          $result_k2 = mysqli_query($conn, $num_k2);
          $row_qnt_k2 = mysqli_fetch_assoc($result_k2);

          $num_f2 = "SELECT count(CPU) AS F2 FROM  tb_consolidado_equipamentos WHERE CPU LIKE '%F2%'";
          $result_f2 = mysqli_query($conn, $num_f2);
          $row_qnt_f2 = mysqli_fetch_assoc($result_f2);

          $num_g41 = "SELECT count(CPU) AS G41 FROM  tb_consolidado_equipamentos WHERE CPU LIKE '%G41%'";
          $result_g41 = mysqli_query($conn, $num_g41);
          $row_qnt_g41 = mysqli_fetch_assoc($result_g41);

          $num_sat1 = "SELECT count(firmware) AS SAT_1 FROM  tb_sat WHERE firmware != '01.00.00' AND status = 'Ativo'";
          $result_sat1 = mysqli_query($conn, $num_sat1);
          $row_qnt_sat1 = mysqli_fetch_assoc($result_sat1);

          $num_sat2 = "SELECT count(firmware) AS SAT_2 FROM  tb_sat WHERE firmware = '01.00.00' AND status = 'Ativo'";
          $result_sat2 = mysqli_query($conn, $num_sat2);
          $row_qnt_sat2 = mysqli_fetch_assoc($result_sat2);

          $num_satfw_010000 = "SELECT count(firmware) AS SATFW_010000 FROM  tb_sat WHERE firmware = '01.00.00' AND status = 'Ativo'";
          $result_satfw_010000 = mysqli_query($conn, $num_satfw_010000);
          $row_qnt_satfw_010000 = mysqli_fetch_assoc($result_satfw_010000);

          $num_satfw_010100 = "SELECT count(firmware) AS SATFW_010100 FROM  tb_sat WHERE firmware = '01.01.00' AND status = 'Ativo'";
          $result_satfw_010100 = mysqli_query($conn, $num_satfw_010100);
          $row_qnt_satfw_010100 = mysqli_fetch_assoc($result_satfw_010100);

          $num_satfw_010200 = "SELECT count(firmware) AS SATFW_010200 FROM  tb_sat WHERE firmware = '01.02.00' AND status = 'Ativo'";
          $result_satfw_010200 = mysqli_query($conn, $num_satfw_010200);
          $row_qnt_satfw_010200 = mysqli_fetch_assoc($result_satfw_010200);

          $num_satfw_010300 = "SELECT count(firmware) AS SATFW_010300 FROM  tb_sat WHERE firmware = '01.03.00' AND status = 'Ativo'";
          $result_satfw_010300 = mysqli_query($conn, $num_satfw_010300);
          $row_qnt_satfw_010300 = mysqli_fetch_assoc($result_satfw_010300);

          $num_satfw_010400 = "SELECT count(firmware) AS SATFW_010400 FROM  tb_sat WHERE firmware = '01.04.00' AND status = 'Ativo'";
          $result_satfw_010400 = mysqli_query($conn, $num_satfw_010400);
          $row_qnt_satfw_010400 = mysqli_fetch_assoc($result_satfw_010400);

          $num_satfw_010401 = "SELECT count(firmware) AS SATFW_010401 FROM  tb_sat WHERE firmware = '01.04.01' AND status = 'Ativo'";
          $result_satfw_010401 = mysqli_query($conn, $num_satfw_010401);
          $row_qnt_satfw_010401 = mysqli_fetch_assoc($result_satfw_010401);

          $num_satfw_010500 = "SELECT count(firmware) AS SATFW_010500 FROM  tb_sat WHERE firmware = '01.05.00' AND status = 'Ativo'";
          $result_satfw_010500 = mysqli_query($conn, $num_satfw_010500);
          $row_qnt_satfw_010500 = mysqli_fetch_assoc($result_satfw_010500);

          $num_imp_dr700le = "SELECT COUNT(ID) AS DR700LE FROM tb_consolidado_equipamentos WHERE Impressora LIKE '%DR700 L%'";
          $result_imp_dr700le = mysqli_query($conn, $num_imp_dr700le);
          $row_qnt_imp_dr700le = mysqli_fetch_assoc($result_imp_dr700le);

          $num_imp_dr700h = "SELECT COUNT(ID) AS DR700H FROM tb_consolidado_equipamentos WHERE Impressora LIKE '%DR700 H%'";
          $result_imp_dr700h = mysqli_query($conn, $num_imp_dr700h);
          $row_qnt_imp_dr700h = mysqli_fetch_assoc($result_imp_dr700h);

          $num_imp_dr800h = "SELECT COUNT(ID) AS DR800H FROM tb_consolidado_equipamentos WHERE Impressora LIKE '%DR800 H%'";
          $result_imp_dr800h = mysqli_query($conn, $num_imp_dr800h);
          $row_qnt_imp_dr800h = mysqli_fetch_assoc($result_imp_dr800h);

          $num_imp_dr800l = "SELECT COUNT(ID) AS DR800L FROM tb_consolidado_equipamentos WHERE Impressora LIKE '%DR800 L%'";
          $result_imp_dr800l = mysqli_query($conn, $num_imp_dr800l);
          $row_qnt_imp_dr800l = mysqli_fetch_assoc($result_imp_dr800l);

          $num_impfw_012104 = "SELECT COUNT(Firmware) AS IMPFW_012104 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%01.21.04%'";
          $result_impfw_012104 = mysqli_query($conn, $num_impfw_012104);
          $row_qnt_impfw_012104 = mysqli_fetch_assoc($result_impfw_012104);

          $num_impfw_012106 = "SELECT COUNT(Firmware) AS IMPFW_012106 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%01.21.06%'";
          $result_impfw_012106 = mysqli_query($conn, $num_impfw_012106);
          $row_qnt_impfw_012106 = mysqli_fetch_assoc($result_impfw_012106);

          $num_impfw_030001 = "SELECT COUNT(Firmware) AS IMPFW_030001 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.00.01%'";
          $result_impfw_030001 = mysqli_query($conn, $num_impfw_030001);
          $row_qnt_impfw_030001 = mysqli_fetch_assoc($result_impfw_030001);

          $num_impfw_030006 = "SELECT COUNT(Firmware) AS IMPFW_030006 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.00.06%'";
          $result_impfw_030006 = mysqli_query($conn, $num_impfw_030006);
          $row_qnt_impfw_030006 = mysqli_fetch_assoc($result_impfw_030006);

          $num_impfw_030007 = "SELECT COUNT(Firmware) AS IMPFW_030007 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.00.07%'";
          $result_impfw_030007 = mysqli_query($conn, $num_impfw_030007);
          $row_qnt_impfw_030007 = mysqli_fetch_assoc($result_impfw_030007);

          $num_impfw_032000 = "SELECT COUNT(Firmware) AS IMPFW_032000 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.20.00%'";
          $result_impfw_032000 = mysqli_query($conn, $num_impfw_032000);
          $row_qnt_impfw_032000 = mysqli_fetch_assoc($result_impfw_032000);

          $num_impfw_032001 = "SELECT COUNT(Firmware) AS IMPFW_032001 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.20.01%'";
          $result_impfw_032001 = mysqli_query($conn, $num_impfw_032001);
          $row_qnt_impfw_032001 = mysqli_fetch_assoc($result_impfw_032001);

          $num_impfw_032002 = "SELECT COUNT(Firmware) AS IMPFW_032002 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.20.02%'";
          $result_impfw_032002 = mysqli_query($conn, $num_impfw_032002);
          $row_qnt_impfw_032002 = mysqli_fetch_assoc($result_impfw_032002);

          $num_impfw_032003 = "SELECT COUNT(Firmware) AS IMPFW_032003 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.20.03%'";
          $result_impfw_032003 = mysqli_query($conn, $num_impfw_032003);
          $row_qnt_impfw_032003 = mysqli_fetch_assoc($result_impfw_032003);

          $num_impfw_031301 = "SELECT COUNT(Firmware) AS IMPFW_031301 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.13.01%'";
          $result_impfw_031301 = mysqli_query($conn, $num_impfw_031301);
          $row_qnt_impfw_031301 = mysqli_fetch_assoc($result_impfw_031301);

          $num_impfw_031302 = "SELECT COUNT(Firmware) AS IMPFW_031302 FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.13.02%'";
          $result_impfw_031302 = mysqli_query($conn, $num_impfw_031302);
          $row_qnt_impfw_031302 = mysqli_fetch_assoc($result_impfw_031302);

          $num_impfw_total = "SELECT COUNT(Firmware) AS TtImpressora FROM tb_consolidado_equipamentos";
          $result_impfw_total = mysqli_query($conn, $num_impfw_total);
          $row_qnt_impfw_total = mysqli_fetch_assoc($result_impfw_total);


      ?>
  
   
  <div id="table_div_I"></div>
  <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Versões');
        data.addColumn('number', 'Quantidade');
        data.addRows([
          ['SA.32.001.10' , <?php echo $row_qntd_versao_3210['versao_3210']; ?> ],
          ['SA.38.001.09' , <?php echo $row_qntd_versao_3809['versao_3809']; ?> ],
          ['SA.38.001.16' , <?php echo $row_qntd_versao_3816['versao_3816']; ?> ],
          ['SA.42.002.06' , <?php echo $row_qntd_versao_4206['versao_4206']; ?> ],
          ['SA.42.002.07' , <?php echo $row_qntd_versao_4207['versao_4207']; ?> ],
          ['SA.42.002.08' , <?php echo $row_qntd_versao_4208['versao_4208']; ?> ],
          ['SA.42.002.10' , <?php echo $row_qntd_versao_4210['versao_4210']; ?> ],
          ['Total de Lojas', <?php echo $row_qntd_lojas['lojas']; ?>            ],
     ]); 
    var options = {
      showRowNumber: true, 
      width: '100%', 
      height: '100%'
    }
        var table = new google.visualization.Table(document.getElementById('table_div_I'));
    
    function selectHandler() {
          var selectedItem =table.getSelection()[0];
          if (selectedItem) {
            var value = data.getValue(selectedItem.row, 0);
           if ( value == "SA.32.001.10"){
          $("#modal_dados_loja3210").modal();
        }
        if ( value == "SA.32.001.22"){
          $("#modal_dados_loja322").modal();
        }
        if ( value == "SA.38.001.09"){
          $("#modal_dados_loja3809").modal();
        }
        if ( value == "Total de Lojas"){
          $("#modal_dados_loja_total").modal();
        }
        if ( value == "SA.42.002.06"){
          $("#modal_dados_loja4206").modal();
        }
        if ( value == "SA.42.002.07"){
          $("#modal_dados_loja4207").modal();
        }
        if ( value == "SA.42.002.08"){
          $("#modal_dados_loja4208").modal();
        }
        if ( value == "SA.42.002.10"){
          $("#modal_dados_loja4210").modal();
        }
        if ( value == "SA.38.001.16"){
          $("#modal_dados_loja3816").modal();
        }
      }
    }
      google.visualization.events.addListener(table, 'select', selectHandler);
    table.draw(data, options);
    
      }
 </script>
<div class="page-header">
<script type="text/javascript" src="../js/loader.js"></script>
<div id="donut_single_I" style="width: 800px; height: 400px;"></div>
</div>
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartI);


      function drawChartI() {

        var data = google.visualization.arrayToDataTable([
          ['Versão', 'Quantidade'],
          ['SA.32.001.10' , <?php echo $row_qntd_versao_3210['versao_3210']; ?> ],
          ['SA.32.003.22' , <?php echo $row_qntd_versao_3222['versao_3222']; ?> ],
          ['SA.38.001.09' , <?php echo $row_qntd_versao_3809['versao_3809']; ?> ],
          ['SA.38.001.16' , <?php echo $row_qntd_versao_3816['versao_3816']; ?> ],
          ['SA.42.002.06' , <?php echo $row_qntd_versao_4206['versao_4206']; ?> ],
          ['SA.42.002.07' , <?php echo $row_qntd_versao_4207['versao_4207']; ?> ],
          ['SA.42.002.08' , <?php echo $row_qntd_versao_4208['versao_4208']; ?> ],
          ['SA.42.002.10' , <?php echo $row_qntd_versao_4210['versao_4210']; ?> ],          
        ]);

        var options = {
          pieHole: 0.4,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'Versão',
          sliceVisibilityThreshold:0,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_I'));
    function selectHandler() {
          var selectedItem =chart.getSelection()[0];
          if (selectedItem) {
            var value = data.getValue(selectedItem.row, 0);
            if ( value == "SA.32.001.10"){
          $("#modal_dados_loja3210").modal();
          }
          if ( value == "SA.32.001.22"){
            $("#modal_dados_loja322").modal();
          }
          if ( value == "SA.38.001.09"){
            $("#modal_dados_loja3809").modal();
          }
          if ( value == "SA.38.001.16"){
            $("#modal_dados_loja3816").modal();
          }
          if ( value == "SA.42.002.06"){
            $("#modal_dados_loja4206").modal();
          }
           if ( value == "SA.42.002.07"){
            $("#modal_dados_loja4207").modal();
          }
          if ( value == "SA.42.002.08"){
            $("#modal_dados_loja4208").modal();
          }
          if ( value == "SA.42.002.10"){
            $("#modal_dados_loja4210").modal();
          }
        }
      }
      google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data,options);
      }
    </script>
    <div class="page-header">
    <script type="text/javascript" src="../js/loader.js"></script>
    <table class="table table-striped table-sm">
    </div>
    <div class="row">
    <table class="table">
    <thead>
    <tr>
      <th>CPU F2</th>
      <th>CPU G41</th>
      <th>CPU K2</th>
    <tbody>
      <tr>
       <td><span name="qntd_f2"><?php echo $row_qnt_f2['F2']; ?></span></td>
       <td><span name="qntd_g41"><?php echo $row_qnt_g41['G41']; ?></span></td>
       <td><span name="qntd_k2"><?php echo $row_qnt_k2['K2']; ?></span></td>
      </tr>
      </tbody>
      </table>
      <div id="donut_single_II" style="width: 800px; height: 400px;"></div>
      </div>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartII);


      function drawChartII() {

        var data = google.visualization.arrayToDataTable([
          ['CPU', 'Quantidade'],
          ['F2',   <?php echo $row_qnt_f2['F2']; ?> ],
          ['G41',  <?php echo $row_qnt_g41['G41']; ?> ],
          ['K2',  <?php echo $row_qnt_k2['K2']; ?> ],
        ]);

        var options = {
          pieHole: 0.4,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'CPU',
          sliceVisibilityThreshold:0,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_II'));
        chart.draw(data,options);
      }
   
    </script>

  
    <div class="page-header">
    <script type="text/javascript" src="../js/loader.js"></script>
    <table class="table table-striped table-sm">
    </div>
    <div class="row">
    <table class="table">
    <thead>
    <tr>
      <th>SAT-1.0</th>
      <th>SAT-2.0</th>
    <tbody>
      <tr>
       <td><span name="qntd_f2"><?php echo $row_qnt_sat1['SAT_1']; ?></span></td>
       <td><span name="qntd_g41"><?php echo $row_qnt_sat2['SAT_2']; ?></span></td>
      </tr>
      </tbody>
      </table>
   <div class="row">
   <div class="col-sm-6" id="donut_single_III" style="width: 800px; height: 400px;"></div>
   <div class="col-sm-6" id="donut_single_IV" style="width: 800px; height: 400px;"></div>
      </div>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartIII);


      function drawChartIII() {

        var data = google.visualization.arrayToDataTable([
          ['Modelo', 'Quantidade'],
          ['SAT-1.0', <?php echo $row_qnt_sat1['SAT_1']; ?> ],
          ['SAT-2.0', <?php echo $row_qnt_sat2['SAT_2']; ?> ],

        ]);

        var options = {
          pieHole: 0.4,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'SAT',
          sliceVisibilityThreshold:0,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_III'));
        chart.draw(data,options);
      }
    </script>
  <div id="table_div"></div>
  <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Firmware');
        data.addColumn('number', 'Quantidade');
        data.addRows([
          ['01.00.00', <?php echo $row_qnt_satfw_010000['SATFW_010000']; ?> ],
          ['01.01.00', <?php echo $row_qnt_satfw_010100['SATFW_010100']; ?> ],
          ['01.02.00', <?php echo $row_qnt_satfw_010200['SATFW_010200']; ?> ],
          ['01.03.00', <?php echo $row_qnt_satfw_010300['SATFW_010300']; ?> ],
          ['01.04.00', <?php echo $row_qnt_satfw_010400['SATFW_010400']; ?> ],  
          ['01.04.01', <?php echo $row_qnt_satfw_010401['SATFW_010401']; ?> ],
          ['01.05.00', <?php echo $row_qnt_satfw_010500['SATFW_010500']; ?> ],
        ]);
    var options = {
      showRowNumber: true, 
      width: '100%', 
      height: '100%'
    }
        var table = new google.visualization.Table(document.getElementById('table_div'));
    
    function selectHandler() {
          var selectedItem =table.getSelection()[0];
          if (selectedItem) {
            var value = data.getValue(selectedItem.row, 0);
            if ( value == "01.00.00"){
            $("#modal_sat_010000").modal();
            }
            if ( value == "01.01.00"){
              $("#modal_sat_010100").modal();
            }
            if ( value == "01.02.00"){
              $("#modal_sat_010200").modal();
            }
            if ( value == "01.03.00"){
              $("#modal_sat_010300").modal();
            }
            if ( value == "01.04.00"){
              $("#modal_sat_010400").modal();
            }
            if ( value == "01.04.01"){
              $("#modal_sat_010401").modal();
            }
            if( value == "01.05.00"){
              $("#modal_sat_010500").modal();
            }
        }
    }
      google.visualization.events.addListener(table, 'select', selectHandler);
    table.draw(data, options);
    
      }
    </script>
   
    </div>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartIV);


      function drawChartIV() {

        var data = google.visualization.arrayToDataTable([
          ['Firmware', 'Quantidade'],
          ['01.00.00', <?php echo $row_qnt_satfw_010000['SATFW_010000']; ?> ],
          ['01.01.00', <?php echo $row_qnt_satfw_010100['SATFW_010100']; ?> ],
          ['01.02.00', <?php echo $row_qnt_satfw_010200['SATFW_010200']; ?> ],
          ['01.03.00', <?php echo $row_qnt_satfw_010300['SATFW_010300']; ?> ],
          ['01.04.00', <?php echo $row_qnt_satfw_010400['SATFW_010400']; ?> ],
          ['01.04.01', <?php echo $row_qnt_satfw_010401['SATFW_010401']; ?> ],
          ['01.05.00', <?php echo $row_qnt_satfw_010500['SATFW_010500']; ?> ],
        ]);

        var options = {
          pieHole: 0.4,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'SAT',
          sliceVisibilityThreshold:0,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_IV'));
    
    function selectHandler() {
      var selectedItem = chart.getSelection()[0];
      if (selectedItem) {
        var value = data.getValue(selectedItem.row, 0);
        if ( value == "01.00.00"){
          $("#modal_sat_010000").modal();
        }
        if ( value == "01.01.00"){
          $("#modal_sat_010100").modal();
        }
        if ( value == "01.02.00"){
          $("#modal_sat_010200").modal();
        }
        if ( value == "01.03.00"){
          $("#modal_sat_010300").modal();
        }
        if ( value == "01.04.00"){
          $("#modal_sat_010400").modal();
        }
        if ( value == "01.04.00"){
          $("#modal_sat_010400").modal();
        }
        if ( value == "01.04.01"){
          $("#modal_sat_010401").modal();
        }
        if( value == "01.05.00"){
          $("#modal_sat_010500").modal();
        }
      }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data,options);
      }
    </script>
  </br></br></br>
    <div class="row">
  <div class="col-sm-8" id="top_x_div" style="width: 700px; height: 600px;"></div>
        <?php
        $num_dataIII = "SELECT COUNT(Id) AS TOTAL_SATIII FROM tb_sat WHERE data_fim_ativacao > '2020-01-01'AND data_fim_ativacao < '2020-12-31'";
        $result_datIII = mysqli_query($conn, $num_dataIII);
        $row_datIII = mysqli_fetch_assoc($result_datIII);
        
        $num_data = "SELECT COUNT(Id) AS TOTAL_SAT FROM tb_sat WHERE data_fim_ativacao > '2021-01-01'AND data_fim_ativacao < '2021-12-31'";
        $result_dat = mysqli_query($conn, $num_data);
        $row_dat = mysqli_fetch_assoc($result_dat);
          
        $num_dataI = "SELECT COUNT(Id) AS TOTAL_SATI FROM tb_sat WHERE data_fim_ativacao > '2022-01-01'AND data_fim_ativacao < '2022-12-31'";
        $result_datI = mysqli_query($conn, $num_dataI);
        $row_datI = mysqli_fetch_assoc($result_datI);
        
        $num_dataII = "SELECT COUNT(Id) AS TOTAL_SATII FROM tb_sat WHERE data_fim_ativacao > '2023-01-01'AND data_fim_ativacao < '2023-12-31'";
        $result_datII = mysqli_query($conn, $num_dataII);
        $row_datII = mysqli_fetch_assoc($result_datII);
      ?>
    
<script type="text/javascript" src="js/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Move', 'Total'],
      ["Vencimento 2020", <?php echo $row_datIII['TOTAL_SATIII'];?>],
          ["Vencimento 2021", <?php echo $row_dat['TOTAL_SAT'];?>],
          ["Vencimento 2022", <?php echo $row_datI['TOTAL_SATI'];?>],
          ["Vencimento 2023", <?php echo $row_datII['TOTAL_SATII'];?>],
        ]);

        var options = {
          width: 700,
          legend: { position: 'none' },
          chart: {
            title: 'Quantidade de SATs próximo do vencimento',
            subtitle: 'Vencimeto: Do certificado de ativação do SAT' },
          axes: {
            x: {
              0: { side: 'top', label: ''} // Top x-axis.
            }
          },
          bar: { groupWidth: "50%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        // Convert the Classic options to Material options.
    function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var value = data.getValue(selectedItem.row, 0);
            if ( value == '2020'){
        $("#modal_sat_venc_2020").modal();
      }
      if ( value == '2021'){
        $("#modal_sat_venc_2021").modal();
      }
      if ( value == '2022'){
        $("#modal_sat_venc_2022").modal();
      }
      if ( value == '2023'){
        $("#modal_sat_venc_2023").modal();
      }
          }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
  <div class="col-sm-4"></br></br></br></br></br></br></br></br></br></br></br></br></br></br>
  <div id="table_divI"></div>
  <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Ano');
        data.addColumn('number', 'Quantidade');
        data.addRows([
      ['2020', <?php echo $row_datIII['TOTAL_SATIII'];?>],
          ['2021',  <?php echo $row_dat['TOTAL_SAT'];?> ],
          ['2022',  <?php echo $row_datI['TOTAL_SATI'];?> ],
          ['2023',  <?php echo $row_datII['TOTAL_SATII'];?> ],
        ]);
    var options = {
      showRowNumber:true,
      width: '100%',
      height: '100%',
    }
        var table = new google.visualization.Table(document.getElementById('table_divI'));
    
    function selectHandler() {
          var selectedItem =table.getSelection()[0];
          if (selectedItem) {
            var value = data.getValue(selectedItem.row, 0);
            if ( value == '2020'){
        $("#modal_sat_venc_2020").modal();
      }
      if ( value == '2021'){
        $("#modal_sat_venc_2021").modal();
      }
      if ( value == '2022'){
        $("#modal_sat_venc_2022").modal();
      }
      if ( value == '2023'){
        $("#modal_sat_venc_2023").modal();
      }
          }
    }
      google.visualization.events.addListener(table, 'select', selectHandler);
          table.draw(data, options);
    
        //table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }
    </script>
  </div>
  </div>
  </br></br></br>
  <div class="page-header">
  <div class="row">
    <script type="text/javascript" src="../js/loader.js"></script>
    <div class="col-sm-8" id="donut_single_V" style="width: 800px; height: 400px;"></div>
  
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartV);


      function drawChartV() {

        var data = google.visualization.arrayToDataTable([
          ['Modelo Impressora', 'Quantidade'],
          ['DR700 L-e', <?php echo $row_qnt_imp_dr700le['DR700LE']; ?> ],
          ['DR700 H-e', <?php echo $row_qnt_imp_dr700h['DR700H']; ?> ],
          ['DR800 L', <?php echo $row_qnt_imp_dr800h['DR800H']; ?> ],
          ['DR800 H-e', <?php echo $row_qnt_imp_dr800l['DR800L']; ?> ],
        ]);

        var options = {
          pieHole: 0.4,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'Impressora',
          sliceVisibilityThreshold:0,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single_V'));
        chart.draw(data,options);
      }
  </script>
  <div class="col-sm-4" id="table_divII"></div>
  <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Modelo');
        data.addColumn('number', 'Quantidade');
        data.addRows([
          ['DR700 Le', <?php echo $row_qnt_imp_dr700le['DR700LE']; ?>],
          ['DR700 H', <?php echo $row_qnt_imp_dr700h['DR700H']; ?> ],
          ['DR800 H', <?php echo $row_qnt_imp_dr800h['DR800H']; ?> ],
          ['DR800 L', <?php echo $row_qnt_imp_dr800l['DR800L']; ?> ],
        ]);
    var options = {
      showRowNumber:true,
      width: '90%',
      height: '50%',
    }
        var table = new google.visualization.Table(document.getElementById('table_divII'));
    
        table.draw(data, options);
    
        //table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }
    </script>
  </div>
  <div class="col-sm-4" id="table_divIII"></div>
  <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTableIV);

      function drawTableIV() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Firmware');
        data.addColumn('number', 'Quantidade');
        data.addRows([
          ['01.21.04', <?php echo $row_qnt_impfw_012104['IMPFW_012104']; ?>],
          ['01.21.06', <?php echo $row_qnt_impfw_012106['IMPFW_012106']; ?>],
          ['03.00.01', <?php echo $row_qnt_impfw_030001['IMPFW_030001']; ?>],
          ['03.00.06', <?php echo $row_qnt_impfw_030006['IMPFW_030006']; ?>],
          ['03.00.07', <?php echo $row_qnt_impfw_030007['IMPFW_030007']; ?>],
          ['03.13.01', <?php echo $row_qnt_impfw_031301['IMPFW_031301']; ?>],
          ['03.13.02', <?php echo $row_qnt_impfw_031302['IMPFW_031302']; ?>],
          ['03.20.00', <?php echo $row_qnt_impfw_032000['IMPFW_032000']; ?>],
          ['03.20.01', <?php echo $row_qnt_impfw_032001['IMPFW_032001']; ?>],
          ['03.20.02', <?php echo $row_qnt_impfw_032002['IMPFW_032002']; ?>],
          ['03.20.03', <?php echo $row_qnt_impfw_032003['IMPFW_032003']; ?>],
          ['Total',    <?php echo $row_qnt_impfw_total['TtImpressora'];  ?>],
        ]);
        var options = {
          showRowNumber:true,
          width: '90%',
          height: '50%',
        }

        var table = new google.visualization.Table(document.getElementById('table_divIII'));
     function selectHandler() {
          var selectedItem =table.getSelection()[0];
          if (selectedItem) {
            var value = data.getValue(selectedItem.row, 0);
            if ( value == '01.21.04'){
                $("#modal_fw_012104").modal();
            }
             if ( value == '01.21.06'){
                $("#modal_fw_012106").modal();
            }
            if ( value == '03.00.01'){
                $("#modal_fw_030001").modal();
            }
            if ( value == '03.00.06'){
                $("#modal_fw_030006").modal();
            }
            if ( value == '03.00.07'){
                $("#modal_fw_030007").modal();
            }
            if ( value == '03.13.01'){
                $("#modal_fw_031301").modal();
            }
            if ( value == '03.13.02'){
                $("#modal_fw_031302").modal();
            }
            if ( value == '03.20.00'){
                $("#modal_fw_032000").modal();
            }
            if ( value == '03.20.01'){
                $("#modal_fw_032001").modal();
            }
            if ( value == '03.20.02'){
                $("#modal_fw_032002").modal();
            }
            if ( value == '03.20.03'){
                $("#modal_fw_032003").modal();
            }
            if ( value == 'Total'){
                $("#modal_fw_total").modal();
            }
          }
      }

      google.visualization.events.addListener(table, 'select', selectHandler);
      table.draw(data, options);
    
        //table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }

    </script>    
  </div>
</div>
</main>
</div>
</div>
<!-- Impressora FW 01.21.04  -->
<div class="modal fade" id="modal_fw_012104" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR700 Firmware 01.21.04</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=01.21.04"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw012104 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%01.21.04%'";
            $query_impfw012104 = mysqli_query($conn, $sql_impfw012104);
            echo "<tbody>";
            while ( $result_impfw012104 = mysqli_fetch_array($query_impfw012104))
            {
              $loja = $result_impfw012104['Loja'];
              $caixa = $result_impfw012104['PDV'];
              $impressora = $result_impfw012104['Impressora'];
              $firmware = $result_impfw012104['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>  
<!-- Impressora FW 01.21.06  -->
<div class="modal fade" id="modal_fw_012106" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR700 Firmware 01.21.06</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=01.21.06"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw012106 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%01.21.06%'";
            $query_impfw012106 = mysqli_query($conn, $sql_impfw012106);
            echo "<tbody>";
            while ( $result_impfw012106 = mysqli_fetch_array($query_impfw012106))
            {
              $loja = $result_impfw012106['Loja'];
              $caixa = $result_impfw012106['PDV'];
              $impressora = $result_impfw012106['Impressora'];
              $firmware = $result_impfw012106['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.00.01  -->
<div class="modal fade" id="modal_fw_030001" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.00.01</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.00.01"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw030001 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.00.01%'";
            $query_impfw030001 = mysqli_query($conn, $sql_impfw030001);
            echo "<tbody>";
            while ( $result_impfw030001 = mysqli_fetch_array($query_impfw030001))
            {
              $loja = $result_impfw030001['Loja'];
              $caixa = $result_impfw030001['PDV'];
              $impressora = $result_impfw030001['Impressora'];
              $firmware = $result_impfw030001['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.00.06  -->
<div class="modal fade" id="modal_fw_030006" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.00.06</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.00.06"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw030006 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.00.06%'";
            $query_impfw030006 = mysqli_query($conn, $sql_impfw030006);
            echo "<tbody>";
            while ( $result_impfw030006 = mysqli_fetch_array($query_impfw030006))
            {
              $loja = $result_impfw030006['Loja'];
              $caixa = $result_impfw030006['PDV'];
              $impressora = $result_impfw030006['Impressora'];
              $firmware = $result_impfw030006['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.00.07  -->
<div class="modal fade" id="modal_fw_030007" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.00.07</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.00.07"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw030007 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.00.07%'";
            $query_impfw030007 = mysqli_query($conn, $sql_impfw030007);
            echo "<tbody>";
            while ( $result_impfw030007 = mysqli_fetch_array($query_impfw030007))
            {
              $loja = $result_impfw030007['Loja'];
              $caixa = $result_impfw030007['PDV'];
              $impressora = $result_impfw030007['Impressora'];
              $firmware = $result_impfw030007['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.13.01  -->
<div class="modal fade" id="modal_fw_031301" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.13.01</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.13.01"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw031301 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.13.01%'";
            $query_impfw031301 = mysqli_query($conn, $sql_impfw031301);
            echo "<tbody>";
            while ( $result_impfw031301 = mysqli_fetch_array($query_impfw031301))
            {
              $loja = $result_impfw031301['Loja'];
              $caixa = $result_impfw031301['PDV'];
              $impressora = $result_impfw031301['Impressora'];
              $firmware = $result_impfw031301['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.13.02  -->
<div class="modal fade" id="modal_fw_031302" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.13.02</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.13.02"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw031302 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.13.02%'";
            $query_impfw031302 = mysqli_query($conn, $sql_impfw031302);
            echo "<tbody>";
            while ( $result_impfw031302 = mysqli_fetch_array($query_impfw031302))
            {
              $loja = $result_impfw031302['Loja'];
              $caixa = $result_impfw031302['PDV'];
              $impressora = $result_impfw031302['Impressora'];
              $firmware = $result_impfw031302['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.20.00  -->
<div class="modal fade" id="modal_fw_032000" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.20.00</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.20.00"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw032000 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.20.00%'";
            $query_impfw032000 = mysqli_query($conn, $sql_impfw032000);
            echo "<tbody>";
            while ( $result_impfw032000 = mysqli_fetch_array($query_impfw032000))
            {
              $loja = $result_impfw032000['Loja'];
              $caixa = $result_impfw032000['PDV'];
              $impressora = $result_impfw032000['Impressora'];
              $firmware = $result_impfw032000['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.20.01  -->
<div class="modal fade" id="modal_fw_032001" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.20.01</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.20.01"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw032001 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.20.01%'";
            $query_impfw032001 = mysqli_query($conn, $sql_impfw032001);
            echo "<tbody>";
            while ( $result_impfw032001 = mysqli_fetch_array($query_impfw032001))
            {
              $loja = $result_impfw032001['Loja'];
              $caixa = $result_impfw032001['PDV'];
              $impressora = $result_impfw032001['Impressora'];
              $firmware = $result_impfw032001['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW 03.20.02  -->
<div class="modal fade" id="modal_fw_032002" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR800 Firmware 03.20.02</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=03.20.02"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfw032002 = "SELECT * FROM tb_consolidado_equipamentos WHERE Firmware LIKE '%03.20.02%'";
            $query_impfw032002 = mysqli_query($conn, $sql_impfw032002);
            echo "<tbody>";
            while ( $result_impfw032002 = mysqli_fetch_array($query_impfw032002))
            {
              $loja = $result_impfw032002['Loja'];
              $caixa = $result_impfw032002['PDV'];
              $impressora = $result_impfw032002['Impressora'];
              $firmware = $result_impfw032002['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Impressora FW Total  -->
<div class="modal fade" id="modal_fw_total" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DR700/DR800 Listado Firmware</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_impressora_fw.php?fw=total"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Impressora</th>
            <th>Firmware</th>
          </tr>
          </thead>
          <?php
            $sql_impfwtotal = "SELECT * FROM tb_consolidado_equipamentos";
            $query_impfwtotal = mysqli_query($conn, $sql_impfwtotal);
            echo "<tbody>";
            while ( $result_impfwtotal = mysqli_fetch_array($query_impfwtotal))
            {
              $loja = $result_impfwtotal['Loja'];
              $caixa = $result_impfwtotal['PDV'];
              $impressora = $result_impfwtotal['Impressora'];
              $firmware = $result_impfwtotal['Firmware']; 
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$impressora."</td>";
              echo "<td>".$firmware."</td>";
              echo "</tr>";
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal SAT 01.00.00 -->
<div class="modal fade" id="modal_sat_010000" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DSAT-2.0 Firmware 01.00.00</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_fw.php?fw=01.00.00"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php
            $sql_sat010000 = "SELECT * FROM tb_sat WHERE firmware = '01.00.00'";
            $query_sat010000 = mysqli_query($conn, $sql_sat010000);
            while ( $result_sat010000 = mysqli_fetch_array($query_sat010000))
            {
              $loja = $result_sat010000['loja'];
              $caixa = $result_sat010000['caixa'];
              $sat = $result_sat010000['sat'];
              $status = $result_sat010000['status'];
              echo "<tbody>";
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "</tr>";
              echo "</tbody>";
            }
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>  
<!-- Modal SAT 01.01.00 -->
<div class="modal fade" id="modal_sat_010100" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DSAT-1.0 Firmware 01.01.00</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_fw.php?fw=01.01.00"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php
            $sql_sat010100 = "SELECT * FROM tb_sat WHERE firmware = '01.01.00' ORDER BY loja";
            $query_sat010100 = mysqli_query($conn, $sql_sat010100);
            echo "<tbody>";
            while ( $result_sat010100 = mysqli_fetch_array($query_sat010100))
            {
              $loja = $result_sat010100['loja'];
              $caixa = $result_sat010100['caixa'];
              $sat = $result_sat010100['sat'];
              $status = $result_sat010100['status'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "</tr>";
            }
            echo "</tbody>"
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>  
<!-- Modal SAT 01.02.00 -->
<div class="modal fade" id="modal_sat_010200" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DSAT-1.0 Firmware 01.02.00</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_fw.php?fw=01.02.00"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php
            $sql_sat010200 = "SELECT * FROM tb_sat WHERE firmware = '01.02.00' ORDER BY loja";
            $query_sat010200 = mysqli_query($conn, $sql_sat010200);
            echo "<tbody>";
            while ( $result_sat010200 = mysqli_fetch_array($query_sat010200))
            {
              $loja = $result_sat010200['loja'];
              $caixa = $result_sat010200['caixa'];
              $sat = $result_sat010200['sat'];
              $status = $result_sat010200['status'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>  
<!-- Modal SAT 01.03.00 -->
<div class="modal fade" id="modal_sat_010300" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DSAT-1.0 Firmware 01.03.00</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_fw.php?fw=01.03.00"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php
            $sql_sat010300 = "SELECT * FROM tb_sat WHERE firmware = '01.03.00' ORDER BY loja";
            $query_sat010300 = mysqli_query($conn, $sql_sat010300);
            echo "<tbody>";
            while ( $result_sat010300 = mysqli_fetch_array($query_sat010300))
            {
              $loja = $result_sat010300['loja'];
              $caixa = $result_sat010300['caixa'];
              $sat = $result_sat010300['sat'];
              $status = $result_sat010300['status'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>  
<!-- Modal SAT 01.04.00 -->
<div class="modal fade" id="modal_sat_010400" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DSAT-1.0 Firmware 01.04.00</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_fw.php?fw=01.04.00"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php
            $sql_sat010400 = "SELECT * FROM tb_sat WHERE firmware = '01.04.00' ORDER BY loja";
            $query_sat010400 = mysqli_query($conn, $sql_sat010400);
            echo "<tbody>";
            while ( $result_sat010400 = mysqli_fetch_array($query_sat010400))
            {
              $loja = $result_sat010400['loja'];
              $caixa = $result_sat010400['caixa'];
              $sat = $result_sat010400['sat'];
              $status = $result_sat010400['status'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal SAT 01.04.01 -->
<div class="modal fade" id="modal_sat_010401" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DSAT-1.0 Firmware 01.04.01</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_fw.php?fw=01.04.01"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php
            $sql_sat010401 = "SELECT * FROM tb_sat WHERE firmware = '01.04.01' ORDER BY loja";
            $query_sat010401 = mysqli_query($conn, $sql_sat010401);
            echo "<tbody>";
            while ( $result_sat010401 = mysqli_fetch_array($query_sat010401))
            {
              $loja = $result_sat010401['loja'];
              $caixa = $result_sat010401['caixa'];
              $sat = $result_sat010401['sat'];
              $status = $result_sat010401['status'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal SAT 01.05.00 -->
<div class="modal fade" id="modal_sat_010500" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DSAT-1.0 Firmware 01.05.00</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_fw.php?fw=01.04.01"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php
            $sql_sat010500 = "SELECT * FROM tb_sat WHERE firmware = '01.05.00' ORDER BY loja";
            $query_sat010500 = mysqli_query($conn, $sql_sat010500);
            echo "<tbody>";
            while ( $result_sat010500 = mysqli_fetch_array($query_sat010500))
            {
              $loja = $result_sat010500['loja'];
              $caixa = $result_sat010500['caixa'];
              $sat = $result_sat010500['sat'];
              $status = $result_sat010500['status'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>    
<!-- Modal SAT Vencimento 2020 -->
<div class="modal fade" id="modal_sat_venc_2020" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">SATs Vencimento 2020</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_vencimento.php?venc=2020"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
            <th>Data Ativação</th>
            <th>Data Fim Ativação</th>
          </tr>
          </thead>
          <?php
            $sql_sat_venc2020 = "SELECT * FROM tb_sat WHERE data_fim_ativacao > '2020-01-01'AND data_fim_ativacao < '2020-12-31' ORDER BY data_fim_ativacao";
            $query_sat_vanc2020 = mysqli_query($conn, $sql_sat_venc2020);
            echo "<tbody>";
            while ( $result_sat_venc2020 = mysqli_fetch_array($query_sat_vanc2020))
            {
              $loja = $result_sat_venc2020['loja'];
              $caixa = $result_sat_venc2020['caixa'];
              $sat = $result_sat_venc2020['sat'];
              $status = $result_sat_venc2020['status'];
              $data_ativacao = $result_sat_venc2020['data_ativacao'];
              $data_fim_ativacao = $result_sat_venc2020['data_fim_ativacao'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "<td>".$data_ativacao."</td>";
              echo "<td>".$data_fim_ativacao."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal SAT Vencimento 2021 -->
<div class="modal fade" id="modal_sat_venc_2021" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">SATs Vencimento 2021</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_vencimento.php?venc=2021"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
            <th>Data Ativação</th>
            <th>Data Fim Ativação</th>
          </tr>
          </thead>
          <?php
            $sql_sat_venc2021 = "SELECT * FROM tb_sat WHERE data_fim_ativacao > '2021-01-01'AND data_fim_ativacao < '2021-12-31' ORDER BY data_fim_ativacao";
            $query_sat_vanc2021 = mysqli_query($conn, $sql_sat_venc2021);
            echo "<tbody>";
            while ( $result_sat_venc2021 = mysqli_fetch_array($query_sat_vanc2021))
            {
              $loja = $result_sat_venc2021['loja'];
              $caixa = $result_sat_venc2021['caixa'];
              $sat = $result_sat_venc2021['sat'];
              $status = $result_sat_venc2021['status'];
              $data_ativacao = $result_sat_venc2021['data_ativacao'];
              $data_fim_ativacao = $result_sat_venc2021['data_fim_ativacao'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "<td>".$data_ativacao."</td>";
              echo "<td>".$data_fim_ativacao."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal SAT Vencimento 2022 -->
<div class="modal fade" id="modal_sat_venc_2022" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">SATs Vencimento 2022</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_vencimento.php?venc=2022"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
            <th>Data Ativação</th>
            <th>Data Fim Ativação</th>
          </tr>
          </thead>
          <?php
            $sql_sat_venc2022 = "SELECT * FROM tb_sat WHERE data_fim_ativacao > '2022-01-01'AND data_fim_ativacao < '2022-12-31' ORDER BY data_fim_ativacao";
            $query_sat_vanc2022 = mysqli_query($conn, $sql_sat_venc2022);
            echo "<tbody>";
            while ( $result_sat_venc2022 = mysqli_fetch_array($query_sat_vanc2022))
            {
              $loja = $result_sat_venc2022['loja'];
              $caixa = $result_sat_venc2022['caixa'];
              $sat = $result_sat_venc2022['sat'];
              $status = $result_sat_venc2022['status'];
              $data_ativacao = $result_sat_venc2022['data_ativacao'];
              $data_fim_ativacao = $result_sat_venc2022['data_fim_ativacao'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "<td>".$data_ativacao."</td>";
              echo "<td>".$data_fim_ativacao."</td>";
              echo "</tr>";
              echo "</tbody>";
            }
            echo "<tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal SAT Vencimento 2022 -->
<div class="modal fade" id="modal_sat_venc_2023" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">SATs Vencimento 2023</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_sat_vencimento.php?venc=2023"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Caixa</th>
            <th>Serie</th>
            <th>Status</th>
            <th>Data Ativação</th>
            <th>Data Fim Ativação</th>
          </tr>
          </thead>
          <?php
            $sql_sat_venc2023 = "SELECT * FROM tb_sat WHERE data_fim_ativacao > '2023-01-01'AND data_fim_ativacao < '2023-12-31' ORDER BY data_fim_ativacao";
            $query_sat_vanc2023 = mysqli_query($conn, $sql_sat_venc2023);
            echo "<tbody>";
            while ( $result_sat_venc2023 = mysqli_fetch_array($query_sat_vanc2023))
            {
              $loja = $result_sat_venc2023['loja'];
              $caixa = $result_sat_venc2023['caixa'];
              $sat = $result_sat_venc2023['sat'];
              $status = $result_sat_venc2023['status'];
              $data_ativacao = $result_sat_venc2023['data_ativacao'];
              $data_fim_ativacao = $result_sat_venc2023['data_fim_ativacao'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$caixa."</td>";
              echo "<td>".$sat."</td>";
              echo "<td>".$status."</td>";
              echo "<td>".$data_ativacao."</td>";
              echo "<td>".$data_fim_ativacao."</td>";
              echo "</tr>";
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal SAT Vencimento 2022 -->
<div class="modal fade" id="modal_dados_loja3210" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.32.001.10</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.32.001.10.BRA-0001"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
            $sql_loja3210 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.32.001.10.BRA-0001' AND contador > 2 ORDER BY Loja";
            $query_loja3210 = mysqli_query($conn, $sql_loja3210);
            echo "<tbody>";
            while ( $result_loja3210 = mysqli_fetch_array($query_loja3210))
            {
              $loja = $result_loja3210['Loja'];
              $tpvs = $result_loja3210['Qntd_Tpvs'];
              $ip = $result_loja3210['IP'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$tpvs."</td>";
              echo "<td>".$ip."</td>";
              echo "</tr>";
              
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 32.003.22 -->
<div class="modal fade" id="modal_dados_loja3222" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.32.003.22</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.32.003.22.BRA-0001"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
            $sql_loja3222 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.32.003.22.BRA-0001' AND contador > 2 ORDER BY Loja";
            $query_loja3222 = mysqli_query($conn, $sql_loja3210);
            echo "<tbody>";
            while ( $result_loja3222 = mysqli_fetch_array($query_loja3222))
            {
              $loja = $result_loja3222['Loja'];
              $tpvs = $result_loja3222['Qntd_Tpvs'];
              $ip = $result_loja3222['IP'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$tpvs."</td>";
              echo "<td>".$ip."</td>";
              echo "</tr>";
              
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 38.001.09 -->
<div class="modal fade" id="modal_dados_loja3809" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.38.001.09</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.38.001.09.BRA-0001"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
            $sql_loja3809 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.09.BRA-0001' AND contador > 2 ORDER BY Loja";
            $query_loja3809 = mysqli_query($conn, $sql_loja3809);
            echo "<tbody>";
            while ( $result_loja3809 = mysqli_fetch_array($query_loja3809))
            {
              $loja = $result_loja3809['Loja'];
              $tpvs = $result_loja3809['Qntd_Tpvs'];
              $ip = $result_loja3809['IP'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$tpvs."</td>";
              echo "<td>".$ip."</td>";
              echo "</tr>";
              
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 38.001.10 -->
<div class="modal fade" id="modal_dados_loja3810" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.38.001.10</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.38.001.10.BRA-0001"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
            $sql_loja3810 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.10.BRA-0001' ORDER BY Loja";
            $query_loja3810 = mysqli_query($conn, $sql_loja3810);
            echo "<tbody>";
            while ( $result_loja3810 = mysqli_fetch_array($query_loja3810))
            {
              $loja = $result_loja3810['Loja'];
              $tpvs = $result_loja3810['Qntd_Tpvs'];
              $ip = $result_loja3810['IP'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$tpvs."</td>";
              echo "<td>".$ip."</td>";
              echo "</tr>";
              
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 38.001.11 -->
<div class="modal fade" id="modal_dados_loja3811" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.38.001.11</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.38.001.11.BRA-0001"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
            $sql_loja3811 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.11.BRA-0001' ORDER BY Loja";
            $query_loja3811 = mysqli_query($conn, $sql_loja3811);
            echo "<tbody>";
            while ( $result_loja3811 = mysqli_fetch_array($query_loja3811))
            {
              $loja = $result_loja3811['Loja'];
              $tpvs = $result_loja3811['Qntd_Tpvs'];
              $ip = $result_loja3811['IP'];
              
              echo "<tr>";
              echo "<td>".$loja."</td>";
              echo "<td>".$tpvs."</td>";
              echo "<td>".$ip."</td>";
              echo "</tr>";
              
              
            }
            echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 38.001.12 -->
<div class="modal fade" id="modal_dados_loja3812" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.38.001.12</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.38.001.12.BRA-0001"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
              $sql_loja3812 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.12.BRA-0001'" ;
              $query_loja3812 = mysqli_query($conn, $sql_loja3812);
              echo "<tbody>";
              while ($result_loja3812 = mysqli_fetch_array($query_loja3812))
              {
                $loja = $result_loja3812['Loja'];
                $tpvs = $result_loja3812['Qntd_Tpvs'];
                $ip = $result_loja3812['IP'];
                
                echo "<tr>";
                echo "<td>".$loja."</td>";
                echo "<td>".$tpvs."</td>";
                echo "<td>".$ip."</td>";
                echo "</tr>";
                
                
              }
              echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 42.002.01.BRA-0006 -->
<div class="modal fade" id="modal_dados_loja4206" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.42.002.06</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.42.002.01.BRA-0006"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
              $sql_loja4206 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0006'" ;
              $query_loja4206 = mysqli_query($conn, $sql_loja4206);
              echo "<tbody>";
              while ($result_loja4206 = mysqli_fetch_array($query_loja4206))
              {
                $loja = $result_loja4206['Loja'];
                $tpvs = $result_loja4206['Qntd_Tpvs'];
                $ip = $result_loja4206['IP'];
                
                echo "<tr>";
                echo "<td>".$loja."</td>";
                echo "<td>".$tpvs."</td>";
                echo "<td>".$ip."</td>";
                echo "</tr>";
                
                
              }
              echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 42.002.01.BRA-0007 -->
<div class="modal fade" id="modal_dados_loja4207" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.42.002.07</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.42.002.01.BRA-0007"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
              $sql_loja4207 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0007'" ;
              $query_loja4207 = mysqli_query($conn, $sql_loja4207);
              echo "<tbody>";
              while ($result_loja4207 = mysqli_fetch_array($query_loja4207))
              {
                $loja = $result_loja4207['Loja'];
                $tpvs = $result_loja4207['Qntd_Tpvs'];
                $ip = $result_loja4207['IP'];
                
                echo "<tr>";
                echo "<td>".$loja."</td>";
                echo "<td>".$tpvs."</td>";
                echo "<td>".$ip."</td>";
                echo "</tr>";
                
                
              }
              echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 42.002.01.BRA-0008 -->
<div class="modal fade" id="modal_dados_loja4208" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.42.002.08</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.42.002.01.BRA-0008"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
              $sql_loja4208 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0008'" ;
              $query_loja4208 = mysqli_query($conn, $sql_loja4208);
              echo "<tbody>";
              while ($result_loja4208 = mysqli_fetch_array($query_loja4208))
              {
                $loja = $result_loja4208['Loja'];
                $tpvs = $result_loja4208['Qntd_Tpvs'];
                $ip = $result_loja4208['IP'];
                
                echo "<tr>";
                echo "<td>".$loja."</td>";
                echo "<td>".$tpvs."</td>";
                echo "<td>".$ip."</td>";
                echo "</tr>";
                
                
              }
              echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 42.002.01.BRA-0010 -->
<div class="modal fade" id="modal_dados_loja4210" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.42.002.10</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.42.002.01.BRA-0010"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
              $sql_loja4210 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.42.002.01.BRA-0010'" ;
              $query_loja4210 = mysqli_query($conn, $sql_loja4210);
              echo "<tbody>";
              while ($result_loja4210 = mysqli_fetch_array($query_loja4210))
              {
                $loja = $result_loja4210['Loja'];
                $tpvs = $result_loja4210['Qntd_Tpvs'];
                $ip = $result_loja4210['IP'];
                
                echo "<tr>";
                echo "<td>".$loja."</td>";
                echo "<td>".$tpvs."</td>";
                echo "<td>".$ip."</td>";
                echo "</tr>";
                
                
              }
              echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal 38.001.16 -->
<div class="modal fade" id="modal_dados_loja3816" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja SA.38.001.16</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=dia-update-SA.38.001.16.BRA-0001"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
              $sql_loja3816 = "SELECT * FROM tb_dados_loja WHERE Versao = 'dia-update-SA.38.001.16.BRA-0001'" ;
              $query_loja3816 = mysqli_query($conn, $sql_loja3816);
              echo "<tbody>";
              while ($result_loja3816 = mysqli_fetch_array($query_loja3816))
              {
                $loja = $result_loja3816['Loja'];
                $tpvs = $result_loja3816['Qntd_Tpvs'];
                $ip = $result_loja3816['IP'];
                
                echo "<tr>";
                echo "<td>".$loja."</td>";
                echo "<td>".$tpvs."</td>";
                echo "<td>".$ip."</td>";
                echo "</tr>";
                
                
              }
              echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Total de lojas -->
<div class="modal fade" id="modal_dados_loja_total" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dados Loja</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
                </button>
      </div>
      <div class="modal-header">
        <a href="../export/export_dados_loja.php?vs=total_lojas"><button type="submit" class="btn btn-danger">Exportar</button></a>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>Loja</th>
            <th>Qntd Tpvs</th>
            <th>IP</th>
          </tr>
          </thead>
          <?php
              $sql_loja_total = "SELECT * FROM tb_dados_loja ORDER BY Loja " ;
              $query_loja_total = mysqli_query($conn, $sql_loja_total);
              echo "<tbody>";
              while ($result_loja_total = mysqli_fetch_array($query_loja_total))
              {
                $loja = $result_loja_total['Loja'];
                $tpvs = $result_loja_total['Qntd_Tpvs'];
                $ip = $result_loja_total['IP'];
                
                echo "<tr>";
                echo "<td>".$loja."</td>";
                echo "<td>".$tpvs."</td>";
                echo "<td>".$ip."</td>";
                echo "</tr>";
                
                
              }
              echo "</tbody>";
          ?>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <script src="../js/jquery-3.2.1.min.js"></script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>

        <!-- Icons -->
        <script src="../js/feather.min.js"></script>
        <script>
          feather.replace()
        </script>
  </body>
</html>
