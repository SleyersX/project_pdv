<?php
  include_once("../security/seguranca.php");
  protegePagina();
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs | Servidor de Chaves</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="../admin/admin.php">Admistrativo -
		  <?php  
		    echo $_SESSION['usuarioNome'];
      ?>	  
      </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="../security/sair.php">Sair</a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="../admin/admin.php">
                  <span data-feather="home"></span>
                  Paine de Controle<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../admin/sats.php">
                  <span data-feather="file"></span>
                  SATs
                </a>
              </li>
			        <li class="nav-item">
                <a class="nav-link" href="../admin/list_lojas.php">
                  <span data-feather="file"></span>
                  Cadastro de Lojas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../admin/usuarios.php">
                  <span data-feather="users"></span>
                  Usuarios<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#?" data-toggle="modal" data-target="#editaSenha">
                  <span data-feather="file"></span>
                  Alterar Senha
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../admin/scripts.php">
                  <span data-feather="bar-chart-2"></span>
                  Scripts
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../admin/nivel.php">
                  <span data-feather="layers"></span>
                  Niveis de Acesso
                </a>
              </li>
			  <li class="nav-item">
                <a class="nav-link active" href="srv_chaves.php">
                  <span data-feather="layers"></span>
                  Servidor de Chaves
                </a>
              </li>
            </ul>

           
          </div>
        </nav>
        <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
              <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                          <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                        </div>
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                          <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Confirma</button>
                    </div>
                  </div>
                </div>
              </div>
        </form>
		<main role="main" class="col-md-4 ml-sm-auto col-lg-10 pt-4 px-4">
		<div class="col-sm-6" style="float:left;">
			<table class="table table-striped table-bordered table-hover" id="tb_sat">
				
							<thead>
								<tr>
								  <th>#</th>
								  <th>SAT</th>
								</tr>
							</thead>
									<tr>
										<?php
											echo "<tbody>";
											$path ='D:/Web-Developers/Server/htdocs/pdv/resorces/srv_chaves/arquivos/claves_sat/';
                      //$path = '/opt/lammp/htdocs/resorces/srv_chaves/arquivos/claves_sat/';
                      $diretorio = dir($path);
													while($arquivo = $diretorio -> read()) 										
													{	
														if ($arquivo != '..' && $arquivo != '.'){
																echo "<tr>";
																echo "<td><img width='40px' height='25px' src='../img/zip.jpg'></td>";
																//echo "<td><a href='/srv_chaves/arquivos/claves_sat/".$arquivo."'>".$arquivo."</a></td>";
                                echo "<td><a href='/pdv/resorces/srv_chaves/arquivos/claves_sat/".$arquivo."'>".$arquivo."</a></td>";
                                echo "</tr>";		
															}
													}
													$diretorio -> close();
											echo "</tbody>";
										?>
					</table>
				</div>
		<div class="col-sm-6" style="float:left;">
			<table class="table table-striped table-bordered table-hover" id="tb_nfce">
				
							<thead>
								<tr>
								  <th>#</th>
								  <th>NFCe</th>
								</tr>
							</thead>
									<tr>
										<?php
											echo "<tbody>";
											$path2='D:/Web-Developers/Server/htdocs/pdv/resorces/srv_chaves/arquivos/claves_nfce/';
											$diretorio2 = dir($path2);
												while($arquivo2 = $diretorio2 -> read()) 										
													{	
														if ($arquivo2 != '..' && $arquivo2 != '.'){
																echo "<tr>";
																echo "<td><img width='40px' height='25px' src='../img/zip.jpg'></td>";
																//echo "<td><a href='/srv_chaves/arquivos/claves_nfce/".$arquivo2."'>".$arquivo2."</a></td>";
                                echo "<td><a href='/pdv/resorces/srv_chaves/arquivos/claves_nfce/".$arquivo2."'>".$arquivo2."</a></td>";
                                echo "</tr>";		
															}
													}
													$diretorio2 -> close();
											echo "</tbody>";
										?>
					</table>
		</div>
		</main>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="../js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
  </body>
</html>
