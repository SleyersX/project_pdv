
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://www.dia.com.br/static/favicon/favicon-16x16.png">

    <title>Support TPVs</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
	<!--<link href="theme.css" rel="stylesheet">-->
	
	<script src="js/ie-emulation-modes-warning.js"></script>
  </head>

  <body>
  <form name="consulta" action="user2.php" method="POST">
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
	  </a>
		<select name="cb_consulta" class="form-control form-control-dark sticky-top bg-dark flex-md-nowrap p-0">
			<option value="vazio"></option>
			<option value="loja">Loja</option>
			<option value="sat">SAT</option>  
		</select>        
      <input class="form-control form-control-dark w-100" type="text" name="txt_consulta" placeholder="Pesquisar: Loja/SAT" aria-label="search" >
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="sair.php">Sair</a>
        </li>
      </ul>
    </nav>
	<!--
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                
          </div>
        </nav>
		-->
	</form>
        <main role="main" class="col-md-20 col-lg-20 pt-20 px-20">
        <h2>Dashboard</h2>
 <?php
		
		 $metodo_consulta = $_POST['cb_consulta'];

if ( $metodo_consulta == "loja")
	{
		  //Criando tabela e cabeçalho de dados:
		 echo '<div class="page-header">';
		 echo '<table class="table table-striped table-sm">';
		 echo '</div>';
		 echo '<div class="row">';
		 echo '<table class="table">';
		 echo '<thead>';
		 echo "<tr>";
		 echo "<th>SAT</th>";
		 echo "<th>Loja</th>";
		 echo "<th>Caixa</th>";
		 echo "<th>IP</th>";
		 echo "<th>MASK</th>";
		 echo "<th>GW</th>";
		 echo "<th>DNS 1</th>";
		 echo "<th>DNS 2</th>";
		 echo "<th>Firmware</th>";
		 echo "<th>Layout</th>";
		 echo "<th>Usado</th>";
		 echo "<th>Ativacao</th>";
		 echo "<th>Fim Ativacao</th>";
		 echo "</tr>";
		 echo "</thead>";

		$nume_loja = $_POST['txt_consulta'];
		$num_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);
		 // Conectando ao banco de dados:
		 $strcon = mysqli_connect('localhost','pdv','123456789','tpvs') or die('Erro ao conectar ao banco de dados');
		 $sql = "SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, status FROM tb_sat WHERE loja ='$num_loja' ORDER BY caixa";
		 $resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
		 // Obtendo os dados por meio de um loop while
			while ($registro = mysqli_fetch_array($resultado))
			{
				$sat = $registro['sat'];
				$loja = $registro['loja'];
				$caixa = $registro['caixa'];
				$ip = $registro['ip'];
				$mascara = $registro['mask'];
				$geteway = $registro['gw'];
				$dns_1 = $registro['dns_1'];
				$dns_2 = $registro['dns_2'];
				$firmware = $registro['firmware'];
				$layout = $registro['layout'];
				$disco = $registro['disco'];
				$disco_usado = $registro['disco_usado'];
				$data_ativacao = $registro['data_ativacao'];
				$data_fim_ativacao = $registro['data_fim_ativacao'];
				$status = $registro['status'];
				echo "<tbody>";
				echo "<tr>";
				echo "<td>".$sat."</td>";
				echo "<td>".$loja."</td>";
				echo "<td>".$caixa."</td>";
				echo "<td>".$ip."</td>";
				echo "<td>".$mascara."</td>";
				echo "<td>".$geteway."</td>";
				echo "<td>".$dns_1."</td>";
				echo "<td>".$dns_2."</td>";
				echo "<td>".$firmware."</td>";
				echo "<td>".$layout."</td>";
				echo "<td>".$disco_usado."</td>";
				echo "<td>".$data_ativacao."</td>";
				echo "<td>".$data_fim_ativacao."</td>";
				echo "</tr>";
				echo "</tbody>";
			
			}
				 mysqli_close($strcon);
				 echo "</table>";
				 echo "</div>";
																			 	 
//
		echo '<div class="page-header">';
		echo '<table class="table table-striped table-sm">';
		echo '</div>';
		echo '<div class="row">';
		echo '<table class="table">';
		echo '<thead>';
		echo "<tr>";
		echo "<th>Loja</th>";
		echo "<th>PDV</th>";
		echo "<th>Impressora</th>";
		echo "<th>CPU</th>";
		echo "</tr>";
																			
		$n_loja = $_POST['txt_consulta'];
		$nu_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);

			 // Conectando ao banco de dados:
			 $strconn = mysqli_connect('localhost','pdv','123456789','tpvs') or die('Erro ao conectar ao banco de dados');
			 $sql = "SELECT Loja, PDV, Impressora, CPU FROM tb_consolidado_equipamentos WHERE Loja ='$nu_loja' ORDER BY PDV";
			 $result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
			 // Obtendo os dados por meio de um loop while
			 while ($reg = mysqli_fetch_array($result))
				 {
					$loja = $reg['Loja'];
					$pdv = $reg['PDV'];
					$imp = $reg['Impressora'];
					$cpu = $reg['CPU'];
					echo "<tbody>";
					echo "<tr>";
					echo "<td>".$loja."</td>";
					echo "<td>".$pdv."</td>";
					echo "<td>".$imp."</td>";
					echo "<td>".$cpu."</td>";
					echo "</tr>";
					echo "</tbody>";
				 }	
					 mysqli_close($strconn);
					 echo "</table>";
				

		echo '<div class="page-header">';
		echo '<table class="table table-striped table-sm">';
		echo '</div>';
		echo '<div class="row">';
		echo '<table class="table">';
		echo '<thead>';
		echo "<tr>";
		echo "<th>Loja</th>";
		echo "<th>QNTD PDVs</th>";
		echo "<th>Versao</th>";
		echo "<th>IP</th>";
		echo "</tr>";


																			
			$n_loja = $_POST['txt_consulta'];
			$nu_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);

				 // Conectando ao banco de dados:
				 $strconn = mysqli_connect('localhost','pdv','123456789','tpvs') or die('Erro ao conectar ao banco de dados');
				 $sql = "SELECT Loja, Qntd_Tpvs, Versao, IP FROM tb_dados_loja WHERE Loja ='$nu_loja'";
				 $result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
				 // Obtendo os dados por meio de um loop while
				 while ($reg = mysqli_fetch_array($result))
				 {
					$loja = $reg['Loja'];
					$qntd_tpvs = $reg['Qntd_Tpvs'];
					$versao = $reg['Versao'];
					$ip = $reg['IP'];
						echo "<tbody>";
						echo "<center>";
						echo "<tr>";
						echo "<td>".$loja."</td>";
						echo "<td>".$qntd_tpvs."</td>";
						echo "<td>".$versao."</td>";
						echo "<td>".$ip."</td>";
						echo "</tr>";
						echo "</center>";
						echo "</tbody>";
					 }	
					 mysqli_close($strconn);
					 echo "</table>";


		 echo '<div class="page-header">';
		 echo '<table class="table table-striped table-sm">';
		 echo '</div>';
		 echo '<div class="row">';
		 echo '<table class="table">';
		 echo '<thead>';
		 echo "<tr>";
		 echo "<th>Loja</th>";
		 echo "<th>Caixa</th>";
		 echo "<th>Cod Empresa</th>";
		 echo "<th>Cod Filial</th>";
		 echo "<th>Cod PDV</th>";
		 echo "</tr>";

				$n_loja = $_POST['txt_consulta'];
				$nu_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);

					 // Conectando ao banco de dados:
					 $strconn = mysqli_connect('localhost','pdv','123456789','tpvs') or die('Erro ao conectar ao banco de dados');
					 $sql = "SELECT Loja, Caixa, Cod_Empresa, Filial, Cod_Pdv FROM tb_dados_tef WHERE Loja ='$nu_loja'";
					 $result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
					 // Obtendo os dados por meio de um loop while
					 while ($reg = mysqli_fetch_array($result))
					 {
						$loja = $reg['Loja'];
						$caixa = $reg['Caixa'];
						$cod_empresa = $reg['Cod_Empresa'];
						$cod_filial = $reg['Filial'];
						$cod_pdv = $reg['Cod_Pdv'];
							echo "<tbody>";
							echo "<tr>";
							echo "<td>".$loja."</td>";
							echo "<td>".$caixa."</td>";
							echo "<td>".$cod_empresa."</td>";
							echo "<td>".$cod_filial."</td>";
							echo "<td>".$cod_pdv."</td>";
							echo "</tr>";
							echo "</tbody>";
						 }	
					 mysqli_close($strconn);
					 echo "</table>";
					 
	}else{
		$metodo_consulta = $_POST['cb_consulta'];
		
		if( $metodo_consulta == "sat")
			
			{
																		
		  //Criando tabela e cabeçalho de dados:
				 echo '<div class="page-header">';
				 echo '<table class="table table-striped table-sm">';
				 echo '</div>';
				 echo '<div class="row">';
				 echo '<table class="table">';
				 echo '<thead>';
				 echo "<tr>";
				 echo "<th>SAT</th>";
				 echo "<th>Loja</th>";
				 echo "<th>Caixa</th>";
				 echo "<th>IP</th>";
				 echo "<th>MASK</th>";
				 echo "<th>GW</th>";
				 echo "<th>DNS 1</th>";
				 echo "<th>DNS 2</th>";
				 echo "<th>Firmware</th>";
				 echo "<th>Layout</th>";
				 echo "<th>Usado</th>";
				 echo "<th>Ativacao</th>";
				 echo "<th>Fim Ativacao</th>";
				 echo "</tr>";
				 echo "</thead>";	 
						$nume_sat = $_POST['txt_consulta'];
						$num_sat = str_pad($nume_sat,9,'0', STR_PAD_LEFT);
						 // Conectando ao banco de dados:
						 $strcon = mysqli_connect('localhost','pdv','123456789','tpvs') or die('Erro ao conectar ao banco de dados');
						 $sql = "SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, status FROM tb_sat WHERE sat ='$num_sat' ORDER BY loja";
						 $resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
						 // Obtendo os dados por meio de um loop while
						 while ($registro = mysqli_fetch_array($resultado))
						 {
							$sat = $registro['sat'];
							$loja = $registro['loja'];
							$caixa = $registro['caixa'];
							$ip = $registro['ip'];
							$mascara = $registro['mask'];
							$geteway = $registro['gw'];
							$dns_1 = $registro['dns_1'];
							$dns_2 = $registro['dns_2'];
							$firmware = $registro['firmware'];
							$layout = $registro['layout'];
							$disco = $registro['disco'];
							$disco_usado = $registro['disco_usado'];
							$data_ativacao = $registro['data_ativacao'];
							$data_fim_ativacao = $registro['data_fim_ativacao'];
							$status = $registro['status'];
							echo "<tbody>";
							echo "<tr>";
							echo "<td>".$sat."</td>";
							echo "<td>".$loja."</td>";
							echo "<td>".$caixa."</td>";
							echo "<td>".$ip."</td>";
							echo "<td>".$mascara."</td>";
							echo "<td>".$geteway."</td>";
							echo "<td>".$dns_1."</td>";
							echo "<td>".$dns_2."</td>";
							echo "<td>".$firmware."</td>";
							echo "<td>".$layout."</td>";
							echo "<td>".$disco_usado."</td>";
							echo "<td>".$data_ativacao."</td>";
							echo "<td>".$data_fim_ativacao."</td>";
							echo "</tr>";
							echo "</tbody>";
							}
							 mysqli_close($strcon);
							 echo "</table>";
							 echo "</div>";
             }
	}
?>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    <!-- Graphs 
    <script src="js/Chart.min.js"></script>
    <script>
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          datasets: [{
            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
            lineTension: 0,
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            borderWidth: 4,
            pointBackgroundColor: '#007bff'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: false
              }
            }]
          },
          legend: {
            display: false,
          }
        }
      });
    </script>
	-->
  </body>
</html>
