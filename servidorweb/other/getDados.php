<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://www.dia.com.br/static/favicon/favicon-16x16.png">

    <title>Support TPVs</title>
 <?php
 include_once("conecta.php");

			$num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
			//Obter a data atual
			$resultado_qnt_cadastros = mysqli_query($conn, $num_users);
			$row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
			
			$data['atual'] = date('Y-m-d H:i:s');	

			//Diminuir 20 segundos 
			$data['online'] = strtotime($data['atual'] . " - 20 seconds");
			$data['online'] = date("Y-m-d H:i:s",$data['online']);
			
			//Pesquisar os ultimos usuarios online nos 20 segundo
			$result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
			
			$resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
			$row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
			
			$qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
			
			
		?>
			<div class="page-header">
				<table class="table table-striped table-sm">
					</div>
						<div class="row">
						<table class="table">
					<thead>
				<tr>
			<th>Usuarios Onlines</th>
			
			<tbody>
			<tr>
			<td><span id='online'><?php echo $row_qnt_visitas['online']; ?></span></td>
			</tr>
			</tbody>
			</table>
			</div>
						
		<!--<h1>Quantidade de usuários online: <span id='online'><?php echo $row_qnt_visitas['online']; ?></span></h1>-->
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		
		<script>
		//Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
		setInterval(function(){
			//Incluir e enviar o POST para o arquivo responsável em fazer contagem
			$.post("processa_vis2.php", {contar: '',}, function(data){
				$('#online').text(data);
			});
		}, 10000);
		</script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Effort', 'Amount given'],
          ['Usuários Onlines',    <?php echo $row_qnt_visitas['online']; ?> ],
		  ['Usuário Offline',  <?php echo $qnt_offline; ?> ],
        ]);

        var options = {
          pieHole: 0.5,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'none'
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="donut_single" style="width: 900px; height: 500px;"></div>
  </body>
</html>
