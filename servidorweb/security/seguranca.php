<?php
/**
* Sistema de segurança com acesso restrito
*
* Usado para restringir o acesso de certas páginas do seu site
*
* @version 1.0
* @package SistemaSeguranca
*/
//  Configurações do Script
// ==============================
$_SG['conectaServidor'] = true;    // Abre uma conexão com o servidor MySQL?
$_SG['abreSessao'] = true;         // Inicia a sessão com um session_start()?
$_SG['caseSensitive'] = false;     // Usar case-sensitive?
$_SG['validaSempre'] = true;       // Deseja validar o usuário e a senha a cada carregamento de página
$_SG['paginaLogin'] = "../index.php"; // Página de login

if ($_SG['conectaServidor'] == true) {
    //Conectando no servidor
    require_once("conecta.php");
    }
    // Verifica se precisa iniciar a sessão
    if ($_SG['abreSessao'] == true)
      session_start();
    /**
    * Função que valida um usuário e senha
    *
    * @param string $usuariot - O usuário a ser validado
    * @param string $senhat - A senha a ser validada
    *
    * @return bool - Se o usuário foi validado ou não (true/false)
    */
    function validaUsuario($usuariot, $senhat) {
      global $_SG;
      $cS = ($_SG['caseSensitive']) ? 'BINARY' : ''; 
      // Usa a função addslashes para escapar as aspas
      $nusuario = addslashes($usuariot);
      $nsenha = addslashes($senhat);
      //$nlembrete = addcslashes($remembert);
	  //echo "<script>alert('Senha1:$nsenha');</script>";
      $user = "root";
      $password = "diabrasil";
        
      // Monta uma consulta SQL (query) para procurar um usuário
      $conn = mysqli_connect('localhost', $user, $password , 'srvremoto')or die ("Erro ao conectar com o banco de dados!");
      //require_once("conecta.php");
      //$sql = "SELECT * FROM tb_usuarios WHERE login='$nusuario' AND senha='$nsenha' LIMIT 1";
      $sql = "SELECT * FROM tb_usuarios WHERE login='$nusuario'";
      $query = mysqli_query($conn,$sql);
      $resultado = mysqli_fetch_assoc($query);
      // Verifica se encontrou algum registro
      if (empty($resultado)) {
        // Nenhum registro foi encontrado => o usuário é inválido
        return false;
      } else {
				$sql_ = "SELECT * FROM tb_usuarios WHERE login='$nusuario'";
				$query_ = mysqli_query($conn,$sql_);
				while($resulta = mysqli_fetch_array($query_))
				{
					$senha_crypt = $resulta['senha'];
					//echo "<script>alert('$senha_crypt');</script>";
					//echo "<script>alert('Senha2:$nsenha');</script>";
					$check_senha=password_verify($nsenha, $senha_crypt);
					echo "<script>alert('$check_senha');</script>";
					if($check_senha == true){
						// Definimos dois valores na sessão com os dados do usuário
						$_SESSION['usuarioID'] = $resultado['id']; // Pega o valor da coluna 'id do registro encontrado no MySQL
						$_SESSION['usuarioNome'] = $resultado['nome']; // Pega o valor da coluna 'nome' do registro encontrado no MySQL
            $_SESSION['usuarioNivel'] = $resultado['nivel'];
            
						// Verifica a opção se sempre validar o login
						if ($_SG['validaSempre'] == true) {
						  // Definimos dois valores na sessão com os dados do login
						  $_SESSION['usuarioLogin'] = $login;
						  $_SESSION['usuarioSenha'] = $senha;
            }
						return true;
					} else {
						return false;
					}
				}
			
		}
    }
    /**
    * Função que protege uma página
    */
    function protegePagina() {
      global $_SG;
      if (!isset($_SESSION['usuarioID']) OR !isset($_SESSION['usuarioNome'])) {
        // Não há usuário logado, manda pra página de login
        expulsaVisitante();
      } else if (!isset($_SESSION['usuarioID']) OR !isset($_SESSION['usuarioNome'])) {
        // Há usuário logado, verifica se precisa validar o login novamente
        if ($_SG['validaSempre'] == true) {
          // Verifica se os dados salvos na sessão batem com os dados do banco de dados
          if (!validaUsuario($_SESSION['usuarioLogin'], $_SESSION['usuarioSenha'])) {
            // Os dados não batem, manda pra tela de login
            expulsaVisitante();
          }
        }
      }
    }
    /**
    * Função para expulsar um visitante
    */
    function expulsaVisitante() {
      echo "<script>alert('Mensagem');</script>";
	  global $_SG;
      // Remove as variáveis da sessão (caso elas existam)
      unset($_SESSION['usuarioID'], $_SESSION['usuarioNome'], $_SESSION['usuarioLogin'], $_SESSION['usuarioSenha']);
      // Manda pra tela de login
      
      header("Location: ".$_SG['paginaLogin']);
    }
	
	
?>
