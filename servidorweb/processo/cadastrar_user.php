<?php
	include_once ("../security/seguranca.php");
	protegePagina();
?>
<?php
	session_start();
	include_once("../security/conecta.php");

	$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
	$login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING); 
	$nivel = filter_input(INPUT_POST, 'cb_nivel', FILTER_SANITIZE_STRING);
	$data = date('Y-m-d H:i:s'); 
	$senha = password_hash('123456', PASSWORD_DEFAULT, ['cost' => 12 ]);

	$result_usuario = "INSERT INTO tb_usuarios (nome, login, email, senha,nivel, data_criacao) VALUES ('$nome', '$login','$email', '$senha','$nivel','$data')";
	$resultado_usuario = mysqli_query($conn, $result_usuario);

	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<p style='color:green;'>Usuário cadastrado com sucesso</p>";
		header("Location: ../admin/usuarios.php");
	}else{
		$_SESSION['msg'] = "<p style='color:red;'>Usuário não foi cadastrado com sucesso</p>";
		header("Location: ../admin/novousuario.php");
	}

?>
