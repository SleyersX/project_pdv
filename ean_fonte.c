#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 *   Autor : Walter Moura
 *   Data  : 2019-02-09
 *   Modificado : 2019-02-10
 *
 *   Algoritimo para gerar digito verificador ean
 *
 *   EAN -> 789114912345
 *
 *   Separamos os números na posição impar(1,3,5,7,9 e 11) e par(2,4,6,8,10 e 12)
 *
 *   1 -> 7 | 3 -> 9 | 5 -> 1 | 7 -> 9 | 9 -> 2 | 11 -> 4
 *   2 -> 8 | 4 -> 1 | 6 -> 4 | 8 -> 1 | 10 -> 3| 12 -> 5
 *
 *   soma_impar -> (7+9+1+9+2+4)     -> 32
 *   soma_par   -> ((8+1+4+1+3+5)*3) -> 66
 *
 *   soma_total -> (soma_impar+soma_par) -> (32+66) -> 98
 *
 *   Fazer a divisão de por 10 e o resto da divisão subtrair 10
 *
 */

//Declaração de funções
char *gets(char *ean);

int numcaract_12(char ean_12[13]);
int numcaract_11(char ean_11[12]);
int numcaract_10(char ean_10[11]);
int numcaract_09(char ean_09[9] );
int numcaract_08(char ean_08[8] );
int numcaract_07(char ean_07[7] );
int numcaract_06(char ean_06[6] );
int numcaract_05(char ean_05[5] );

int main(){

    int num_caract, digito;
    char ean[13];

    printf("Digite um EAN-13: ");
    gets(ean);

    num_caract = strlen(ean);

    if(num_caract == 12){
        digito=numcaract_12(ean);
        printf("%s%d\n", ean, digito);
    }else if(num_caract == 11){
        digito=numcaract_11(ean);
        printf("%s%d\n", ean, digito);
    }else if(num_caract == 10){
        digito=numcaract_10(ean);
        printf("%s%d\n", ean, digito);
    }else if(num_caract == 9 ){
        digito=numcaract_09(ean);
        printf("%s%d\n", ean, digito);
    }else if(num_caract == 8 ){
        digito=numcaract_08(ean);
        printf("%s%d\n", ean, digito);
    }else if(num_caract == 7 ){
        digito=numcaract_07(ean);
        printf("%s%d\n", ean, digito);
    }else if(num_caract == 6){
        digito=numcaract_06(ean);
        printf("%s%d\n", ean, digito);
    }else if(num_caract == 5){
        digito=numcaract_05(ean);
        printf("%s%d\n", ean, digito);
    }else{
        printf("Número de digitos insuficientes, mínimo permitido 5 caracteres.\n");      
    }

    return 0;
}

int numcaract_12(char ean_12[13]){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento

    x = 0;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 11 ; i++){
        if(ean_12[i] == 48){
            ean_int[x] = 0;
         }else if(ean_12[i] == 49){
            ean_int[x] = 1;
         }else if(ean_12[i] == 50){
            ean_int[x] = 2;
         }else if(ean_12[i] == 51){
            ean_int[x] = 3;
         }else if(ean_12[i] == 52){
            ean_int[x] = 4;
         }else if(ean_12[i] == 53){
            ean_int[x] = 5;
         }else if(ean_12[i] == 54){
            ean_int[x] = 6;
         }else if(ean_12[i] == 55){
            ean_int[x] = 7;
         }else if(ean_12[i] == 56){
            ean_int[x] = 8;
         }else if(ean_12[i] == 57){
            ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}

int numcaract_11(char ean_11[12]){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento
    ean_int[0] = 0;
    x = 1;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 10 ; i++){
        if(ean_11[i] == 48){
            ean_int[x] = 0;
         }else if(ean_11[i] == 49){
            ean_int[x] = 1;
         }else if(ean_11[i] == 50){
            ean_int[x] = 2;
         }else if(ean_11[i] == 51){
            ean_int[x] = 3;
         }else if(ean_11[i] == 52){
            ean_int[x] = 4;
         }else if(ean_11[i] == 53){
            ean_int[x] = 5;
         }else if(ean_11[i] == 54){
            ean_int[x] = 6;
         }else if(ean_11[i] == 55){
            ean_int[x] = 7;
         }else if(ean_11[i] == 56){
            ean_int[x] = 8;
         }else if(ean_11[i] == 57){
            ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}

int numcaract_10(char ean_10[11]){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento
    ean_int[0] = 0;
    ean_int[1] = 0;
    x = 2;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 9 ; i++){
        if(ean_10[i] == 48){
            ean_int[x] = 0;
         }else if(ean_10[i] == 49){
            ean_int[x] = 1;
         }else if(ean_10[i] == 50){
            ean_int[x] = 2;
         }else if(ean_10[i] == 51){
            ean_int[x] = 3;
         }else if(ean_10[i] == 52){
            ean_int[x] = 4;
         }else if(ean_10[i] == 53){
            ean_int[x] = 5;
         }else if(ean_10[i] == 54){
            ean_int[x] = 6;
         }else if(ean_10[i] == 55){
            ean_int[x] = 7;
         }else if(ean_10[i] == 56){
            ean_int[x] = 8;
         }else if(ean_10[i] == 57){
            ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}

int numcaract_09(char ean_09[10] ){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento
    ean_int[0] = 0;
    ean_int[1] = 0;
    ean_int[2] = 0;
    x = 3;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 8 ; i++){
        if(ean_09[i] == 48){
            ean_int[x] = 0;
         }else if(ean_09[i] == 49){
            ean_int[x] = 1;
         }else if(ean_09[i] == 50){
            ean_int[x] = 2;
         }else if(ean_09[i] == 51){
            ean_int[x] = 3;
         }else if(ean_09[i] == 52){
            ean_int[x] = 4;
         }else if(ean_09[i] == 53){
            ean_int[x] = 5;
         }else if(ean_09[i] == 54){
            ean_int[x] = 6;
         }else if(ean_09[i] == 55){
            ean_int[x] = 7;
         }else if(ean_09[i] == 56){
            ean_int[x] = 8;
         }else if(ean_09[i] == 57){
            ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}

int numcaract_08(char ean_08[9] ){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento
    ean_int[0] = 0;
    ean_int[1] = 0;
    ean_int[2] = 0;
    ean_int[3] = 0;
    x = 4;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 7 ; i++){
        if(ean_08[i] == 48){
            ean_int[x] = 0;
         }else if(ean_08[i] == 49){
            ean_int[x] = 1;
         }else if(ean_08[i] == 50){
            ean_int[x] = 2;
         }else if(ean_08[i] == 51){
            ean_int[x] = 3;
         }else if(ean_08[i] == 52){
            ean_int[x] = 4;
         }else if(ean_08[i] == 53){
            ean_int[x] = 5;
         }else if(ean_08[i] == 54){
            ean_int[x] = 6;
         }else if(ean_08[i] == 55){
            ean_int[x] = 7;
         }else if(ean_08[i] == 56){
            ean_int[x] = 8;
         }else if(ean_08[i] == 57){
            ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}

int numcaract_07(char ean_07[8] ){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento
    ean_int[0] = 0;
    ean_int[1] = 0;
    ean_int[2] = 0;
    ean_int[3] = 0;
    ean_int[4] = 0;
    x = 5;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 6 ; i++){
        if(ean_07[i] == 48){
            ean_int[x] = 0;
         }else if(ean_07[i] == 49){
            ean_int[x] = 1;
         }else if(ean_07[i] == 50){
            ean_int[x] = 2;
         }else if(ean_07[i] == 51){
            ean_int[x] = 3;
         }else if(ean_07[i] == 52){
            ean_int[x] = 4;
         }else if(ean_07[i] == 53){
            ean_int[x] = 5;
         }else if(ean_07[i] == 54){
            ean_int[x] = 6;
         }else if(ean_07[i] == 55){
            ean_int[x] = 7;
         }else if(ean_07[i] == 56){
            ean_int[x] = 8;
         }else if(ean_07[i] == 57){
            ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}

int numcaract_06(char ean_06[7] ){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento
    ean_int[0] = 0;
    ean_int[1] = 0;
    ean_int[2] = 0;
    ean_int[3] = 0;
    ean_int[4] = 0;
    ean_int[5] = 0;
    x = 6;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 5 ; i++){
        if(ean_06[i] == 48){
            ean_int[x] = 0;
         }else if(ean_06[i] == 49){
            ean_int[x] = 1;
         }else if(ean_06[i] == 50){
            ean_int[x] = 2;
         }else if(ean_06[i] == 51){
            ean_int[x] = 3;
         }else if(ean_06[i] == 52){
            ean_int[x] = 4;
         }else if(ean_06[i] == 53){
            ean_int[x] = 5;
         }else if(ean_06[i] == 54){
            ean_int[x] = 6;
         }else if(ean_06[i] == 55){
            ean_int[x] = 7;
         }else if(ean_06[i] == 56){
            ean_int[x] = 8;
         }else if(ean_06[i] == 57){
            ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}

int numcaract_05(char ean_05[6] ){

    //Declaração de variáveis
    int ean_int[13], x, impar[6], par[6], soma_impar, soma_par, soma_total, divisao, digito;

    //Processamento
    ean_int[0] = 0;
    ean_int[1] = 0;
    ean_int[2] = 0;
    ean_int[3] = 0;
    ean_int[4] = 0;
    ean_int[5] = 0;
    ean_int[6] = 0;
    x = 7;
    //Convertamos o retorno ASCII para seu inteiro respectivo
    for(int i = 0 ; i <= 4 ; i++){
        if(ean_05[i] == 48){
            ean_int[x] = 0;
         }else if(ean_05[i] == 49){
             ean_int[x] = 1;
         }else if(ean_05[i] == 50){
             ean_int[x] = 2;
         }else if(ean_05[i] == 51){
             ean_int[x] = 3;
         }else if(ean_05[i] == 52){
             ean_int[x] = 4;
         }else if(ean_05[i] == 53){
             ean_int[x] = 5;
         }else if(ean_05[i] == 54){
             ean_int[x] = 6;
         }else if(ean_05[i] == 55){
             ean_int[x] = 7;
         }else if(ean_05[i] == 56){
             ean_int[x] = 8;
         }else if(ean_05[i] == 57){
             ean_int[x] = 9;
         }else{
            printf("Um dos digitos não é númerico.\n");
            printf("Programa será encerrado!\n");
            return 0;
        }
         x = (x+1);
    }
    
    //Separamos os números que estão na posição impar
    x=0;
    for(int i = 0 ; i <= 5 ; i++){
        impar[i] = ean_int[x];
        x=(x+2);
    }

     //Separamos os números que estão na posição par
    x=1;
    for(int i = 0 ; i <= 5 ; i++){
        par[i] = ean_int[x];
        x=(x+2);
    }

    //Somamos os  números na posição impar
    soma_impar = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_impar = (soma_impar + impar[i]);
    }

    //Somamos os  números na posição par
    soma_par = 0;
    for(int i = 0 ; i <= 5 ; i++){
        soma_par = (soma_par + par[i]);
    }
    //Multiplicamos por 3x e imprimimos o resultado
    soma_par = (soma_par * 3);

    //Somamos o resultado de impares e pares
    soma_total = (soma_impar + soma_par);

    //Fazemos a divisão por modulo % 10 para obter o resto da divisão e subtrair 10
    divisao = (soma_total % 10);
    if(divisao == 0){
        digito = 0;
    }else{
        digito = (10 - divisao);
    }

    //Retornamos para função main o digito
    return digito;
}
