#!/bin/bash

#echo -e "\033[01;32m[INFO        ] - .\033[00;37m"
#echo -e "\033[01;33m[WARNING     ] - .\033[00;37m"
#echo -e "\033[01;31m[ERROR       ] - .\033[00;37m"


MYSQL_HOST="10.106.68.78"
MYSQL_DB="srvremoto"
MYSQL_USER="dba"
FILE_TEMP="/tmp/resultado.txt"
IP_SERVER=`hostname -I | awk -F " " '{print $1}'`
LOG="/var/log/control_bots.log"
DEBUG="/var/log/debug_control_bots.log"

set -x
exec 1>> $DEBUG
exec 2>> $DEBUG

function main(){
    echo "################################################# [$(date +%Y%m%d-%H%M%S)]::[START TASK] #################################################." >> $LOG
    execTaskOnTime
    echo "################################################# [$(date +%Y%m%d-%H%M%S)]::[END   TASK] #################################################." >> $LOG
}

function execTaskOnTime(){
    # Hora atual do servidor    
    H=`date +%H`
    # Minuto atual do servidor
    M=`date +%M`

    # Sintexe MYSQL
    MYSQL_CMD="SELECT id, responsible_server_ip, name_bot, hour_execution, minute_execution, script_name, local_script, name_log_script, local_log_script, DATE_FORMAT(date_time_previous_execution, '%Y%m%d%H%m%S') AS date_time_previous_execution, DATE_FORMAT(date_time_last_execution, '%Y%m%d%H%m%S') AS date_time_last_execution, status_bot FROM tb_control_bots WHERE responsible_server_ip = '$IP_SERVER';"
    # Comando MYSQL
    mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e "$MYSQL_CMD" > $FILE_TEMP

    # Loop de leitura dos dados obtidos do servidor
    while read id responsible_server_ip name_bot hour_execution minute_execution script_name local_script name_log_script local_log_script date_time_previous_execution date_time_last_execution status_bot
    do
        hour_execution=`printf "%02d" $hour_execution`
        minute_execution=`printf "%02d" $minute_execution`
        
        if [ $status_bot -eq 1 ]; then
            # Verifica se a hora do servidor é compatível com alguma tarefa agenda
            if [ $H -eq $hour_execution ]; then
                echo -e "\033[01;32m[INFO        ] - Tarefa agendada para executar ['$script_name']::['$hour_execution:$minute_execution'].\033[00;37m"
                echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Tarefa agendada para executar ['$script_name']::['$hour_execution:$minute_execution']." >> $LOG
                # Verifica se o minuto tem alguma tarefa agendada
                if [ $M -eq $minute_execution ]; then
                    echo -e "\033[01;33m[WARNING     ] - Executando tarefa agendada ['$script_name']:['$hour_execution:$minute_execution'].\033[00;37m"
                    echo "[$(date +%Y%m%d-%H%M%S)]:[WARNING     ] - Executando tarefa agendada ['$script_name']:['$hour_execution:$minute_execution']." >> $LOG
                    # Verificando se tarefa agenda já não está execução
                    #pid=`ps aux | grep -a -i "bash -x $local_script/$script_name" | head -n1 | awk '$11=="bash"' | tail -n1 | awk -F " " '{print $2}'`
                    count_pid=`ps aux | grep -a -i "bash -x $local_script/$script_name" | awk -F " " '{print $11}' | grep bash | wc -l`
                    #binario=`ps aux | grep -a -i "bash -x $local_script/$script_name" | head -n1 | awk '$11=="bash"' | tail -n1 | awk -F " " '{print $11}'`
                    if [ $count_pid -ge 1 ]; then
                        echo -e "\033[01;33m[WARNING     ] - Tarefa ['$script_name'] já em execução.\033[00;37m"
                        echo "[$(date +%Y%m%d-%H%M%S)]:[WARNING     ] - Tarefa ['$script_name'] já em execução." >> $LOG
                    else
                        echo -e "\033[01;32m[INFO        ] - Executando tarefa ['$script_name']::['$hour_execution:$minute_execution'].\033[00;37m"
                        echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Executando tarefa ['$script_name']::['$hour_execution:$minute_execution']." >> $LOG
                        nohup bash -x $local_script/$script_name > $local_log_script/$name_log_script 2>&1 &
                        if [ "$date_time_last_execution" == "NULL" ]; then
                            MYSQL_CMD="UPDATE tb_control_bots SET date_time_last_execution = NOW() WHERE id = $id"
                            mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e "$MYSQL_CMD"
                            echo -e "\033[01;32m[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_last_execution'].\033[00;37m"
                            echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_last_execution']." >> $LOG
                        else
                            MYSQL_CMD="UPDATE tb_control_bots SET date_time_previous_execution = '$date_time_last_execution', date_time_last_execution = NOW() WHERE id = $id"
                            mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e "$MYSQL_CMD"
                            echo -e "\033[01;32m[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_previous_execution'].\033[00;37m"
                            echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_previous_execution']." >> $LOG
                        fi
                    fi
                elif [ $minute_execution -eq 0 ] && [ $M -eq 0 ]; then
                    echo -e "\033[01;32m[INFO        ] - Tarefa agendada para executar ['$script_name']::['$hour_execution:$minute_execution'].\033[00;37m"
                    echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Tarefa agendada para executar ['$script_name']::['$hour_execution:00']." >> $LOG
                    # Verificando se tarefa agenda já não está execução
                    #pid=`ps aux | grep -a -i "bash -x $local_script/$script_name" | head -n1 | awk '$11=="bash"' | tail -n1 | awk -F " " '{print $2}'`
                    count_pid=`ps aux | grep -a -i "bash -x $local_script/$script_name" | awk -F " " '{print $11}' | grep bash | wc -l`
                    #binario=`ps aux | grep -a -i "bash -x $local_script/$script_name" | head -n1 | awk '$11=="bash"' | tail -n1 | awk -F " " '{print $11}'`
                    if [ $count_pid -ge 1 ]; then
                        echo -e "\033[01;33m[WARNING     ] - Tarefa ['$script_name'] já em execução.\033[00;37m"
                        echo "[$(date +%Y%m%d-%H%M%S)]:[WARNING     ] - Tarefa ['$script_name'] já em execução." >> $LOG
                    else
                        echo -e "\033[01;32m[INFO        ] - Executando tarefa ['$script_name']::['$hour_execution:$minute_execution'].\033[00;37m"
                        echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Executando tarefa ['$script_name']::['$hour_execution:$minute_execution']." >> $LOG
                        nohup bash -x $local_script/$script_name > $local_log_script/$name_log_script 2>&1 &
                        if [ "$date_time_last_execution" == "NULL" ]; then
                            MYSQL_CMD="UPDATE tb_control_bots SET date_time_last_execution = NOW() WHERE id = $id"
                            mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e "$MYSQL_CMD"
                            echo -e "\033[01;32m[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_last_execution'].\033[00;37m"
                            echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_last_execution']." >> $LOG
                        else
                            MYSQL_CMD="UPDATE tb_control_bots SET date_time_previous_execution = '$date_time_last_execution', date_time_last_execution = NOW() WHERE id = $id"
                            mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e "$MYSQL_CMD"
                            echo -e "\033[01;32m[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_previous_execution'].\033[00;37m"
                            echo "[$(date +%Y%m%d-%H%M%S)]:[INFO        ] - Tarefa ['$script_name'] executada, atualizando informações no banco de dados ['date_time_previous_execution']." >> $LOG
                        fi
                    fi
                else
                    echo -e "\033[01;33m[WARNING     ] - Nenhuma tarefa agenda para script ['$script_name'].\033[00;37m"
                    echo "[$(date +%Y%m%d-%H%M%S)]:[WARNING     ] - Nenhuma tarefa agenda para script ['$script_name']." >> $LOG
                fi
            else
                echo -e "\033[01;33m[WARNING     ] - Nenhuma tarefa agenda para script ['$script_name'].\033[00;37m"
                echo "[$(date +%Y%m%d-%H%M%S)]:[WARNING     ] - Nenhuma tarefa agenda para script ['$script_name']." >> $LOG
            fi
        else
            echo -e "\033[01;33m[WARNING     ] - Tarefa ['$script_name'] atualmenete está desativada.\033[00;37m"
            echo "[$(date +%Y%m%d-%H%M%S)]:[WARNING     ] - Tarefa ['$script_name'] atualmenete está desativada." >> $LOG
        fi
    done < $FILE_TEMP
}

main