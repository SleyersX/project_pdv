from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import *
from time import sleep
import os
import logging

# Configurando log
arquivo_de_log = os.getcwd() + os.sep + 'log' + os.sep + 'infolog.log'
logging.basicConfig(
    filename=arquivo_de_log,
    level=logging.WARNING,
    format='%(levelname)s %(asctime)s: %(message)s',
    datefmt='%d/%m/%Y %H:%M:%S'
)


class EnviaSefaz:
    def __init__(self):
        chrome_options = Options()
        chrome_options.add_argument('--lang=pt-BR')
        self.driver = webdriver.Chrome(executable_path=
                                       os.getcwd() + os.sep + 'chromedriver' + os.sep + 'chromedriver.exe',
                                       options=chrome_options)

        self.wait = WebDriverWait(
            driver=self.driver,
            timeout=10,
            poll_frequency=1,
            ignored_exceptions=[
                NoSuchElementException,
                ElementNotVisibleException,
                ElementNotSelectableException,
            ]
        )

        # self.lote = os.getcwd() + os.sep + 'arquivos_a_enviar' + os.sep + 'LOTE181.zip'

    def coleta_arquivos(self):
        arquivos_a_enviar = os.getcwd() + os.sep + 'arquivos_a_enviar'
        lista_arquivos = os.listdir(arquivos_a_enviar)

        arquivos = [arquivos_a_enviar, lista_arquivos]
        return arquivos

    def iniciar(self):
        self.driver.get('https://satsp.fazenda.sp.gov.br/COMSAT/Account/LoginSSL.aspx?ReturnUrl=%2fCOMSAT')
        sleep(60)
        self.navegar()

    def navegar(self):
        arquivos = self.coleta_arquivos()
        arquivos_a_enviar = arquivos[0]
        lista_arquivos = arquivos[1]

        for arquivo in lista_arquivos:
            lote_a_enviar = f"{arquivos_a_enviar}\{arquivo}"

            def realiza_envio_sefaz():
                # Abrindo interface de envio de arquivos
                envio_arquivos = self.wait.until(
                    expected_conditions.element_to_be_clickable((By.XPATH, '//*[@id="conteudo_fupArquivoCupons"]'))
                )
                envio_arquivos.send_keys(lote_a_enviar)
                sleep(2)

                # Clicando no botão enviar
                botao_enviar = self.wait.until(
                    expected_conditions.element_to_be_clickable((By.XPATH, '//*[@id="conteudo_btnEnviarArquivoCupons"]'))
                )
                botao_enviar.click()
                sleep(2)

                # Erro no formato dos xmls
                try:
                    # Confirmando envio
                    botao_confirmar = self.wait.until(
                        expected_conditions.element_to_be_clickable(
                            (By.XPATH, '//*[@id="container-assinatura"]/p[2]/input[2]'))
                    )
                    botao_confirmar.click()
                    sleep(1)

                    # Confirmando o envio com sucesso
                    botao_enviado_com_sucesso = self.wait.until(
                        expected_conditions.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div/button'))
                    )
                    botao_enviado_com_sucesso.click()
                    sleep(2)
                except TimeoutException:
                    erro_format_xml = self.wait.until(
                        expected_conditions.element_to_be_clickable(
                            (By.XPATH, '//*[contains(text(), "SGRS@T: Alerta")]'))
                    )
                    if erro_format_xml:
                        botao_ok = self.wait.until(
                            expected_conditions.element_to_be_clickable(
                                (By.XPATH, '/html/body/div[2]/div[3]/div/button'))
                        )
                        botao_ok.click()
                        sleep(2)
                        return 1
                return 0

            status_envio = realiza_envio_sefaz()
            if status_envio == 0:
                print(f'Lote {arquivo} enviado')
            if status_envio == 1:
                logging.warning(f'[{arquivo}] falha de formatação !!!')
                print(f"Lote {arquivo} apresentou problemas de formatação, verificar")

    def teste(self):
        self.driver.close()
        arquivos = self.coleta_arquivos()
        arquivos_a_enviar = arquivos[0]
        lista_arquivos = arquivos[1]
        print(lista_arquivos)
        for arquivo in lista_arquivos:
            lote_a_enviar = f"{arquivos_a_enviar}\{arquivo}"

            def test2():
                print(lote_a_enviar)


envio = EnviaSefaz()
envio.iniciar()
envio.coleta_arquivos()
# envio.teste()


"""
Erros a tratar

Alerta de erro no formato dos xmls
tag do alerta = //*[@id="ui-id-1"], valor do campo -> SGRS@T: Alerta
campo do alerta = //*[@id="dialog-modal"], valor do campo -> O arquivo não tem a formatação correta. 
botão de ok = /html/body/div[2]/div[3]/div/button

saida de exessão: TimeoutException

volta para tela inicial
"""