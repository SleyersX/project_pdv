/*
 * numerologia.h
 *
 *  Created on: 24 de out de 2019
 *      Author: wam001br
 */

/*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*****************************************************************/
#ifndef NUMEROLOGIA_H_
#define NUMEROLOGIA_H_
#define MAXCARACTERE 100
#define MAXCARACTEREBUFFER 500
char BUFFER_ENTRADA[MAXCARACTEREBUFFER];
char BUFFER_INTERNO[5];
int N_CRT, SOMA=0, BUFFER_SAIDA=0, BUFFER_RESULT=0, BUFFER_TEMP=0,TEMP_RETURN=0;
void fn_exibirNome(int TEMP_NCRT);
int fn_gerarNumeroDaSorte(int TEMP_SOMA);
void fn_validaCaractere(int TEMP_NCRT);
void fn_validaTamString(int TEMP_NCRT);
#endif /* NUMEROLOGIA_H_ */


/*****************************************************************/

int fn_gerarNumero(int TEMP_NCRT){

	fn_validaTamString(TEMP_NCRT);
	fn_validaCaractere(TEMP_NCRT);

	fn_exibirNome(TEMP_NCRT);
	memset(&SOMA,0,sizeof(int));
	memset(&BUFFER_SAIDA,0,sizeof(int));
	for (int i = 0; i < (TEMP_NCRT-1); ++i) {
		if(BUFFER_ENTRADA[i]==32){
			SOMA=SOMA+9;
		}else if(toupper(BUFFER_ENTRADA[i])<=73){
			SOMA=SOMA+(toupper(BUFFER_ENTRADA[i])-64);
		}else if(toupper(BUFFER_ENTRADA[i])>=74 && toupper(BUFFER_ENTRADA[i])<=82){
			SOMA=SOMA+(toupper(BUFFER_ENTRADA[i])-73);
		}else if(toupper(BUFFER_ENTRADA[i])>=83 && toupper(BUFFER_ENTRADA[i])<=90){
			SOMA=SOMA+(toupper(BUFFER_ENTRADA[i])-82);
		}
	}
	BUFFER_SAIDA=fn_gerarNumeroDaSorte(SOMA);

	return BUFFER_SAIDA;
}

void fn_exibirNome(int TEMP_NCRT){
	for (int i = 0; i < (TEMP_NCRT-1); ++i) {
		if(BUFFER_ENTRADA[i]==32){
			printf("%c->%d->%d\n", BUFFER_ENTRADA[i],BUFFER_ENTRADA[i],9);
		}else if(toupper(BUFFER_ENTRADA[i])<=73){
			printf("%c->%d->%d\n", toupper(BUFFER_ENTRADA[i]),toupper(BUFFER_ENTRADA[i]),toupper(BUFFER_ENTRADA[i])-64);
		}else if(toupper(BUFFER_ENTRADA[i])>=74 && toupper(BUFFER_ENTRADA[i])<=82){
			printf("%c->%d->%d\n", toupper(BUFFER_ENTRADA[i]),toupper(BUFFER_ENTRADA[i]),toupper(BUFFER_ENTRADA[i])-73);
		}else if(toupper(BUFFER_ENTRADA[i])>=83 && toupper(BUFFER_ENTRADA[i])<=90){
			printf("%c->%d->%d\n", toupper(BUFFER_ENTRADA[i]),toupper(BUFFER_ENTRADA[i]),toupper(BUFFER_ENTRADA[i])-82);
		}
	}
}

int fn_gerarNumeroDaSorte(int TEMP_SOMA){

	do{
		memset(&BUFFER_TEMP,0,sizeof(int));
		snprintf(BUFFER_INTERNO, sizeof(TEMP_SOMA),"%d",TEMP_SOMA);
		N_CRT=strlen(BUFFER_INTERNO);
		for(int i = 0; i < N_CRT;i++){
			BUFFER_TEMP=BUFFER_TEMP+(BUFFER_INTERNO[i]-48);
		}
		memset(&TEMP_SOMA,0,sizeof(TEMP_SOMA));
		TEMP_SOMA=TEMP_SOMA+BUFFER_TEMP;
		memset(&BUFFER_INTERNO,0,sizeof(BUFFER_INTERNO));
		memset(&N_CRT,0,sizeof(int));
	}while(BUFFER_TEMP>9);

	return BUFFER_TEMP;
}

void fn_validaCaractere(int TEMP_NCRT){
	for(int i = 0; i < TEMP_NCRT; i++){
		if(isalpha(BUFFER_ENTRADA[i]) || isspace(BUFFER_ENTRADA[i])){
			TEMP_RETURN+=TEMP_RETURN;
		}else{
			printf("Detectado caractere não permitido.\nNão utilizar acentuaçãoes, caracteres especias ou números.");
			exit(1);
		}
	}
}

void fn_validaTamString(int TEMP_NCRT){
	if(TEMP_NCRT>MAXCARACTERE){
		printf("Digite até %d caracteres.", MAXCARACTERE);
		exit(1);
	}
}
