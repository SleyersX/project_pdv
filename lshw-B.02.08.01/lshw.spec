# Conditional build (--with/--without option)
#   --without gui

%define name lshw
%define version B.02.08.01
%define release 1

Summary: HardWare LiSter
Name: %{name}
Version: %{version}
Release: %{release}
Vendor: Lyonel Vincent <lyonel@ezix.org>
Source: %{name}-%{version}.tar.gz
URL: http://www.ezix.org/software/lshw.html
License: GPL
Group: Applications/System
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: /usr/bin/install

%description
lshw (Hardware Lister) is a small tool to provide detailed informaton on the hardware configuration of the machine. It can report exact memory configuration, firmware version, mainboard configuration, CPU version and speed, cache configuration, bus speed, etc. on DMI-capable x86 systems and on some PowerPC machines (PowerMac G4 is known to work).

Information can be output in plain text, XML or HTML.

For detailed information on lshw features and usage, please see the
included documentation or go to the lshw Web page,
http://www.ezix.org/software/lshw.html

%if %{?_with_gui:1}%{!?_with_gui:0}
%package gui
Summary: HardWare LiSter (GUI version)
Group: Applications/System
Requires: %{name} >= %{version}
Requires: gtk2 >= 2.4
BuildRequires: gtk2-devel >= 2.4
BuildRequires: /usr/bin/install

%description gui
lshw (Hardware Lister) is a small tool to provide detailed informaton on the hardware configuration of the machine. It can report exact memory configuration, firmware version, mainboard configuration, CPU version and speed, cache configuration, bus speed, etc. on DMI-capable x86 systems and on some PowerPC machines (PowerMac G4 is known to work).

This package provides a graphical user interface to display hardware information.

For detailed information on lshw features and usage, please see the
included documentation or go to the lshw Web page,
http://www.ezix.org/software/lshw.html

%endif

%prep
%setup

%build
%{__make} %{?_smp_mflags} \
	PREFIX="%{_prefix}" \
	SBINDIR="%{_sbindir}" \
	MANDIR="%{_mandir}" \
	DATADIR="%{_datadir}" \
	all
%if %{?_with_gui:1}%{!?_with_gui:0}
%{__make} %{?_smp_mflags} \
	PREFIX="%{_prefix}" \
	SBINDIR="%{_sbindir}" \
	MANDIR="%{_mandir}" \
	DATADIR="%{_datadir}" \
	gui
%endif

%install
%{__rm} -rf "%{buildroot}"

%{__make} \
	DESTDIR="%{buildroot}" \
	PREFIX="%{_prefix}" \
	SBINDIR="%{_sbindir}" \
	MANDIR="%{_mandir}" \
	DATADIR="%{_datadir}" \
	install
%if %{?_with_gui:1}%{!?_with_gui:0}
%{__make} \
	DESTDIR="%{buildroot}" \
	PREFIX="%{_prefix}" \
	SBINDIR="%{_sbindir}" \
	MANDIR="%{_mandir}" \
	DATADIR="%{_datadir}" \
	install-gui
%endif

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root, 0755)
%doc README COPYING docs/TODO docs/Changelog docs/lshw.xsd
%attr(555,bin,bin) %{_sbindir}/lshw
%doc %{_mandir}/man?/*
%{_datadir}/lshw/

%if %{?_with_gui:1}%{!?_with_gui:0}
%files gui
%defattr(-,root,root, 0755)
%doc COPYING
%attr(555,bin,bin) %{_sbindir}/gtk-lshw
%endif

