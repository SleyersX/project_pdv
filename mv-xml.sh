#!/usr/bin

PATH_SERVIDOR_DFW="/srv/Debian6.0/srv/servidorDFW/"
PATH_XML="$PATH_SERVIDOR_DFW/sat"
PATH_REJECTED="$PATH_XML/rejected"
PATH_SEND="$PATH_XML/send"
PATH_ANSW="$PATH_XML/answ"
LOG="/tmp/mv-xml.log"

COUNT_XML_REJECTED=`ls -ltr $PATH_REJECTED/CFe* | wc -l`
if [ $COUNT_XML_REJECTED -ge 1 ]; then
    mv -vf $PATH_REJECTED/CFe* $PATH_ANSW/
    if [ $? -eq 0 ]; then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:Arquivos movidos com sucesso [$COUNT_XML_REJECTED]." >> $LOG
    else
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:Erro ao mover arquivos [$COUNT_XML_REJECTED]." >> $LOG
    fi
else
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:Sem arquivos na pasta rejected." >> $LOG
fi