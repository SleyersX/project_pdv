#!/bin/bash
#
# Autor: Walter Moura
# Data Criação: 04/09/2020
# Data Modificação: 04/09/2020
#
#   Script responsável por ajustar a configuração das lojas cadastradas no MGV6, 
# ajustando as informações na tabela de configuração, como digitos ean, transmissao de carga...
#
# Version 1.0
# - Gerar lista de lojas cadastradas no servidor (sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD" -Q "SELECT [LOJ_CODIGO] FROM [MGV6_GLOBAL].[dbo].[tbLojas];")
# - Ler a lista de lojas
# - Update banco de dados (sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD" -Q "UPDATE [MGV6_0541].[dbo].[tbConfiguracao] SET[CFG_TIPO_TX_CARGA_AUTOMATICA] = 0, [CFG_APRESENTACAO_EAN_13] = 1, [CFG_DIGITOS_CODIGO_EAN_13] = 6, [CFG_ENDERECO_IP_CARGA_REMOTA]= 'BRPDCVW12037', [CFG_PASTA_COMUNICACAO_MIT_MANAGER] = 'D:\Program Files (x86)\Toledo do Brasil\MGV6\',   [CFG_CARGA_REMOTA_ALTERADOS] = 0, [CFG_CARGA_REMOTA_COMPLETA] = 1;")
#
# Exemplos:
# - Consulta lista de lojas
#   sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD" -Q "SELECT [LOJ_CODIGO] FROM [MGV6_GLOBAL].[dbo].[tbLojas];"
#   sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD02" -Q "SELECT [LOJ_CODIGO] FROM [MGV6_GLOBAL].[dbo].[tbLojas];"
#
# - Updates tabelas
#   sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD" -Q "UPDATE [MGV6_0541].[dbo].[tbConfiguracao] SET [CFG_ENDERECO_IP_CARGA_REMOTA] = 'BRPDCVW12037', [CFG_PASTA_COMUNICACAO_MIT_MANAGER] = 'D:\Program Files (x86)\Toledo do Brasil\MGV6\';"
#
#   sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD" -Q "UPDATE [MGV6_0541].[dbo].[tbConfiguracao] SET[CFG_TIPO_TX_CARGA_AUTOMATICA] = 0, [CFG_APRESENTACAO_EAN_13] = 1, [CFG_DIGITOS_CODIGO_EAN_13] = 6, [CFG_ENDERECO_IP_CARGA_REMOTA]= 'BRPDCVW12037', [CFG_PASTA_COMUNICACAO_MIT_MANAGER] = 'D:\Program Files (x86)\Toledo do Brasil\MGV6\',   [CFG_CARGA_REMOTA_ALTERADOS] = 0, [CFG_CARGA_REMOTA_COMPLETA] = 1;"
#
#   sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD" -Q "UPDATE [MGV6_0006].[dbo].[tbConfiguracao] SET[CFG_TIPO_TX_CARGA_AUTOMATICA] = 0, [CFG_APRESENTACAO_EAN_13] = 1, [CFG_DIGITOS_CODIGO_EAN_13] = 6, [CFG_ENDERECO_IP_CARGA_REMOTA]= 'BRPDCVW12037', [CFG_PASTA_COMUNICACAO_MIT_MANAGER] = 'D:\Program Files (x86)\Toledo do Brasil\MGV6\',   [CFG_CARGA_REMOTA_ALTERADOS] = 0, [CFG_CARGA_REMOTA_COMPLETA] = 1, [CFG_ENVIA_CARGA_SIMULTANEA] = 'False';"
#
# - Update combinado com laço de repetição
#   for i in $(cat lojas_mgv6.txt ) ; do SHOP=`echo $i | tr -d ' '`; SHOP=`printf "%04d\n" $SHOP`; sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD02" -Q "UPDATE [MGV6_$SHOP].[dbo].[tbConfiguracao] SET[CFG_TIPO_TX_CARGA_AUTOMATICA] = 0, [CFG_APRESENTACAO_EAN_13] = 1, [CFG_DIGITOS_CODIGO_EAN_13] = 6, [CFG_ENDERECO_IP_CARGA_REMOTA]= 'SRVMGVAPP', [CFG_PASTA_COMUNICACAO_MIT_MANAGER] = 'C:\Program Files (x86)\Toledo do Brasil\MGV6\',   [CFG_CARGA_REMOTA_ALTERADOS] = 0, [CFG_CARGA_REMOTA_COMPLETA] = 1, [CFG_ENVIA_CARGA_SIMULTANEA] = 'False';"; done

#for i in $(cat lojas_mgv6.txt ) ; do SHOP=`echo $i | tr -d ' '`; SHOP=`printf "%04d\n" $SHOP`; sqlcmd -U sa -P Di@Br@silTpvs -S "10.106.68.196\SQL_MGV6BD" -Q "SELECT [BAL_CODIGO],[BAL_ENDERECO_IP],[BAL_DMP_VERSAO_BALANCA],[BAL_DMP_ENDERECO_IP],[BAL_DMP_MAC_ADDRESS_WIFI],[BAL_DMP_MASK],[BAL_DMP_GATEWAY],[BAL_DMP_IPPORTA],[BAL_DMP_CHAVECRIPTO],[BAL_DMP_IP_GW] [MGV6_$SHOP].[dbo].[tbBalanca];"; done

#SELECT [BAL_CODIGO],[BAL_ENDERECO_IP],[BAL_DMP_VERSAO_BALANCA],[BAL_DMP_ENDERECO_IP],[BAL_DMP_MAC_ADDRESS_WIFI],[BAL_DMP_MASK],[BAL_DMP_GATEWAY],[BAL_DMP_IPPORTA],[BAL_DMP_CHAVECRIPTO],[BAL_DMP_IP_GW] [MGV6_$SHOP].[dbo].[tbBalanca]

# Variveis Globais

SCRIPT=`basename $0`
USERACTUAL=$(grep $EUID /etc/group | awk -F ":" '{print $1}')
PATH_USER="~/sqlcmd/"
PATH_LOG="$PATH_USER/log"
LOG="$PATH_LOG/$SCRIPT.log"
DEBUG="$PATH_LOG/$SCRIPT.debug"
SQL_USER="sa"
SQL_PASS="Di@Br@silTpvs"
SQL_SERVER="10.106.68.196"
SQL_INSTANCIA="SQL_MGV6BD"

function fnVerficaDiretorios(){

    if [ ! -d $PATH_USER ]; then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VERIFICADIRETORIOS:Diretório ['$PATH_USER'] não existe."
        mkdir -p $PATH_USER
        RET=$?
        if [ $RET -eq 0 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VERIFICADIRETORIOS:Diretório ['$PATH_USER'] criado com sucesso."
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VERIFICADIRETORIOS:Erro ao criar diretório ['$PATH_USER'] -> ['$RET']."
            exit 1
        fi
    fi

}

function main(){

    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Inicializando programa."
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Call function check directory."
    fnVerficaDiretorios
    
}

main

