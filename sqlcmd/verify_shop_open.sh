#!/bin/bash

SSH_USER="root"
SSH_PASS="root"
SSH_PORT="10001"
SSH_CONFIG="-oConnectTimeout=1 -oConnectionAttempts=3 -oKexAlgorithms=+diffie-hellman-group1-sha1 -oStrictHostKeyChecking=no"

MYSQL_HOST="10.106.68.78"
MYSQL_USER="dba"
MYSQL_PASS=""
MYSQL_BD="srvremoto"

SQL_HOST="10.106.68.196\SQL_MGV6BD"
SQL_USER="sa"
SQL_PASS="Di@Br@silTpvs"
SQL_BD="[MGV6_GLOBAL].[dbo].[tbLojas]"

for i in $(sqlcmd -U $SQL_USER -P $SQL_PASS -S "$SQL_HOST" -Q "SELECT [LOJ_CODIGO] FROM $SQL_BD;")
do 
    if [[ $i = ?(+|-)+([0-9]) ]]; then 
        ip=`mysql -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT ip FROM tb_ip WHERE loja = $i"` 
    fi
    if [ $ip ]; then
        SHOP_1=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip '. /confdia/bin/setvari ; echo ${NUMETIEN}'`
        RET=$?
        if [ $RET -eq 0 ]; then
            SHOP_0=`printf "%05d" $i`
            if [ "$SHOP_0" != "$SHOP_1" ];then
                echo "[$SHOP_0] != [$SHOP_1]"
            fi
        else
            echo "Erro ao conectar com a loja [$i]"
        fi    
    fi
done