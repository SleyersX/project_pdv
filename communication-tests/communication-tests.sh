#!/bin/bash
#
# Scritp testes SDWAN
#
#ping scopetef1 -c 4
#ping pointsystem -c 4
#ping nfceweb -c 4
#ping nfceweb1 -c 4
#ping servidorcupones -c 4
#ping brconcen1 -c 4
#ping brconcen2 -c 4
#ping pafbackup -c 4
#openssl s_client -debug -connect wssatsp.fazenda.sp.gov.br:443

USERACTUAL=$(echo $USER)
PATH_USER="/tmp/$USERACTUAL"
PATH_LOG="$PATH_USER/communication-tests/log"
LOG="$PATH_LOG/error.log"
DEBUG="$PATH_LOG/debug.log"
SCRIPT=`basename $0`
BD="$PATH_USER/.baseDeDados.db"
ARQ_PRINT="$PATH_USER/communication-tests/arq_print.txt"

SSH_USER="root"
SSH_PASS="root"
SSH_PORT="10001"
SSH_CONFIG="-oConnectTimeout=1 -oConnectionAttempts=3 -oKexAlgorithms=+diffie-hellman-group1-sha1 -oStrictHostKeyChecking=no"

MYSQL_HOST="10.106.68.78"
MYSQL_USER="dba"
MYSQL_PASS=""
MYSQL_BD="srvremoto"

function main(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:Main:Executando o programa." >> $LOG
    fnValidaLog
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:Main:Obtendo número da loja." >> $LOG
    SHOP=$(dialog --stdout --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --form "" 11 50 0 "Informe a loja:" 1 1 "" 1 16 6 4)
	[ "$SHOP" ] || { dialog --ok-label "OK" --title "COMMUNICATION TESTS - 1.0" --msgbox "ERROR: Não foi informado a loja." 5 70 ; sleep 1 ; exit ; }
    if [[ $SHOP = ?(+|-)+([0-9]) ]]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):Main:Valor ['$SHOP'] é do tipo número, incluímos 0 a esquerda." >> $LOG
        SHOP=`printf "%05d" $SHOP`
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):Main:Valor ['$SHOP'] não é do tipo númerico, será ignorado." >> $LOG
    fi
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:Main:Call function ['fnGetIP']." >> $LOG
    fnGetIP
    if [ $RET -eq 0 ]; then
        fnValidateConnection
        if [ $RET -eq 0 ]; then
            fnCommunicationTests    
        else
            dialog --sleep 2 --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --infobox "\nErro conectar com a lojas -> ['$RET']." 5 70
            exit 1
        fi
    else
        dialog --sleep 2 --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --infobox "\nLoja não encontrada no banco de dados." 5 70
        clear
        dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --yesno "Deseja informar o ip manualmente?" 0 0
        decisao=$?
        case $decisao in
            "0")
                read -p " " ipss
                ipss=$(dialog --stdout --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --form "Inclusão de dados" 11 55 0 "Digite IP (XXX.XXX.XXX.XXX)=" 1 1 "" 1 30 15 15)
                # Validadmos se as variáveis estão vazias
                [ "$ipss" ] || { dialog --sleep 2 --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --infobox "\nERROR: Não foi detectado nenhum caracter." 5 70 ; sleep 1 ; main ; }
                ip=$ipss
            ;;
            "1"|"255")
                main  
            ;;
            *)
                dialog --sleep 2 --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --infobox "\nOpção inválida." 5 70
                main
        esac
        fnValidateConnection
        if [ $RET -eq 0 ]; then
            fnCommunicationTests    
        else
            dialog --sleep 2 --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --infobox "\nErro conectar com a lojas -> ['$RET']." 5 70
            exit 1
        fi
    fi
}

function fnValidaLog(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnfnValidaLog:Iniciando função ['fnfnValidaLog']." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Verificando se o diretório ['$PATH_LOG'] existe." >> $LOG
    if [ -d $PATH_LOG ]; then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnfnValidaLog:Diretório ['$PATH_LOG'] já existe." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Verificando se o arquivo ['$LOG'] é maior que 200mb." >> $LOG
        KB=`du -hsk $LOG | awk -F " " '{print $1;}'`
        if [ $KB -ge 200000 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Arquivo ['$LOG'] é maior que 200mb ['$KB kb']." >> $LOG
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Verificando a quantidades de arquivos ['$LOG*']." >> $LOG
            COUNT=`ls -ltr $LOG* | wc -l`
            if [ $COUNT -gt 1 ]; then
                if [ $COUNT -gt 5 ]; then
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Quantidades de arquivos ['$LOG*']:['$COUNT']." >> $LOG
                    rm -vf $LOG.1
                    for x in $(seq 2 5);
                    do
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Movendo arquivo ['$LOG.$x'] -> ['$LOG.$(( x - 1))']." >> $LOG
                        mv -vf $LOG.$x $LOG.$(( x - 1))
                    done
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Movendo arquivo ['$LOG'] -> ['$LOG.5']" >> $LOG
                    mv -vf $LOG $LOG.5
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Movendo arquivo ['$LOG'] -> ['$LOG.$COUNT']." >> $LOG
                    mv -vf $LOG $LOG.$COUNT
                fi
            else
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Quantidades de arquivos ['$LOG*']:['$COUNT']." >> $LOG
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Movendo arquivo ['$LOG'] -> ['$LOG.$COUNT']." >> $LOG
                mv -vf $LOG $LOG.$COUNT
            fi
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Arquivo ['$LOG'] é menor que 200mb ['$KB kb']." >> $LOG
        fi
    else
        mkdir -p $PATH_LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:fnValidaLog:Diretório ['$PATH_LOG'] não existe." >> $LOG
    fi
}

function fnGetIP(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`mysql --connect-timeout=5 -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP'"`
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
            ip=`mysql -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP'"`            
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Retorno IP -> ['$ip']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Erro ao obter IP loja ['$SHOP'] problema ao acessar banco de dados MySQL." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Saindo por contigencia." >> $LOG
        EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Retorno IP -> ['$ip']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GetIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    fi
}

function fnValidateConnection(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:ValidateConnection:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:ValidateConnection:Validando conexão ['$SHOP:$ip']." >> $LOG
    sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT -l$SSH_USER $ip exit
    RET=$?
}

function fnCommunicationTests(){
    BAR_SIZE="######################################################"
    PERC=0
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:CommunicationTests:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:CommunicationTests:Obtendo número de PDVs [$SHOP]:[$ip]." >> $LOG
    NTPVS=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip '. /confdia/bin/setvari; echo $NUMETPVS'`
    RET=$?
    if [ $RET -eq 0 ]; then
        FORMAT_DATA=`date "+%d/%m/%Y" --date="$DATA_FILTRO"`
        
        echo "******************************************** COMMUNICATION TESTS [$SHOP]::[$FORMAT_DATA] ********************************************" > $ARQ_PRINT
        printf '\n' >> $ARQ_PRINT
        printf '+----------+----------+---------------+----------+----------+--------------------+----------+----------+---------------+----------+\n' >> $ARQ_PRINT
        printf '|%-10s|%-10s|%-15s|%-10s|%-10s|%-20s|%-10s|%-10s|%-15s|%-10s|\n' "PDV" "SCOPETEF1" "POINTSYSTEM" "NFCEWEB" "NFCEWEB1" "SERVIDORCUPONES" "BRCONCEN1" "BRCONCEN2" "PAFBACKUP" "SEFAZ" >> $ARQ_PRINT
        printf '+----------+----------+---------------+----------+----------+--------------------+----------+----------+---------------+----------+\n' >> $ARQ_PRINT 
        # Test
        for i in $(seq 1 $NTPVS)
        do
            (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i" 8 70 0
            # Valida conexão caixa
            SSH_PORT="1000$i"
            fnValidateConnection
            if [ $RET -eq 0 ]; then
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck ScopeTEF ...\n[${BAR_SIZE:0:0}] 0%" 8 70 0
            sleep 1
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck ScopeTEF ...\n[${BAR_SIZE:0:6}] 11%" 8 70 0
                SCOPETEF=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping scopetef1 -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    SCOPETEF="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck PointSystem ...\n[${BAR_SIZE:0:12}] 22%" 8 70 0
                POINTSYSTEM=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping pointsystem -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    POINTSYSTEM="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck NFCeWEB ...\n[${BAR_SIZE:0:18}] 33%" 8 70 0
                NFCEWEB=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping nfceweb -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    NFCEWEB="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck NFCeWEB1 ...\n[${BAR_SIZE:0:24}] 44%" 8 70 0
                NFCEWEB1=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping nfceweb1 -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    NFCEWEB1="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck ServidorCupones ...\n[${BAR_SIZE:0:30}] 55%" 8 70 0
                SERVIDORCUPONES=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping servidorcupones -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    SERVIDORCUPONES="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck BRConcen1 ...\n[${BAR_SIZE:0:36}] 66%" 8 70 0
                BRCONCEN1=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping brconcen1 -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    BRCONCEN1="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck BRConcen2 ...\n[${BAR_SIZE:0:42}] 77%" 8 70 0
                BRCONCEN2=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping brconcen2 -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    BRCONCEN2="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck PafBackup ...\n[${BAR_SIZE:0:48}] 88%" 8 70 0
                PAFBACKUP=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip 'ping pafbackup -c 4 > /dev/null 2>&1 ; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    PAFBACKUP="TIMEOUT"    
                fi
                (echo $PERC; \
            ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i\nCheck Sefaz ...\n[${BAR_SIZE:0:54}] 100%" 8 70 0
                SEFAZ=`sshpass -p $SSH_PASS ssh $SSH_CONFIG -p$SSH_PORT $SSH_USER@$ip '(echo :q! ) | openssl s_client -debug -connect $(echo $(echo wssatsp.fazenda.sp.gov.br:443)) > /dev/null 2>&1; if [ $? -eq 0 ]; then echo OK ; else echo NOK; fi'`
                RET=$?
                if [ $RET != 0 ]; then
                    SEFAZ="TIMEOUT"    
                fi
            else
                SCOPETEF="TIMEOUT"
                POINTSYSTEM="TIMEOUT"
                NFCEWEB="TIMEOUT"
                NFCEWEB1="TIMEOUT"
                SERVIDORCUPONES="TIMEOUT"
                BRCONCEN1="TIMEOUT"
                BRCONCEN2="TIMEOUT"
                PAFBACKUP="TIMEOUT"
                SEFAZ="TIMEOUT"
            fi
            printf '|%-10s|%-10s|%-15s|%-10s|%-10s|%-20s|%-10s|%-10s|%-15s|%-10s|\n' "PDV $i" $SCOPETEF $POINTSYSTEM $NFCEWEB $NFCEWEB1 $SERVIDORCUPONES $BRCONCEN1 $BRCONCEN2 $PAFBACKUP $SEFAZ >> $ARQ_PRINT

            PERC=$(( $PERC + $(( 100 / $NTPVS )) ))
            if [ $i -eq $NTPVS ]; then
                PERC=100
                (echo $PERC; \
                ) | dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "Processamento" --gauge "Programa em execução -> LOJA:$SHOP PDV:0$i" 8 70 0
            fi
        done

        printf '+----------+----------+---------------+----------+----------+--------------------+----------+----------+---------------+----------+\n' >> $ARQ_PRINT

        #Convertendo para UNIX
	    dos2unix $ARQ_PRINT > /dev/null 2>&1

        dialog --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --textbox $ARQ_PRINT 30 135

        exit 1
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:CommunicationTests:Erro ao obter número de PDVs [$SHOP]:[$ip]." >> $LOG
        dialog --sleep 2 --backtitle "COMMUNICATION TESTS - 1.0" --title "COMMUNICATION TESTS - 1.0" --infobox "\nErro ao obter número de PDVs [$SHOP]:[$ip]." 5 70
    fi

}
set -x
exec 2>>$DEBUG
echo "####################################### INICIO EXECUÇÃO [$(date +%d/%m/%Y-%H:%M:%S)] #######################################"
main