#!/bin/bash
# Programa para verificar os combos gravados
# e retornar uma validação sobre a gravação do combo
# 
# Author: Marcos Marin/Walter Moura
# Data Criação: 23/03/2020
#
# SQLite3
# MAEOFER2 / miraBD 100  - Maestro de Ofertas - Registro de propiedades de oferta
# MAEOFER32 / miraBD 101 - Maestro de Ofertas - Registro de Condiciones
# MAEOFER33  / miraBD 102 - Maestro de Ofertas - Registro de Regalos de la oferta
# MAEGRUP1 / miraBD 103  - Maestro de Grupos de Ofertas
# Documento para orientação
# https://docs.google.com/document/d/1bMr2jsjqvSfVAa1f-ys1TC3U72WtB3ky/edit
#
#

# Definições de variaveis 'globais'
PATH_USER="/root/testsales"
PATH_BKP="$PATH_USER/backup"
PATH_LOG="$PATH_USER/log"
ARQRET="$PATH_USER/analise_oferta.log"
ARQNAME="analise_oferta.log"
ARQARRAYFILES="$PATH_USER/.array_files.txt"
ARQARRAYFILESLOG="$PATH_USER/.array_files_log.txt"
LOG="$PATH_LOG/error.testsales.log"
LOGNAME="error.testsales.log"
BACKUP="$PATH_LOG/backup"
ARQTEMP=""
ARQSHOPS=""
ARQOFER="$PATH_USER/ofertas.txt"
ARQCODIBASE="$PATH_USER/codibase.txt"
ARQBASEREGA="$PATH_USER/baserega.txt"
PROMO_RESULT="$PATH_USER/resultado_analise_combo.txt"


# ** Mudança no algoritmo **
# Informar o código de oferta
# Após isso, mostrar quais artigos fazem parte da promoção
# Mostrar as dinâmicas

# Copiando arquivo atual para backup
function copy_file_actual(){
    cd $PATH_USER
    mv resultado_analise_combo.txt $BACKUP/resultado_analise_combo_$(date +%y%m%d-%H%M%S).txt
}

# Criando arquivo de ofertas
function create_oferta(){
    /confdia/bin/miraBD 100 | awk -F "|" '{print $1}' > $ARQOFER
}


# Tabela Base Regalo
#VALOR | DESCRIPCIÓN
# 1    | Ticket
# 2    | Sección
# 3    | Familia
# 4    | Subfamilia
# 5    | Producto
# 6-7  | Grupo sección y fin grupo
# 8-9  | Grupo familia y fin grupo
# 10-11| Grupo subfamila y fin grupo
# 12-13| Grupo artículos y fin grupo.
# 14   | Grupo definido.

# Passos:
# - Pegar informações da oferta (código, descrição, limites minimo e maximo);
# - Saber a base de condição (entre 1 e 14);
# - Pegar o Formato de condição (1UN, 2IMPORTE, 3KG);
# - Tipo de relação (0 OBRIGATORIO, 1 OR, 2 AND, 3)*****;
# - Com o codigo base pesquisar na tabela maegroup1;

function analisy(){
for oferta in $(cat $ARQOFER); do
    # Pega a descrição da oferta
    desc_ofer=$(echo "SELECT DescOfer FROM maeofer2 WHERE CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega a data de fecha e data de fim da promo
    data_fecha=$(echo "SELECT FechInic FROM maeofer2 WHERE CodiOfer LIKE '$oferta';" | sqlite3 --noheader)
    data_fin=$(echo "SELECT FechFin FROM maeofer2 WHERE CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega limites minimos e máximos e quantidade
    limi_min=$(echo "SELECT LimiMin FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)
    limi_max=$(echo "SELECT LimiMaxi FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)
    cant_cond=$(echo "SELECT CantCond FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)
    
    # Pega o codigo de condição em que os artigos pertencem a promoção
    base_cond=$(echo "SELECT BaseCond FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega o tipo de promoção -> 1UN, 2IMPORTE, 3Kilos
    form_cond=$(echo "SELECT FormCond FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega o tipo relação -> 0 Obrigatorio, 1 OR, 2 e 3 AND
    tipo_rel=$(echo "SELECT TipoRela FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega o código base a ser pesquisado nas tabelas
    codi_base=$(echo "SELECT CodiBase FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    case $base_cond in
        "1")
            # Por Ticket
            echo 1
        ;;
        "2")
            # Sección
            echo 2
        ;;
        "3")
            # Por Familia
            echo $desc_ofer '|' $FechInic '|' $FechFin >> $PROMO_RESULT
            echo "SELECT Codi, Desc, Pvp_4_ FROM maearti1 WHERE TipoFami IN ($codi_base);" | sqlite3 --noheader --column >> $PROMO_RESULT
            echo '---------------------------------------------------------------------' >> $PROMO_RESULT            
        ;;
        "4")
            # Por SubFamilia
            echo $desc_ofer '|' $FechInic '|' $FechFin >> $PROMO_RESULT
            echo "SELECT Codi, Desc, Pvp_4_ FROM maearti1 WHERE SubFami IN ($codi_base);" | sqlite3 --noheader --column >> $PROMO_RESULT
            echo '---------------------------------------------------------------------' >> $PROMO_RESULT            
        ;;
        "5")
            # Por Código do Produto
            echo $desc_ofer '|' $FechInic '|' $FechFin >> $PROMO_RESULT
            echo "SELECT Codi, Desc, Pvp_4_ FROM maearti1 WHERE Codi IN ($codi_base);" | sqlite3 --noheader --column >> $PROMO_RESULT
            echo '---------------------------------------------------------------------' >> $PROMO_RESULT
        ;;
        "6" | "7")
            # Grupo sección y Fin grupo
            echo 6 7
        ;;
        "8" | "9")
            # Grupo familia y Fin grupo
            echo 8 9
        ;;
        "10" | "11")
            # Grupo subfamilia y fin grupo
            echo 10 11
        ;;
        "12" | "13")
            # Grupo articulos y fin grupo
            echo 12 13
        ;;
        "14")
            # Por grupo definido
            codigos_dos_artigos=$(echo "SELECT CodiElem FROM maegrup1 WHERE CodiGrup LIKE '$codi_base';" | sqlite3 --noheader)
            codi_arti=$(echo $codigos_dos_artigos | tr ' ' ',')
            echo $desc_ofer '|' $data_fecha '|' $data_fin >> $PROMO_RESULT
            echo "SELECT Codi, Desc, Pvp_4_ FROM maearti1 WHERE Codi IN ($codi_arti);" | sqlite3 --noheader --column >> $PROMO_RESULT
            echo '---------------------------------------------------------------------' >> $PROMO_RESULT
        ;;
    esac
done
}

# Criando arquivo de base de dados
function create_base(){
    DATABASE="$PATH_USER/database.sh"
    ./$DATABASE
}
    
function main(){
    copy_file_actual
    create_oferta
    analisy
}
main



