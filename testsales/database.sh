echo "DROP TABLE IF EXISTS combos;
CREATE TABLE combos(
    id numeric not null unique,
    codiOfer numeric,
    descOfer text,
    fechInic date,
    fechFin date,
    codiArti numeric
)" | sqlite3 