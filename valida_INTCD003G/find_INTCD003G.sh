#!/bin/bash
#
# Criado por: Walter Moura(wam001br)
# Data Criação: 27.08.2020
# Modificado por: Walter Moura
# Data moficação: 27.08.2020
#
# Script para consultar arquivo INCD003G enviado pela CISS via FTP para o CWS
# 
#
# Version 1.0
# - Fazer o download dos arquivos DCV03YYYYMMDDHHMMSS do CWS
# - Verificar se a loja informada, recebemos o 3G do dia anterior
#
# Version 1.1
# - Inclusão do script python para leitura do XML encontrado
#
# Version 1.2
# - Implementado caixas dialog


#USERACTUAL=$(grep $EUID /etc/group | awk -F ":" '{print $1}')
USERACTUAL=`echo $USER`
SHOP=$1
PATH_USER="/tmp/$USERACTUAL"
PATH_LOG="$PATH_USER/cws/logs_INTCD003G"
LOG="$PATH_LOG/error.log"
DEBUG="$PATH_LOG/debug.log"
OUT="$PATH_USER/out.txt"
DATA_FIND=`date "+%Y%m%d" --date="-1 day"`
HOST_FTP="10.105.186.168"
USER_FTP="userinfoci"
PASSWD_FTP="hostciss"
FILE_DOWNLOAD_FTP="DCV03$DATA_FIND"
TEMP_DOWNLOAD_FTP="$PATH_USER/cws/files_INTCD003G"
DIR_FTP_3G="/Ciss/CissHost/proc"
SCRIPT=`basename $0`

if [ -d $TEMP_DOWNLOAD_FTP ]; then
    rm -vf $TEMP_DOWNLOAD_FTP/$FILE_DOWNLOAD_FTP* > /dev/null &2>1
fi

function fnMain(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Executando o programa." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Chamanda função ['fnValidaLog']." >> $LOG
    SHOP=$(dialog --stdout --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --form "" 11 50 0 "Informe a loja:" 1 1 "" 1 16 6 4)
	[ "$SHOP" ] || { dialog --ok-label "OK" --title "FIND INTCD003G - 1.0" --msgbox "ERROR: Não foi informado a loja." 5 70 ; sleep 1 ; exit ; }
    fnValidaLog
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Chamanda função ['fnCopyFile']." >> $LOG
    fnCopyFile
}

function fnValidaLog(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Iniciando função ['fnValidaLog']." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Verificando se o diretório ['$PATH_LOG'] existe." >> $LOG
    if [ -d $PATH_LOG ]; then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Diretório ['$PATH_LOG'] já existe." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Verificando se o arquivo ['$LOG'] é maior que 200mb." >> $LOG
        KB=`du -hsk $LOG | awk -F " " '{print $1;}'`
        if [ $KB -ge 200000 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Arquivo ['$LOG'] é maior que 200mb ['$KB kb']." >> $LOG
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Verificando a quantidades de arquivos ['$LOG*']." >> $LOG
            COUNT=`ls -ltr $LOG* | wc -l`
            if [ $COUNT -gt 1 ]; then
                if [ $COUNT -gt 5 ]; then
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Quantidades de arquivos ['$LOG*']:['$COUNT']." >> $LOG
                    rm -vf $LOG.1
                    for x in $(seq 2 5);
                    do
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG.$x'] -> ['$LOG.$(( x - 1))']." >> $LOG
                        mv -vf $LOG.$x $LOG.$(( x - 1))
                    done
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG'] -> ['$LOG.5']" >> $LOG
                    mv -vf $LOG $LOG.5
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG'] -> ['$LOG.$COUNT']." >> $LOG
                    mv -vf $LOG $LOG.$COUNT
                fi
            else
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Quantidades de arquivos ['$LOG*']:['$COUNT']." >> $LOG
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG'] -> ['$LOG.$COUNT']." >> $LOG
                mv -vf $LOG $LOG.$COUNT
            fi
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Arquivo ['$LOG'] é menor que 200mb ['$KB kb']." >> $LOG
        fi
    else
        mkdir -p $PATH_LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Diretório ['$PATH_LOG'] não existe." >> $LOG
    fi
}

function fnCopyFile(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando função ['fnCopyFile']." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Verificando se o diretório ['$TEMP_DOWNLOAD_FTP'] existe." >> $LOG
    if [ -d $TEMP_DOWNLOAD_FTP ];then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Diretório ['$TEMP_DOWNLOAD_FTP'] já existe." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando download arquivos ['$FILE_DOWNLOAD_FTP*.xml']." >> $LOG
        fnGetFtp
        clear
        if [ $RET -eq 0 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP'] efetuado com sucesso." >> $LOG
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Verificando se recebemos o 3G da loja ['$SHOP'] ." >> $LOG
            fnVerificaShop
            if [ $RET -eq 0 ]; then
                if [ $RESULTADO -ge 1 ]; then
                    fnNameFileINTCD003G
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:3G loja ['$SHOP'] recebido ['$XML']." >> $LOG
                    dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --yesno "Resultado -> $SHOP encontrado arquivo 3G ['$XML'].\nDeseja abrir o arquivo [$XML]?" 7 80
                    RESPOSTA=$?
                    case $RESPOSTA in
                        0)
                            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando leitura do arquivo ['$XML']." >> $LOG
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 1 --infobox "Iniciando leitura do arquivo ['$XML']." 5 70
                            python /usr/sbin/readxml.py $SHOP $TEMP_DOWNLOAD_FTP/$XML > $OUT
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "$XML" --textbox $OUT 35 115
                        ;;
                        1)
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 5 --infobox "Encerrando, bye." 5 70
                        ;;
                        *)
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 5 --infobox "Encerrando, bye." 5 70
                        ;;
                    esac
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:3G loja ['$SHOP'] não foi recebido." >> $LOG
                    dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --msgbox "Resultado -> $SHOP não foi encontrado arquivo 3G ['$XML']." 5 70
                fi
            else
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao verificar 3G loja ['$SHOP']." >> $LOG
                dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 10 --infobox "Erro ao verificar 3G loja ['$SHOP']." 5 70
            fi
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao realizar download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP']." >> $LOG
            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 10 --infobox "Erro ao realizar download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP']." 5 70
        fi
    else
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Diretório ['$TEMP_DOWNLOAD_FTP'] não existe." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Criando diretório ['$TEMP_DOWNLOAD_FTP']." >> $LOG        
        mkdir -p $TEMP_DOWNLOAD_FTP
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando download arquivos ['$FILE_DOWNLOAD_FTP*.xml']." >> $LOG
        fnGetFtp
        clear
        if [ $RET -eq 0 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP'] efetuado com sucesso." >> $LOG
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Verificando se recebemos o 3G da loja ['$SHOP'] ." >> $LOG
            fnVerificaShop
            if [ $RET -eq 0 ]; then
                if [ $RESULTADO -ge 1 ]; then
                    fnNameFileINTCD003G
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:3G loja ['$SHOP'] recebido ['$XML']." >> $LOG
                    dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --yesno "Resultado -> $SHOP encontrado arquivo 3G ['$XML'].\nDeseja abrir o arquivo [$XML]?" 7 80
                    RESPOSTA=$?
                    case $RESPOSTA in
                        0)
                            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando leitura do arquivo ['$XML']." >> $LOG
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 1 --infobox "Iniciando leitura do arquivo ['$XML']." 5 70
                            python /usr/sbin/readxml.py $SHOP $TEMP_DOWNLOAD_FTP/$XML > $OUT
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "$XML" --textbox $OUT 35 115
                        ;;
                        1)
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 5 --infobox "Encerrando, bye." 5 70
                        ;;
                        *)
                            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 5 --infobox "Encerrando, bye." 5 70
                        ;;
                    esac
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:3G loja ['$SHOP'] não foi recebido." >> $LOG
                    dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --msgbox "Resultado -> $SHOP não foi encontrado arquivo 3G ['$XML']." 5 70
                fi
            else
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao verificar 3G loja ['$SHOP']." >> $LOG
                dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 10 --infobox "Erro ao verificar 3G loja ['$SHOP']." 5 70
            fi
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao realizar download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP']." >> $LOG
            dialog --backtitle "FIND INTCD003G - 1.0" --title "FIND INTCD003G - 1.0" --sleep 10 --infobox "Erro ao realizar download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP']." 5 70
        fi
    fi
}

function fnGetFtp(){
    ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
    lcd $TEMP_DOWNLOAD_FTP
    cd $DIR_FTP_3G
    prompt
    mget $FILE_DOWNLOAD_FTP*.xml
    prompt
    quit
END_SCRIPT
    RET=$?
}   

function fnVerificaShop(){
    RESULTADO=`grep -a -i "idStore=.*$SHOP" $TEMP_DOWNLOAD_FTP/$FILE_DOWNLOAD_FTP* | wc -l`
    RET=$?
}

function fnNameFileINTCD003G(){
    XML=`grep -a -i "idStore=.*$SHOP" $TEMP_DOWNLOAD_FTP/$FILE_DOWNLOAD_FTP* | awk -F ":" '{print $1;}' | awk -F "/" '{print $6;}'`
}

#if [ -z $SHOP ]; then
#    echo -e "\033[01;31mHelp: $SCRIPT {NUMERO LOJA} <enter>\033[00;37m"
#    exit 1
#fi
fnMain