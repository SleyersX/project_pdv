#!/bin/bash

# Script para testes de conexões internas de loja
# Autor: Marcos Marinho
# Version: V.0.1 - * Inicio Script

# Funções: Testar as conexões internas de loja DIA% passada como parâmetro do comando;
#          Os testes são:
            # ping scopetef1 -c 4
            # ping pointsystem -c 4
            # ping nfceweb -c 4
            # ping nfceweb1 -c 4
            # ping servidorcupones -c 4
            # ping brconcen1 -c 4
            # ping brconcen2 -c 4
            # ping pafbackup -c 4
