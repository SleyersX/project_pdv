#!/bin/bash

FECHA=$(date +"%d/%m/%Y %T")
DATE=$(date +"%Y%m%d")
HORA=$(date +"%H%M%S")
BINDIA=/confdia/bin
LOG=/tmp/aux-getsale.log
EXISTE=0
FICHEROTAR=""
unset fileGN
. /confdia/bin/setvari
SHOP=$NUMETIEN

exec &> $LOG

function checkSP(){
	
	STSP=`pidof dispatch.exe`
 	if [ -z $STSP ]; then 
		echo "M�aster nao esta em Selecione Program.a"
		echo "Programa sera encerrado."
		exit 0		 
 	fi
}

function IncrContador(){
	
	checkSP	
	CONT=0; FILE=$1
	[ ! -z "$NAME_FILE" ] && CONT=$(( ${FILE:6:2} + 1))
	[ $CONT -gt 99 ] && CONT=0
	FILE_BASE=$(printf "M%s%02d" $NUMETIEN $CONT)
	echo $FILE_BASE
}

cd $BINDIA
. ./functions

rm -f /tmp/envioGN*.tgz /tmp/M*gz


echo "INICIO PROCESO SalvarVentas."

#Stop_App

[ ! -d /confdia/logscomu ] && echo "ERROR: não existe diretorio logscomu no caixa" && exit 6

cd /confdia/logscomu

NAME_FILE=`find . -name "*_M*" | sort | tail -1`
NEW_FILE=`IncrContador "${NAME_FILE#*_*_}"`

# Paramos o serviço nfs para que a master não receba informação das escravas
echo "Paramos o serviço de rede para as escravas..." && service nfs stop > /dev/null 2>&1
echo "Realizamos uma copia de segurança da Base de Dados..."
tar czf /root/BBDD_copia_$NEW_FILE.tgz /confdia/ctrdatos/maepara1.* /confdia/ficcaje/maeemis* /confdia/ficcaje/intdocu1.dat /confdia/ficcaje/hisdocu1.dat > /dev/null 2>&1
RETtar=$?
[ $RETtar -ne 0 ] && echo "ERROR: falha ao executar TAR..." && exit 6

echo "Executamos o binario, que se encarrrgara de extrair as informações de venda da máster..."
chmod 755 /root/aux-getsale
/root/aux-getsale $SHOP
RETCom=$?
#RETCom=1
[ $RETCom -ne 0 ] && echo "ERROR 3: error ao executar ./aux-getsale $SHOP" && exit 3

echo "Vendas atuais obtidas. Iniciamos o serviço nfs novamente..."
#service nfs start > /dev/null 2>&1
at $(date +%H:%M --date="+1 min") -f /root/startnfs.sh

echo "Gerando arquivos de venda..."
cd /tmp
rm -f M*pat
[ -e M*vta ] && cat M*vta | gzip > $NEW_FILE.VGZ
[ -e M*sun ] && cat M*sun | gzip > $NEW_FILE.SGZ

FICHEROTAR=$DATE"_"$HORA"_"$NEW_FILE
tar zcf $FICHEROTAR.tgz $NEW_FILE.VGZ $NEW_FILE.SGZ
cp -f $FICHEROTAR.tgz /root/envioGN_$NEW_FILE.tgz
mv $FICHEROTAR.tgz /confdia/logscomu/ -f

#Start_App

DT=$(date +"%d/%m/%Y %T")
echo "$DT: Processo de salvar vendas executado com sucesso."
echo "ERROR 0: PROCESO CORRECTO"
exit 0