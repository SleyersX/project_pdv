#!/bin/bash

ip=$1
TGZ="/var/www/html/resorces/tgz/getsale.tgz"

STSP=`sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip 'pidof dispatch.exe'`
if [ -z $STSP ]; then
    echo "Master não esta em Selecione programa."
    echo "Programa será encerrado."
    exit 0
else
    sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip 'chmod -x /confdia/bin/01'
    ERR=$?
    if [ $ERR -eq 0 ]; then
        sshpass -p root scp -P10001 $TGZ root@$ip:/tmp/ > /dev/null 2>&1
        ERR=$?
        if [ $ERR -eq 0 ]; then
            #echo -e "Arquivo enviado com sucesso.\n"
            sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip 'cd /tmp/; tar -xzvf getsale.tgz' > /dev/null 2>&1
            ERR=$?
            if [ $ERR -eq 0 ]; then
                #echo -e "\nCópia da venda realizada com sucesso."
                sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip 'cd /tmp/; chmod +x get_sale.sh; chmod +x aux-getsale; chmod +x startnfs.sh '  > /dev/null 2>&1
                ERR=$?
                if [ $ERR -eq 0 ]; then
                    sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip 'cd /tmp/; bash -x get_sale.sh'  > /dev/null 2>&1
                    ERR=$?
                    if [ $ERR -eq 0 ]; then
                        #echo -e "Arquivo enviado com sucesso.\n"
                        sshpass -p root scp -P10001 root@$ip:/tmp/envioGN* /var/www/html/resorces/getsale/ > /dev/null 2>&1
                        ERR=$?
                        if [ $ERR -eq 0 ]; then
                            sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip 'chmod +x /confdia/bin/01'
                            echo -e "Arquivo gerado com sucesso, confira no botão ['Files'].\n"
                        else
                            echo -e "Erro ao copiar arquivo de venda ['/tmp/envioGN_XXXXXXX.tgz']\n."
                        fi
                    else
                        echo "Erro ao gerar venda ['$ip']."
                    fi
                else
                    echo "Erro ao dar permissão ['$ip']."
                fi    
            else
                echo "Erro ao executar TAR ['$ip']."
            fi
        else
            echo "Erro ao enviar arquivos ['$ip']."
        fi
    else
        echo "Erro ao remover permissões ['$ip']."
    fi
fi