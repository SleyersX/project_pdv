<?php

function get_ac() {

if (!empty($_POST['cnpj_contribuinte'])){
        
        $arq="/var/www/html/resorces/bash/.cnpjs.txt";
        $certificate = $_POST['cb_certificate'];
        $cnpj = $_POST['cnpj_contribuinte'];
        $temp=validar_cnpj($cnpj);
        $cnpjs="03476811000151".$cnpj;
        if($temp != 1){
            $file_cnpj = fopen($arq, "w+");
            fwrite($file_cnpj, $cnpjs);
            fclose($file_cnpj);
            $chave = shell_exec("bash ../bash/gerarAC.sh $certificate");
            echo '<div class="form-group">';
            echo '<label for="recipient-name" class="control-label">CNPJ Software House: 03476811000151</label>';
            echo '</br>';
            echo '<label for="recipient-name" class="control-label">CNPJ Contribuinte: '.$cnpj.'</label>';
            echo '</div>';
            echo '<div class="form-group">';
            echo '<label for="message-text" class="control-label">Chave</label>';
            echo '<textarea type="text" class="form-control" name="chave_ac" readonly>'.$chave.'</textarea>';
            echo '</div>';
        }
		unset($cnpj);
	}
}

function validar_cnpj($cnpj){
    
    $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
    
    // Lista de CNPJs inválidos
    $invalidos = [
        '00000000000000',
        '11111111111111',
        '22222222222222',
        '33333333333333',
        '44444444444444',
        '55555555555555',
        '66666666666666',
        '77777777777777',
        '88888888888888',
        '99999999999999'
    ];

    // Verifica se o CNPJ está na lista de inválidos
    if (in_array($cnpj, $invalidos)) {
        echo "<script>alert('CNPJ Inválido!');</script>";	
        return 1;
    }
	// Valida tamanho
    if (strlen($cnpj) < 14){
        echo "<script>alert('Número de caracteres inválidos!');</script>";
        return 1;
    }
    
	// Verifica se todos os digitos são iguais
    if (preg_match('/(\d)\1{13}/', $cnpj))
        echo "<script>alert('CNPJ Inválido!');</script>";
        return false;

	// Valida primeiro dígito verificador
	for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)){
        echo "<script>alert('CNPJ digito inválido!');</script>";
        return 1;
    }

	// Valida segundo dígito verificador
	for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
    return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
}
?>
