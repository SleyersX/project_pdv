<?php
    include_once("../security/seguranca.php");
    protegePagina();
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <meta http-equiv="refresh" content="30">
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | Admin</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);
        ?>    
        <script src="../js/jquery-3.2.1.min.js"></script>    
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <nav class="navbar navbar-expand-lg navbar-dark bg-mattBlackLight fixed-top">
            <button class="navbar-toggler sideMenuToggler" type="button">
          <span class="navbar-toggler-icon"></span>
        </button>
            <a class="navbar-brand" href="admin.php">Admin - <?php echo $_SESSION['usuarioNome'];?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons icon">
                  person
                </i>
                            <span class="text">Account</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">
                                <span data-feather="info"></span> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                                <span data-feather="lock"></span> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <span data-feather="share"></span> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="wrapper d-flex">
            <div class="sideMenu bg-mattBlackLight">
                <div class="sidebar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="admin.php" class="nav-link px-2 active">
                                <i class="material-icons icon">
                                    home
                                </i>
                                <span class="text">
                                    Home
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="consultas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    search
                                </i>
                                <span class="text">
                                    Consultas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="srv_chaves.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dns
                                </i>
                                <span class="text">
                                    Servidor de Chaves
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="sats.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    camera_alt
                                </i>
                                <span class="text">
                                    SATs
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="list_lojas.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    pages
                                </i>
                                <span class="text">
                                    Cadastro de Lojas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="usuarios.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    person
                                </i>
                                <span class="text">
                                    Usuários
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="niveis.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    layers
                                </i>
                                <span class="text">
                                    Níveis
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="tabelas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dashboard
                                </i>
                                <span class="text">
                                    Tabelas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="control.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    settings
                                </i>
                                <span class="text">
                                    Settings
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="scripts.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    description
                                </i>
                                <span class="text">
                                    Scripts
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 sideMenuToggler">
                                <i class="material-icons icon expandView ">
                                    view_list
                                </i>
                                <span class="text">
                                    Ocultar
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content">

                <main>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-mattBlackLight px-3 py-3">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="container-fluid">
						<form id="frm_consulta" action="srv_chaves.php" method="POST">
									<div class="input-group mb-3">
										<input type="text hidden" class="form-control" id="id_loja" name="nume_loja"  placeholder="Digite o número da loja">
										<div class="input-group-append">
											<!--
											<button class="btn btn-outline-secondary" type="button submit" id="myBtn" data-toggle="modal" data-target="#consulta_chaves" data-whatever="Resultado Loja - ">Consultar</button>-->
											<button class="btn btn-outline-secondary" type="submit" >Consultar</button>
										</div>
									</div>
										<?php
											include_once("../processo/consulta_chaves.php");
											get_num_loja();	
										?>
						</form>
	<main>
		<div class="col-md-8">
				<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#nova_chave_sat">Nova Chave SAT</button>
				<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#nova_chave_nfce">Nova Chave NFCe</button>
		</div>
		</br>
			<div class="col-sm-6" style="float:left;">
				<div class="table-responsive">
					<table class="table table-sm table-striped table-bordered table-hover" id="tb_sat">				
						<thead>
							<tr>
							<th>#</th>
							<th>SAT</th>
							</tr>
						</thead>
							<?php
									echo "<tbody>";
									$path='/var/www/html/resorces/srv_chaves/arquivos/claves_sat';
									$diretorio = dir($path);
										while($arquivo = $diretorio -> read()) 										
											{	
												if ($arquivo != '..' && $arquivo != '.'){
														echo "<tr>";
														echo "<td><img width='40px' height='25px' src='../img/zip.png'></td>";
														echo "<td><a href='../srv_chaves/arquivos/claves_sat/".$arquivo."'>".$arquivo."</a></td>";
														echo "</tr>";		
													}
											}
											$diretorio -> close();
									echo "</tbody>";
							?>
					</table>
				</div>
			</div>
			<div class="col-sm-6" style="float:left;">
				<div class="table-responsive">
					<table class="table table-sm table-striped table-bordered table-hover" id="tb_nfce">
						<thead>
							<tr>
								<th>#</th>
								<th>NFCe</th>
							</tr>
						</thead>
							<?php
								echo "<tbody>";
								$path2='/var/www/html/resorces/srv_chaves/arquivos/claves_nfce';
								$diretorio2 = dir($path2);
									while($arquivo2 = $diretorio2 -> read()) 										
										{	
											if ($arquivo2 != '..' && $arquivo2 != '.'){
													echo "<tr>";
													echo "<td><img width='40px' height='25px' src='../img/zip.png'></td>";
													echo "<td><a href='../srv_chaves/arquivos/claves_nfce/".$arquivo2."'>".$arquivo2."</a></td>";
													echo "</tr>";		
												}
										}
										$diretorio2 -> close();
								echo "</tbody>";
							?>
					</table>
				</div>
			</div>
			</main>
		</div>
	</div>
	<form name="frm_nova_chave" action="../processo/nova_chave.php" method="POST">
		<div class="modal fade" id="nova_chave_sat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Gerar Chaves</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  </div>
				  <div class="modal-body">
					<form>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">Loja:</label>
						<input type="text" class="form-control" name="loja">
					  </div>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">CFe-Key:</label>
						<input type="text" class="form-control" name="cfe-key">
					  </div>
					  <div class="form-group">
						<label for="message-text" class="control-label">AC-SAT</label>
							<textarea type="text" class="form-control" name="ac-sat"></textarea>
					  </div>
					</form>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-success">Enviar</button>
				  </div>
				</div>
			  </div>
			</div>
	</form>
	<form name="frm_nova_chave_nfce" action="../processo/nova_chave_nfce.php" method="POST">
		<div class="modal fade" id="nova_chave_nfce" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Gerar Chaves</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  </div>
				  <div class="modal-body">
					<form>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">Loja:</label>
						<input type="text" class="form-control" name="loja">
					  </div>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">CFe-Key:</label>
						<input type="text" class="form-control" name="cfe-key">
					  </div>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">ID:</label>
						<select name="id" class="custom-select navbar navbar-grey sticky-top bg-grey ">
							<option value="000001">000001</option>
							<option value="000002">000002</option>
							<option value="000003">000003</option>
							<option value="000004">000004</option> 
						</select>        
					  </div>
					  <div class="form-group">
						<label for="message-text" class="control-label">Token:</label>
							<input type="text" class="form-control" name="token">
					  </div>
					</form>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-success">Enviar</button>
				  </div>
				</div>
			  </div>
			</div>
		</form>

		
		<div class="modal fade" id="consulta_chaves" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				  	<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Resultado Chaves </h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  	</div>
				  	<div class="modal-body">
						<form>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Loja</label>
								<input type="text" class="form-control" id="recipient-name">
							</div>
						</form>
				  	</div>
				  	<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				  	</div>
			  	</div>
			</div>
		</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
		<script type="text/JavaScript">

			$('#consulta_chaves').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget)
				var recipient = button.data('whatever')
				var num_loja = document.getElementById('id_loja').value;
				var modal = $(this)

				//modal.find('.modal-title').text(recipient + num_loja)
				modal.find('.modal-body input').val(num_loja)
			})

		</script>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
            feather.replace()
        </script>
    </body>
</html>
