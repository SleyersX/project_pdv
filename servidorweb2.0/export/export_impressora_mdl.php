<?php
   include_once("../security/seguranca.php");
   protegePagina();
   $mdl = filter_input(INPUT_GET, 'mdl', FILTER_SANITIZE_STRING);
   header( 'Content-type: application/csv' );   
   header( 'Content-Disposition: attachment; filename=export_impressora_modelo'.$mdl.'.csv' );   
   header( 'Content-Transfer-Encoding: binary' );
   header( 'Pragma: no-cache');

   $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
   $stmt = $pdo->prepare( 'SELECT loja, pdv, impressora, firmware, data_modificacao FROM tb_consolidado_equipamentos WHERE impressora = "'.$mdl.'"' );   
   $stmt->execute();
   $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

   $out = fopen( 'php://output', 'w' );
   foreach ( $results as $result ) 
   {
      fputcsv( $out, $result );
   }
   fclose( $out );
?>
