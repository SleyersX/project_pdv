<?php
   include_once("../security/seguranca.php");
   protegePagina();
   $vs = filter_input(INPUT_GET, 'vs', FILTER_SANITIZE_STRING);
   header( 'Content-type: application/csv' );   
   header( 'Content-Disposition: attachment; filename=export_lojas_versao_'.$vs.'.csv' );   
   header( 'Content-Transfer-Encoding: binary' );
   header( 'Pragma: no-cache');
   if ($vs != 'total_lojas' ){
	   $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
	   $stmt = $pdo->prepare( 'SELECT * FROM tb_dados_loja WHERE Versao = "'.$vs.'"' );   
	   $stmt->execute();
	   $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

	   $out = fopen( 'php://output', 'w' );
	   foreach ( $results as $result ) 
	   {
		  fputcsv( $out, $result );
	   }
	   fclose( $out );
   }else{
	   $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
	   $stmt = $pdo->prepare( 'SELECT * FROM tb_dados_loja ' );   
	   $stmt->execute();
	   $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

	   $out = fopen( 'php://output', 'w' );
	   foreach ( $results as $result ) 
	   {
		  fputcsv( $out, $result );
	   }
	   fclose( $out );
   }
?>
