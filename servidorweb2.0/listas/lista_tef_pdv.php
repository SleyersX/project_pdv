<?php
include_once("../security/seguranca.php");
protegePagina();
include_once("../security/conecta.php");

$pagina = filter_input(INPUT_POST, 'pagina', FILTER_SANITIZE_NUMBER_INT);
$qnt_result_pg = filter_input(INPUT_POST, 'qnt_result_pg', FILTER_SANITIZE_NUMBER_INT);
//calcular o inicio visualização
$inicio = ($pagina * $qnt_result_pg) - $qnt_result_pg;

//consultar no banco de dados
$result_tef_pdv = "SELECT * FROM cn_erro_tef_pdv LIMIT $inicio, $qnt_result_pg";
$resultado_tef_pdv = mysqli_query($conn, $result_tef_pdv);


//Verificar se encontrou resultado na tabela "usuarios"
if(($resultado_tef_pdv) AND ($resultado_tef_pdv->num_rows != 0)){
?>
    <div class="table-responsive">
        <table class="table table-sm table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Loja</th>
                    <th>Caixa</th>
                    <th>Empresa</th>
                    <th>Filial</th>
                    <th>Cod.PDV</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    while($row_tef_pdv = mysqli_fetch_assoc($resultado_tef_pdv)){
                ?>
                    <tr>
                        <th><?php echo $row_tef_pdv['Loja']; ?></th>
                        <td><?php echo $row_tef_pdv['Caixa']; ?></td>
                        <td><?php echo $row_tef_pdv['Cod_Empresa']; ?></td>
                        <td><?php echo $row_tef_pdv['Filial']; ?></td>
                        <td><?php echo $row_tef_pdv['Cod_Pdv']; ?></td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
<nav aria-label="Page navigation exemple">
    <ul class="pagination pagination-sm justify-content-end">
        <?php

        //Paginação - Somar a quantidade de usuários
        $result_pg = "SELECT COUNT(Loja) AS num_result FROM cn_erro_tef_pdv";
        $resultado_pg = mysqli_query($conn, $result_pg);
        $row_pg = mysqli_fetch_assoc($resultado_pg);

        //Quantidade de pagina
        $quantidade_pg = ceil($row_pg['num_result'] / $qnt_result_pg);

        //Limitar os link antes depois
        $max_links = 2;
        if($pagina==1){
            echo "<li class='page-item disabled'>";
            echo "<a class='page-link' href='#' onclick='lista_tef_pdv(1, $qnt_result_pg)'>Primeira</a>";    
        }else{
            echo "<li class='page-item'>";
            echo "<a class='page-link' href='#' onclick='lista_tef_pdv(1, $qnt_result_pg)'>Primeira</a> ";
        }
        for($pag_ant = $pagina - $max_links; $pag_ant <= $pagina - 1; $pag_ant++){
            if($pag_ant >= 1){
                echo "<li class='page-item'>";
                echo "<a class='page-link' href='#' onclick='lista_tef_pdv($pag_ant, $qnt_result_pg)'>$pag_ant</a>";
            }
        }
        echo "<li class='page-item disabled'>";
        echo "<a class='page-link'>$pagina</a> ";

        for ($pag_dep = $pagina + 1; $pag_dep <= $pagina + $max_links; $pag_dep++) {
            if($pag_dep <= $quantidade_pg){
                echo "<li class='page-item'>";
                echo "<a class='page-link' href='#' onclick='lista_tef_pdv($pag_dep, $qnt_result_pg)'>$pag_dep</a> ";
            }
        }
        if($pagina==$quantidade_pg){
            echo "<li class='page-item disabled'>";
            echo "<a class='page-link' href='#' onclick='lista_tef_pdv($quantidade_pg, $qnt_result_pg)'>Última</a>";
        }else{
            echo "<li class='page-item'>";
            echo "<a class='page-link' href='#' onclick='lista_tef_pdv($quantidade_pg, $qnt_result_pg)'>Última</a>";
        }
        ?>
    </ul>
</nav>
<?php
    }
?>