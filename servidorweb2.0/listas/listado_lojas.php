<?php
include_once("../security/seguranca.php");
protegePagina();
include_once("../security/conecta.php");

$pagina = filter_input(INPUT_POST, 'pagina', FILTER_SANITIZE_NUMBER_INT);
$qnt_result_pg = filter_input(INPUT_POST, 'qnt_result_pg', FILTER_SANITIZE_NUMBER_INT);
//calcular o inicio visualização
$inicio = ($pagina * $qnt_result_pg) - $qnt_result_pg;

//consultar no banco de dados
$result_dados_loja = "SELECT * FROM tb_dados_loja WHERE data BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE()ORDER BY Loja LIMIT $inicio, $qnt_result_pg ";
$resultado_dados_loja = mysqli_query($conn, $result_dados_loja);


//Verificar se encontrou resultado na tabela "usuarios"
if(($resultado_dados_loja) AND ($resultado_dados_loja->num_rows != 0)){
?>
    <div class="table-responsive">
        <table class="table table-sm table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Loja</th>
                    <th>PDVs</th>
                    <th>Versão</th>
                    <th>IP</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    while($row_dados_loja = mysqli_fetch_assoc($resultado_dados_loja)){
                ?>
                    <tr>
                        <th><?php echo $row_dados_loja['Loja']; ?></th>
                        <td><?php echo $row_dados_loja['Qntd_Tpvs']; ?></td>
                        <td><?php echo $row_dados_loja['Versao']; ?></td>
                        <td><?php echo $row_dados_loja['IP']; ?></td>
                        <td><button type="button" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#listadoLojasModal<?php echo $row_dados_loja['Loja']; ?>">Visualizar</button><a href="editaloja.php?id=<?php echo $row_dados_loja['Loja']; ?>"><button type="button" class="btn btn-outline-success btn-sm">Atualizar</button></a></td>
                    </tr>
                    <?php
                        echo "<div class='modal fade' id='listadoLojasModal".$row_dados_loja['Loja']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                            echo "<div class='modal-dialog' modal-dialog-scrollable modal-lg>";
                                echo "<div class='modal-content' bg-mattBlackLight px-3 py-3'>";
                                    echo "<div class='modal-header'>";
                                        echo "<h4>Dados da Loja - ".$row_dados_loja['Loja']."</h4>";
                                        echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                    echo "</div>";
                                echo "<div class='modal-body'>";
                                    echo "<form>";
                                        echo "<div class='form-group'>";
                                            echo '<div class="row">';
                                                echo '<div class="col-md-2">Loja</div>';
                                                echo '<div class="col-md-3">Caixa</div>';
                                                echo '<div class="col-md-2">SAT</div>';
                                                echo '<div class="col-md-3">Última</div>';
                                            echo "</div>";
                                            echo "<div class='row'>";
                                                $sql2 = "SELECT sat, loja, caixa, data_atualizacao FROM tb_sat WHERE loja = ".$row_dados_loja['Loja']."";
                                                $result = mysqli_query($conn,$sql2) or die ("Erro ao retornar dados.");
                                                while ( $regi = mysqli_fetch_array($result))
                                                {

                                                    $numloja=$regi['loja'];
                                                    $numsat=$regi['sat'];
                                                    $numcaixa=$regi['caixa'];
                                                    $dtatualizacao=$regi['data_atualizacao'];

                                                    echo '<div class="col-md-2">'.$numloja.'</div>';
                                                    echo '<div class="col-md-3">'.$numsat.'</div>';
                                                    echo '<div class="col-md-2">'.$numcaixa.'</div>';
                                                    echo '<div class="col-md-3">'.$dtatualizacao.'</div>';
                                                }
                                            echo "</div>";
                                        echo "</div>"; 
                                    echo "</form>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    ?>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
    <nav aria-label="Page navigation exemple">
        <ul class="pagination pagination-sm justify-content-end">
            <?php

            //Paginação - Somar a quantidade de usuários
            $result_pg = "SELECT COUNT(Loja) AS num_result FROM tb_dados_loja WHERE data BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() ORDER BY Loja";
            $resultado_pg = mysqli_query($conn, $result_pg);
            $row_pg = mysqli_fetch_assoc($resultado_pg);

            //Quantidade de pagina
            $quantidade_pg = ceil($row_pg['num_result'] / $qnt_result_pg);

            //Limitar os link antes depois
            $max_links = 2;
            if($pagina==1){
                echo "<li class='page-item disabled'>";
                echo "<a class='page-link' href='#' onclick='listado_dados_loja(1, $qnt_result_pg)'>Primeira</a>";    
            }else{
                echo "<li class='page-item'>";
                echo "<a class='page-link' href='#' onclick='listado_dados_loja(1, $qnt_result_pg)'>Primeira</a> ";
            }
            for($pag_ant = $pagina - $max_links; $pag_ant <= $pagina - 1; $pag_ant++){
                if($pag_ant >= 1){
                    echo "<li class='page-item'>";
                    echo "<a class='page-link' href='#' onclick='listado_dados_loja($pag_ant, $qnt_result_pg)'>$pag_ant</a>";
                }
            }
            echo "<li class='page-item disabled'>";
            echo "<a class='page-link'>$pagina</a> ";

            for ($pag_dep = $pagina + 1; $pag_dep <= $pagina + $max_links; $pag_dep++) {
                if($pag_dep <= $quantidade_pg){
                    echo "<li class='page-item'>";
                    echo "<a class='page-link' href='#' onclick='listado_dados_loja($pag_dep, $qnt_result_pg)'>$pag_dep</a> ";
                }
            }
            if($pagina==$quantidade_pg){
                echo "<li class='page-item disabled'>";
                echo "<a class='page-link' href='#' onclick='listado_dados_loja($quantidade_pg, $qnt_result_pg)'>Última</a>";
            }else{
                echo "<li class='page-item'>";
                echo "<a class='page-link' href='#' onclick='listado_dados_loja($quantidade_pg, $qnt_result_pg)'>Última</a>";
            }
            ?>
        </ul>
    </nav>
<?php
    }
?>