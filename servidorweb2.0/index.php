<?php
    $txt_usuario = (isset($_COOKIE['cookieUsuario'])) ? base64_decode($_COOKIE['cookieUsuario']) : '';
    $txt_senha = (isset($_COOKIE['cookieSenha'])) ? base64_decode($_COOKIE['cookieSenha']) : '';
    $lembreme = (isset($_COOKIE['cookieLembreme'])) ? base64_decode($_COOKIE['cookieLembreme']) : '';
    $checked = ($lembreme == 'rememberme') ? 'checked' : '';
    require_once("security/valida.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico">
	
    <title>Support TPVs | Login</title>

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-5 p-b-30">
				<form class="login100-form validate-form" name="frmLogin" action="index.php" method="POST">
                    <img class="mb-4" src="img/Logo-1.png" alt="" width="125" height="125" />
					<span class="login100-form-title p-b-40">
						Acesso Restrito
                    </span>
					<div class="wrap-input100 validate-input m-b-16" data-validate="Por favor entre com seu usuário.">
						<input class="input100" type="text" value="<?=$txt_usuario?>" name="txt_usuario" id="txt_usuario" placeholder="Usuário" style="text-align: center;">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-20" data-validate = "Digite sua senha">
						<span class="btn-show-pass">
							<i class="fa fa fa-eye"></i>
						</span>
						<input class="input100" type="password" value="<?=$txt_senha?>" name="txt_senha" id="txt_senha" placeholder="Senha" style="text-align: center;">
						<span class="focus-input100"></span>
					</div>
					<div class="checkbox">
                        <label>
                            <input type="checkbox" name="lembrete" value="rememberme" <?=$checked?>> Remember me
                        </label>
                    </div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
					<p class="mt-5 mb-3 text-muted">&copy; 2017-<?php $ano=date('Y'); echo $ano;?></p>
				</form>
			</div>
		</div>
	</div>
	
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>