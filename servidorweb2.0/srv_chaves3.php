<?php
// Note que !== não existia antes do PHP 4.0.0-RC2

if ($handle = opendir('/mnt/claves_sat')) {
    echo "Manipulador de diretório: $handle\n";
    echo "Arquivos:\n";

    /* Esta é a forma correta de varrer o diretório */
    while (false !== ($file = readdir($handle))) {
        echo "$file\n";
    }

    /* Esta é a forma INCORRETA de varrer o diretório */
    while ($file = readdir($handle)) {
        echo "$file\n";
    }

    closedir($handle);
}
?>