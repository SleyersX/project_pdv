#!/bin/bash
#
# Criado por: Walter Moura(wam001br)
# Data Criação: 26.10.2020
# Modificado por: Walter Moura
# Data moficação: 26.10.2020
#
# Servico para gerar relatorio do log da impressora das lojas e enviar via FTP
#
# Version 1.0
# Version 1.1
# - Incluido funcao comprimir tgz
# Version 1.2
# - Incluido funcao comprimir zip
# Version 1.3
# - Desabilitado compressao por erro no formato no windows
# Version 1.4
# - Check mes anterior
# Version 1.5
# - Valida arquivo backup
# Version 1.6
# - Correcao na funcao check_files()
# - Corrigido bug de sintaxe na funcao get_log()
# - Incluido nas funcoes get_log() && check_mes_previous(), uma validacao dos arquivos já enviados se o size_file = 0, gera novamente
# Version 1.7
# - Validacao do tamanho do arquivo antes do envio pelo FTP

set -x

exec 1> /tmp/intauditoriapdv.log
exec 2>> /tmp/intauditoriapdv.log

. /confdia/bin/setvari

PATH_USER="/tmp/auditoria_pdv"
SHOP=${NUMETIEN}
POS=${NUMECAJA}
PATH_SEND="$PATH_USER/send"
ARQARRAYFILES=".temp_files.txt"
PATH_REJECTED="$PATH_USER/rejected"
PATH_LOG="$PATH_USER/log"
LOG="$PATH_LOG/error.auditoria-pdv.log"

#Homologacao
#HOST_FTP="10.106.68.149"
#USER_FTP="ftpciss"
#PASSWD_FTP="diabrasil"
#DIR_FTP_REMOTO_SHOP="AUDITORIA_PDV/D$SHOP/"
#DIR_FTP_REMOTO_YEAR="AUDITORIA_PDV/D$SHOP/$(date +%Y --date='-1 day')/"
#DIR_FTP_REMOTO_MONTH="AUDITORIA_PDV/D$SHOP/$(date +%Y --date='-1 day')/$(date +%m --date='-1 day')/"
#DIR_FTP_REMOTO_POS="AUDITORIA_PDV/D$SHOP/$(date +%Y --date='-1 day')/$(date +%m --date='-1 day')/PDV_$POS/"
#DIR_FTP_REMOTO="AUDITORIA_PDV/D$SHOP/$(date +%Y --date='-1 day')/$(date +%m --date='-1 day')/PDV_$POS/"
#Producao
HOST_FTP="10.105.186.150"
USER_FTP="usr_auditoriapdv"
PASSWD_FTP="diabrasil1"

function main(){
	check_files
	create_dir_ftp_previous_I
	check_mes_previous_I
	create_dir_ftp_previous
	check_mes_previous
	create_dir_ftp
	get_log
}

function check_files(){
	echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificando se o diretorio ['$PATH_USER'] existe." >> $LOG
	if [ -e $PATH_USER ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] já existe." >> $LOG
		mkdir -p $PATH_USER
		mkdir -p $PATH_SEND
		mkdir -p $PATH_LOG
		mkdir -p $PATH_REJECTED
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] não existe." >> $LOG
		mkdir -p $PATH_USER
		mkdir -p $PATH_SEND
		mkdir -p $PATH_LOG
		mkdir -p $PATH_REJECTED
		RETURN=$?
		if [ $RETURN -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] criado com sucesso." >> $LOG
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Ocorreu um erro ao criar o diretorio ['$PATH_USER']." >> $LOG
			exit 1
		fi
	fi
}

function create_dir_ftp_previous(){

	DIR_FTP_REMOTO_SHOP="D$SHOP/"
	DIR_FTP_REMOTO_YEAR="D$SHOP/$(date +%Y --date='-1 month')/"
	DIR_FTP_REMOTO_MONTH="D$SHOP/$(date +%Y --date='-1 month')/$(date +%m --date='-1 month')/"
	DIR_FTP_REMOTO_POS="D$SHOP/$(date +%Y --date='-1 month')/$(date +%m --date='-1 month')/PDV_$POS/"
	DIR_FTP_REMOTO="D$SHOP/$(date +%Y --date='-1 month')/$(date +%m --date='-1 month')/PDV_$POS/"

	echo "$(date +%Y%m%d-%H%M%S.%s):CREATE_DIR_FTP_PREVIOUS:Criando diretorios FTP ['$DIR_FTP_REMOTO']." >> $LOG
	ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
	mkdir $DIR_FTP_REMOTO_SHOP
	mkdir $DIR_FTP_REMOTO_YEAR
	mkdir $DIR_FTP_REMOTO_MONTH
	mkdir $DIR_FTP_REMOTO_POS
    quit
END_SCRIPT

}

function create_dir_ftp_previous_I(){
	
	DIR_FTP_REMOTO_SHOP="D$SHOP/"
	DIR_FTP_REMOTO_YEAR="D$SHOP/$(date +%Y --date='-2 month')/"
	DIR_FTP_REMOTO_MONTH="D$SHOP/$(date +%Y --date='-2 month')/$(date +%m --date='-2 month')/"
	DIR_FTP_REMOTO_POS="D$SHOP/$(date +%Y --date='-2 month')/$(date +%m --date='-2 month')/PDV_$POS/"
	DIR_FTP_REMOTO="D$SHOP/$(date +%Y --date='-2 month')/$(date +%m --date='-2 month')/PDV_$POS/"
	echo "$(date +%Y%m%d-%H%M%S.%s):CREATE_DIR_FTP_PREVIOUS:Criando diretorios FTP ['$DIR_FTP_REMOTO']." >> $LOG
	ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
	mkdir $DIR_FTP_REMOTO_SHOP
	mkdir $DIR_FTP_REMOTO_YEAR
	mkdir $DIR_FTP_REMOTO_MONTH
	mkdir $DIR_FTP_REMOTO_POS
    quit
END_SCRIPT

}

function create_dir_ftp(){
	DIR_FTP_REMOTO_SHOP="D$SHOP/"
	DIR_FTP_REMOTO_YEAR="D$SHOP/$(date +%Y --date='-1 day')/"
	DIR_FTP_REMOTO_MONTH="D$SHOP/$(date +%Y --date='-1 day')/$(date +%m --date='-1 day')/"
	DIR_FTP_REMOTO_POS="D$SHOP/$(date +%Y --date='-1 day')/$(date +%m --date='-1 day')/PDV_$POS/"
	DIR_FTP_REMOTO="D$SHOP/$(date +%Y --date='-1 day')/$(date +%m --date='-1 day')/PDV_$POS/"

	echo "$(date +%Y%m%d-%H%M%S.%s):CREATE_DIR_FTP:Criando diretorios FTP ['$DIR_FTP_REMOTO']." >> $LOG
	ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
	mkdir $DIR_FTP_REMOTO_SHOP
	mkdir $DIR_FTP_REMOTO_YEAR
	mkdir $DIR_FTP_REMOTO_MONTH
	mkdir $DIR_FTP_REMOTO_POS
    quit
END_SCRIPT

}

function check_mes_previous(){
	EXT="txt"
	ano=$(date +%Y --date="-1 month")
	mes_previuous=$(date +%m --date="-1 month")
	mes_previuous=$(seq $mes_previuous $mes_previuous)
	dt=$(date +%Y%m --date="-1 month")

	for i in $(cal -m $mes_previuous $ano | tail -n6 | xargs -n 1)
	do 
		if [ $(ls -ltr $PATH_SEND/impressora_$dt$(printf "%02d" $i).$EXT.sent | wc -l) -eq 0 ] || [ $( du -hsk $PATH_SEND/impressora_$dt$(printf "%02d" $i).$EXT.sent | cut -d "/" -f1 | tr -d "\n") -eq 0 ]; then
			FILES_JOB=$(for x in /confdia/backup/impresora.log.gz.*; do KB=`du -hsk $x | cut -d "/" -f1` ; if [ $KB -ge 1 ]; then echo -n " $x" ; fi ; done)
			(zcat $FILES_JOB | grep -a ^$dt$(printf "%02d" $i) > $PATH_USER/impressora_$dt$(printf "%02d" $i).txt) && (cat /var/log/impresora.log | grep -a ^$dt$(printf "%02d" $i) >> $PATH_USER/impressora_$dt$(printf "%02d" $i).txt) || (cat /var/log/impresora.log | grep -a ^$dt$(printf "%02d" $i) >> $PATH_USER/impressora_$dt$(printf "%02d" $i).txt)
			ARQ="impressora_$dt$(printf "%02d" $i)"
			#create_zip
			#create_tgz
			create_file
			if [ $RET -eq 0 ] && [ $(du -hsk $PATH_USER/impressora_$dt$(printf "%02d" $i).txt | cut -d "/" -f1 | tr -d "\n" ) -ge 1 ]; then
				send_file
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_MES_PREVIOUS:Arquivo ['impressora_$dt$(printf "%02d" $i).txt'] nao pode ser enviado." >> $LOG
				mv -vf impressora_$dt$(printf "%02d" $i).txt $PATH_REJECTED/impressora_$dt$(printf "%02d" $i).txt.err
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_MES_PREVIOUS:Arquivo movido  ['impressora_$dt$(printf "%02d" $i).txt'] ->  ['$PATH_USER/rejected/impressora_$dt$(printf "%02d" $i).txt.err']." >> $LOG
			fi
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_MES_PREVIOUS:Arquivo ['impressora_$dt$(printf "%02d" $i).$EXT'] ja consta como enviado ao servidor FTP." >> $LOG 
		fi
	done

}

function check_mes_previous_I(){
	EXT="txt"
	ano=$(date +%Y --date="-2 month")
	mes_previuous=$(date +%m --date="-2 month")
	mes_previuous=$(seq $mes_previuous $mes_previuous)
	dt=$(date +%Y%m --date="-2 month")
	for i in $(cal -m $mes_previuous $ano | tail -n6 | xargs -n 1)
	do 
		if [ $(ls -ltr $PATH_SEND/impressora_$dt$(printf "%02d" $i).$EXT.sent | wc -l) -eq 0 ] || [ $( du -hsk $PATH_SEND/impressora_$dt$(printf "%02d" $i).$EXT.sent | cut -d "/" -f1 | tr -d "\n") -eq 0 ]; then
			FILES_JOB=$(for x in /confdia/backup/impresora.log.gz.*; do KB=`du -hsk $x | cut -d "/" -f1` ; if [ $KB -ge 1 ]; then echo -n " $x" ; fi ; done)
			(zcat $FILES_JOB | grep -a ^$dt$(printf "%02d" $i) > $PATH_USER/impressora_$dt$(printf "%02d" $i).txt) && (cat /var/log/impresora.log | grep -a ^$dt$(printf "%02d" $i) >> $PATH_USER/impressora_$dt$(printf "%02d" $i).txt) || (cat /var/log/impresora.log | grep -a ^$dt$(printf "%02d" $i) >> $PATH_USER/impressora_$dt$(printf "%02d" $i).txt)
			ARQ="impressora_$dt$(printf "%02d" $i)"
			#create_zip
			#create_tgz
			create_file
			if [ $RET -eq 0 ] && [ $(du -hsk $PATH_USER/impressora_$dt$(printf "%02d" $i).txt | cut -d "/" -f1 | tr -d "\n" ) -ge 1 ]; then
				send_file
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_MES_PREVIOUS:Arquivo ['impressora_$dt$(printf "%02d" $i).txt'] nao pode ser enviado." >> $LOG
				mv -vf impressora_$dt$(printf "%02d" $i).txt $PATH_REJECTED/impressora_$dt$(printf "%02d" $i).txt.err
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_MES_PREVIOUS:Arquivo movido  ['impressora_$dt$(printf "%02d" $i).txt'] ->  ['$PATH_USER/rejected/impressora_$dt$(printf "%02d" $i).txt.err']." >> $LOG
			fi
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_MES_PREVIOUS:Arquivo ['impressora_$dt$(printf "%02d" $i).$EXT'] ja consta como enviado ao servidor FTP." >> $LOG 
		fi
	done

}

function get_log(){
	EXT="txt"
	for i in $(seq 1 31 )
	do 
		dt=$(date +%Y%m)
		dia=$(date +%d)
		dia=$(seq $dia $dia)
		if [ $i -lt $dia  ] || [ $dia -eq 31 ]; then 
			if [ $(ls -ltr $PATH_SEND/impressora_$dt$(printf "%02d" $i).$EXT.sent | wc -l) -eq 0 ] || [ $( du -hsk $PATH_SEND/impressora_$dt$(printf "%02d" $i).$EXT.sent | cut -d "/" -f1 | tr -d "\n") -eq 0 ]; then
				FILES_JOB=$(for x in /confdia/backup/impresora.log.gz.*; do KB=`du -hsk $x | cut -d "/" -f1` ; if [ $KB -ge 1 ]; then echo -n " $x" ; fi ; done)
				#zcat /confdia/backup/impresora.log.gz.* | grep -a ^$dt$(printf "%02d" $i) > $PATH_USER/impressora_$dt$(printf "%02d" $i).txt || cat /var/log/impresora.log | grep -a ^$dt$(printf "%02d" $i) > $PATH_USER/impressora_$dt$(printf "%02d" $i).txt
				(zcat $FILES_JOB | grep -a ^$dt$(printf "%02d" $i) > $PATH_USER/impressora_$dt$(printf "%02d" $i).txt) && (cat /var/log/impresora.log | grep -a ^$dt$(printf "%02d" $i) >> $PATH_USER/impressora_$dt$(printf "%02d" $i).txt) || (cat /var/log/impresora.log | grep -a ^$dt$(printf "%02d" $i) >> $PATH_USER/impressora_$dt$(printf "%02d" $i).txt)
				ARQ="impressora_$dt$(printf "%02d" $i)"
				#create_zip
				#create_tgz
				create_file
				if [ $RET -eq 0 ] && [ $(du -hsk $PATH_USER/impressora_$dt$(printf "%02d" $i).txt | cut -d "/" -f1 | tr -d "\n" ) -ge 1 ]; then
					send_file
				else
					echo "$(date +%Y%m%d-%H%M%S.%s):GET_LOG:Arquivo ['impressora_$dt$(printf "%02d" $i).txt'] nao pode ser enviado." >> $LOG
					mv -vf impressora_$dt$(printf "%02d" $i).txt $PATH_REJECTED/impressora_$dt$(printf "%02d" $i).txt.err
					echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_MES_PREVIOUS:Arquivo movido  ['impressora_$dt$(printf "%02d" $i).txt'] ->  ['$PATH_USER/rejected/impressora_$dt$(printf "%02d" $i).txt.err']." >> $LOG
				fi
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):GET_LOG:Arquivo ['impressora_$dt$(printf "%02d" $i).$EXT'] ja consta como enviado ao servidor FTP." >> $LOG 
			fi
		fi 
	done
}

function create_zip(){
	echo "$(date +%Y%m%d-%H%M%S.%s):CREATE_ZIP:Transformando arquivo ['impressora_$dt$(printf "%02d" $i).txt'] em zip." >> $LOG
	cd $PATH_USER
	zip $ARQ.zip $ARQ.log
	RET=$?
	if [ $RET -eq 0 ]; then
		rm -vf $ARQ.log
		FILE_SEND_FTP="$ARQ.zip"
	fi
}

function create_tgz(){
	echo "$(date +%Y%m%d-%H%M%S.%s):CREATE_TGZ:Transformando arquivo ['impressora_$dt$(printf "%02d" $i).txt'] em tgz." >> $LOG
	cd $PATH_USER
	tar -czf $ARQ.tgz $ARQ.log
	RET=$?
	if [ $RET -eq 0 ]; then
		rm -vf $ARQ.log
		FILE_SEND_FTP="$ARQ.tgz"
	fi
}

function create_file(){
	cd $PATH_USER
	FILE_SEND_FTP="$ARQ.txt"
	RET=$?
}

function send_file(){
	cd $PATH_USER

    ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
    cd $DIR_FTP_REMOTO
    put $FILE_SEND_FTP
    quit
END_SCRIPT
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Arquivo ['$FILE_SEND_FTP'] enviado com sucesso." >> $LOG
    	mv $PATH_USER/$FILE_SEND_FTP $PATH_SEND/$FILE_SEND_FTP.sent
		RETURN=$?
		if [ $RETURN -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Arquivo ['$FILE_SEND_FTP'] movido/renomeado com sucesso ['$FILE_SEND_FTP.sent']." >> $LOG
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Erro ao mover/renomear ['$FILE_SEND_FTP'] para ['$FILE_SEND_FTP.sent']." >> $LOG
		fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Erro ao enviar arquivo ['$FILE_SEND_FTP']." >> $LOG
    fi

}

main
