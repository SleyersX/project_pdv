#!/bin/bash

FILE_BASH="/root/intauditoriapdv.sh"
COUNT_AT=`at -l | wc -l`
if [ $COUNT_AT -ge 1 ] ; then 
	for i in $(at -l | awk -F " " '{print $1;}' | tr -d '\0') 
	do 
		at -d $i 
	done
else 
	at $(date +%H:%M --date="+1 min") -f $FILE_BASH
fi
