#!/bin/bash

PATH_USER="/root/outquery"
NAMEFILE="intstockonline.log"
SHOP=${NUMETIEN}
PATH_BKP="$PATH_USER/backup/debug"
ARQARRAYFILES=".temp_files.txt"

if [ -e $PATH_BKP ]; then
    COUNTBKP=`ls -ltr $PATH_BKP/$NAMEFILE* | wc -l`
    cd  $PATH_USER
    if [ $COUNTBKP -eq 0 ]; then
        mv -vf /tmp/$NAMEFILE $PATH_BKP/$NAMEFILE.0
    else
        if [ $COUNTBKP -eq 100 ]; then
            cd $PATH_BKP
            FILESTEMP=`ls -tr $NAMEFILE*`
            echo $FILESTEMP > $ARQARRAYFILES
            while read filename;
            do
                progress=("$filename")
            done < $ARQARRAYFILES
            ARQARRAYFILES=(${progress[0]})
            i=0
            x=1
            while [ $i != ${#ARQARRAYFILES[@]} ]
            do
                if [ $i -eq 0 ]; then
                    rm -vf ${ARQARRAYFILES[i]}
                    mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
                else
                    mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
                fi
                let "i = i +1"
                let "x = x +1"
                if [ $i -eq 99 ]; then
                    break
                fi
            done
            mv -vf /tmp/$NAMEFILE $PATH_BKP/$NAMEFILE.99
        else
            COUNTBKP=`echo $COUNTBKP | tr -d " "`
            mv -vf /tmp/$NAMEFILE $PATH_BKP/$NAMEFILE.$COUNTBKP
        fi
    fi
else
    cd $PATH_USER
    mkdir backup
    cd backup
    mkdir debug
    mv -vf /tmp/$NAMEFILE $PATH_BKP/$NAMEFILE.0
fi
cd $PATH_USER
bash -x /root/intstockonline.sh > /tmp/intstockonline.log 2>&1