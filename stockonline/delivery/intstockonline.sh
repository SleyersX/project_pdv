#!/bin/bash
#
# Criado por: Walter Moura(wam001br)
# Data Criação: 24.03.2020
# Modificado por: Walter Moura
# Data moficação: 15.08.2020
#
# Serviço para gerar um listado com stock online das lojas, com corte de 15/30 min e enviar os dados via FTP
# a um servidor de banco de dados, se encarregara de ler os dados e fazer o input no banco de dados.
#
# Version 1.0
# Base de dados
# - catalog.db 
# Ler as seguintes tabelas do PDV:
# - MAEARTI1
# - Family
# - SubFamily
# - Section
# - ACUARTI1
#
# Version 1.1
# Ajuste na query stock
# Correcao check arquivos

. /confdia/bin/setvari

PATH_USER="/root/outquery"
BD="/confdia/movil/catalog.db"
OUTFILE="$PATH_USER/list_stock"
NAMEFILE="list_stock"
SHOP=${NUMETIEN}
PATH_BKP="$PATH_USER/backup"
ARQARRAYFILES=".temp_files.txt"
OUTQUERY="$PATH_USER/.query_stock"
LOG="$PATH_USER/error.intstockonline.log"

function main(){

	check_files
	catalog_builder
	query_items
	ajust_query

}

function check_files(){

	echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificando se o diretorio ['$PATH_USER'] existe." >> $LOG
	if [ -e $PATH_USER ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] já existe." >> $LOG
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] não existe." >> $LOG
		mkdir $PATH_USER
		RETURN=$?
		if [ $RETURN -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] criado com sucesso." >> $LOG
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Ocorreu um erro ao criar o diretorio ['$PATH_USER']." >> $LOG
			exit 1
		fi
	fi

	if [ -e $PATH_BKP ]; then
		COUNTBKP=`ls -ltr $PATH_BKP/$NAMEFILE* | wc -l`
        cd  $PATH_USER
		if [ $COUNTBKP -eq 0 ]; then
			mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.0
		else
			if [ $COUNTBKP -eq 100 ]; then
				cd $PATH_BKP
				FILESTEMP=`ls -tr $NAMEFILE*`
				echo $FILESTEMP > $ARQARRAYFILES
				while read filename;
				do
					progress=("$filename")
				done < $ARQARRAYFILES
				ARQARRAYFILES=(${progress[0]})
				i=0
				x=1
				while [ $i != ${#ARQARRAYFILES[@]} ]
				do
					if [ $i -eq 0 ]; then
					    rm -vf ${ARQARRAYFILES[i]}
					    mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
					else
						mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
					fi
					let "i = i +1"
					let "x = x +1"
					if [ $i -eq 99 ]; then
						break
					fi
				done
				mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.99
			else
                COUNTBKP=`echo $COUNTBKP | tr -d " "`
				mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.$COUNTBKP
			fi
		fi
	else
		cd $PATH_USER
		mkdir backup
		mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.0
	fi
	cd $PATH_USER
}

function catalog_builder(){

	DIR_FICH_XML="/confdia/movil/items_S.xml"
	FICH_DB="catalog.db"
	FICH_XML=$(basename "$DIR_FICH_XML")

	DIR_CHROOT_DEB="/srv/Debian6.0"
	DIR_DST=$(dirname "$DIR_FICH_XML")
	if [ -e "$DIR_FICH_XML" ]; then
		if [ ! -e $DIR_FICH_XML ]; then 
			echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Nao existe catalog xml ['$DIR_FICH_XML']." >> $LOG
			exit 1
		fi	
		if [ -e  "$DIR_DST/$FICH_DB" ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Se o arquivo .db ja existe, apagamos ['$DIR_DST/$FICH_DB']." >> $LOG
			rm -f "$DIR_DST/$FICH_DB"
		fi
		echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Copiamos o arquivo XML ['$DIR_FICH_XML'] para o diretorio tmp ['$DIR_CHROOT_DEB/tmp']  e executamos o JAVA ['CatalogBuilder.jar']." >> $LOG
		cp -f $DIR_FICH_XML "${DIR_CHROOT_DEB}/tmp/"
		chroot $DIR_CHROOT_DEB /usr/bin/java -jar /srv/CatalogBuilder.jar "/tmp/${FICH_XML}"
		if [ ! -e "$DIR_CHROOT_DEB/$FICH_DB" ];then
			echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Erro ao gerar catalog db ['$DIR_CHROOT_DEB/$FICH_DB']." >> $LOG
			exit 1
		fi
		cp -f $DIR_CHROOT_DEB/$FICH_DB $DIR_DST
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Erro de parametros." >> $LOG	
		exit 1
	fi
}

function query_items(){

	echo	"SELECT 
				MAEARTI1.Codi, 
				MAEARTI1.Desc, 
				printf('%.2f', MAEARTI1.Pvp_0_) AS PVP_Tarifa, 
				printf('%.2f', MAEARTI1.Pvp_4_) AS PVP_NFidelizado, 
				printf('%.2f', MAEARTI1.Pvp_2_) AS PVP_Fidelizado, 
				Section.Description AS Secao, 
				Family.Description AS Familia, 
				SubFamily.Description AS SubFamilia, 
				MAEARTI1.TipoTrat, 
				(ACUARTI1.StocActuUnid - ACUARTI2.AcumUnid) AS StockActUnid,
                (ACUARTI1.StocActuKilo - ACUARTI2.AcumKilo) AS StockActKilo
			FROM 
				(MAEARTI1 
					INNER JOIN ACUARTI1 ON (MAEARTI1.Codi LIKE ACUARTI1.Codi) 
					INNER JOIN Family ON (MAEARTI1.TipoFami LIKE Family.FamilyID ) 
					INNER JOIN SubFamily ON (MAEARTI1.SubFami LIKE SubFamily.SubFamilyID) 
					INNER JOIN Section ON (MAEARTI1.CodiSecc LIKE Section.SectionID)
                    INNER JOIN ACUARTI2 ON (MAEARTI1.Codi LIKE ACUARTI2.Codi)
			    );" | sqlite3 $BD -noheader > $OUTQUERY
	if [ $? != 0 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):QUERY_ITEMS:Erro ao executar query." >> $LOG
		exit 1
	fi
}

function ajust_query(){

	DATA_QUERY=$(date "+%d/%m/%Y - %H:%M:%S")
	echo "Shop""|""Date""|""Codi""|""Desc""|""PVP_Tarifa""|""PVP_NFidelizado""|""PVP_Fidelizado""|""Secao""|""Familia""|""SubFamilia""|""TipoTrat""|""StockActUnid""|""StockActKilo" > $OUTFILE
	IFS="|"
	while read Codi Desc PVP_Tarifa PVP_NFidelizado PVP_Fidelizado Secao Familia SubFamilia TipoTrat StockActUnid StockActKilo;
	do
		if [ $TipoTrat -eq 0 ]; then
			echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$PVP_Tarifa""|""$PVP_NFidelizado""|""$PVP_Fidelizado""|""$Secao""|""$Familia""|""$SubFamilia""|""$TipoTrat""|""$StockActUnid""|""0" >> $OUTFILE
		elif [ $TipoTrat -eq 1 ]; then
			echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$PVP_Tarifa""|""$PVP_NFidelizado""|""$PVP_Fidelizado""|""$Secao""|""$Familia""|""$SubFamilia""|""$TipoTrat""|""0""|""$StockActKilo" >> $OUTFILE
		fi
	done < $OUTQUERY
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUST_QUERY:Query ajustada com sucesso." >> $LOG
        send_file
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUST_QUERY:Erro ao ajustar query." >> $LOG
		exit 1
    fi

}

function send_file(){

	HOST_FTP="10.106.68.57"
    USER_FTP="FTPPC"
    PASSWD_FTP="FTPPCAS400"
    FILE_SEND_FTP=list_stock_$SHOP.csv

    cd $PATH_USER
    cp -vf $NAMEFILE $FILE_SEND_FTP

    ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
    cd Estoque
    put $FILE_SEND_FTP
    quit
END_SCRIPT
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Arquivo ['$FILE_SEND_FTP'] enviado com sucesso." >> $LOG
    	mv $FILE_SEND_FTP $FILE_SEND_FTP.sent
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Erro ao enviar arquivo ['$FILE_SEND_FTP']." >> $LOG
    fi

}

exec 1>> /tmp/intstockonline.log
exec 2>> /tmp/intstockonline.log
set -x

main