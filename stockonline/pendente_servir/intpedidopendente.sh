#!/bin/bash
#
# Criado por: Walter Moura(wam001br)
# Data Criação: 13.01.2021
# Modificado por: Walter Moura
# Data moficação: 13.01.2021
#
# Serviço para gerar um listado com stock pendete online das lojas, com corte de 15/30 min e enviar os dados via FTP
# a um servidor de banco de dados, se encarregara de ler os dados e fazer o input no banco de dados.
#
# Version 1.0
# Base de dados
# - catalog.db 
# Ler as seguintes tabelas do PDV:
# - MAEARTI1
# - ACUARTI1
#
# Version 1.115.08.2020
# Ajuste na query stock
# Correcao check arquivos

. /confdia/bin/setvari

PATH_USER="/root/outquerypendente"
BD="/confdia/movil/catalog.db"
OUTFILE="$PATH_USER/list_stock"
NAMEFILE="list_stock"
SHOP=${NUMETIEN}
PATH_BKP="$PATH_USER/backup"
ARQARRAYFILES=".temp_files.txt"
OUTQUERY="$PATH_USER/.query_stock"
LOG="$PATH_USER/error.intstockpendenteonline.log"

function main(){

	check_files
	catalog_builder
	query_items
	ajust_query

}

function check_files(){

	echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificando se o diretorio ['$PATH_USER'] existe." >> $LOG
	if [ -e $PATH_USER ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] já existe." >> $LOG
    else
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] não existe." >> $LOG
		mkdir $PATH_USER
		RETURN=$?
		if [ $RETURN -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] criado com sucesso." >> $LOG
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Ocorreu um erro ao criar o diretorio ['$PATH_USER']." >> $LOG
			exit 1
		fi
	fi

	if [ -e $PATH_BKP ]; then
		COUNTBKP=`ls -ltr $PATH_BKP/$NAMEFILE* | wc -l`
        cd  $PATH_USER
		if [ $COUNTBKP -eq 0 ]; then
			mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.0
		else
			if [ $COUNTBKP -eq 100 ]; then
				cd $PATH_BKP
				FILESTEMP=`ls -tr $NAMEFILE*`
				echo $FILESTEMP > $ARQARRAYFILES
				while read filename;
				do
					progress=("$filename")
				done < $ARQARRAYFILES
				ARQARRAYFILES=(${progress[0]})
				i=0
				x=1
				while [ $i != ${#ARQARRAYFILES[@]} ]
				do
					if [ $i -eq 0 ]; then
					    rm -vf ${ARQARRAYFILES[i]}
					    mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
					else
						mv -vf ${ARQARRAYFILES[x]} ${ARQARRAYFILES[i]}
					fi
					let "i = i +1"
					let "x = x +1"
					if [ $i -eq 100 ]; then
						break
					fi
				done
				mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.100
			else
                COUNTBKP=`echo $COUNTBKP | tr -d " "`
				mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.$COUNTBKP
			fi
		fi
	else
		cd $PATH_USER
		mkdir backup
		mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.0
	fi
	cd $PATH_USER
}

function catalog_builder(){

	DIR_FICH_XML="/confdia/movil/items_S.xml"
	FICH_DB="catalog.db"
	FICH_XML=$(basename "$DIR_FICH_XML")

	DIR_CHROOT_DEB="/srv/Debian6.0"
	DIR_DST=$(dirname "$DIR_FICH_XML")
	if [ -e "$DIR_FICH_XML" ]; then
		if [ ! -e $DIR_FICH_XML ]; then 
			echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Nao existe catalog xml ['$DIR_FICH_XML']." >> $LOG
			exit 1
		fi	
		if [ -e  "$DIR_DST/$FICH_DB" ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Se o arquivo .db ja existe, apagamos ['$DIR_DST/$FICH_DB']." >> $LOG
			rm -f "$DIR_DST/$FICH_DB"
		fi
		echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Copiamos o arquivo XML ['$DIR_FICH_XML'] para o diretorio tmp ['$DIR_CHROOT_DEB/tmp']  e executamos o JAVA ['CatalogBuilder.jar']." >> $LOG
		cp -f $DIR_FICH_XML "${DIR_CHROOT_DEB}/tmp/"
		chroot $DIR_CHROOT_DEB /usr/bin/java -jar /srv/CatalogBuilder.jar "/tmp/${FICH_XML}"
		if [ ! -e "$DIR_CHROOT_DEB/$FICH_DB" ];then
			echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Erro ao gerar catalog db ['$DIR_CHROOT_DEB/$FICH_DB']." >> $LOG
			exit 1
		fi
		cp -f $DIR_CHROOT_DEB/$FICH_DB $DIR_DST
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CATALOG_BUILDER:Erro de parametros." >> $LOG	
		exit 1
	fi
}

function query_items(){

	echo	"SELECT 
				MAEARTI1.Codi, 
				MAEARTI1.Desc, 
				MAEARTI1.TipoTrat,
                ACUARTI1.UnidPendReci AS PendenteUnid,
                ACUARTI1. KiloPendReciEnGram AS PendenteKilo, 
				(ACUARTI1.StocActuUnid - ACUARTI2.AcumUnid) AS StockActUnid,
                (ACUARTI1.StocActuKilo - ACUARTI2.AcumKilo) AS StockActKilo
			FROM 
				(MAEARTI1 
					INNER JOIN ACUARTI1 ON (MAEARTI1.Codi LIKE ACUARTI1.Codi)
                    INNER JOIN ACUARTI2 ON (MAEARTI1.Codi LIKE ACUARTI2.Codi)
			    );" | sqlite3 $BD -noheader > $OUTQUERY
	if [ $? != 0 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):QUERY_ITEMS:Erro ao executar query." >> $LOG
		exit 1
	fi
}

function ajust_query(){

	DATA_QUERY=$(date "+%d/%m/%Y - %H:%M:%S")
	echo "Shop""|""Date""|""Codi""|""Desc""|""TipoTrat""|""PendenteUnid""|""PendenteKilo""|""StockActUnid""|""StockActKilo" > $OUTFILE
	IFS="|"
	while read Codi Desc TipoTrat PendUnid PendKilo StockActUnid StockActKilo;
	do
		if [ $TipoTrat -eq 0 ]; then
			echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$TipoTrat""|""$PendUnid""|""0""|""$StockActUnid""|""0" >> $OUTFILE
		elif [ $TipoTrat -eq 1 ]; then
            echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$TipoTrat""|""0""|""$PendKilo""|""0""|""$StockActKilo" >> $OUTFILE
		fi
	done < $OUTQUERY
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUST_QUERY:Query ajustada com sucesso." >> $LOG
        ApiCurl
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUST_QUERY:Erro ao ajustar query." >> $LOG
		exit 1
    fi

}

function ApiCurl(){
    HOST="10.106.68.149"
    HOSTCTG="10.106.68.78"

    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Teste conexao Server API_1['$HOST']." >> $LOG
    REST=`curl --connect-timeout 5 --request GET "http://$HOST/restapi/v1/api/source/Controllers/MonitoramentoArtigosPendentes.php?token=7c6ce378b1ef0a29180dd36a0d436a91&shop=99999&codigo=0"`
    CHECK_REST=`echo $REST | grep -a -i response | wc -l`
    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Retorno teste conexao Server API_1['$HOST']:['$REST']:['$CHECK_REST']." >> $LOG

    if [ -z $REST ] || [ $CHECK_REST -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Erro ao conectar Server API_1['$HOST']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Teste conexao Server API_2['$HOSTCTG']." >> $LOG
            REST=`curl --connect-timeout 5 --request GET "http://$HOSTCTG/restapi/v1/api/source/Controllers/Sats.php?token=7c6ce378b1ef0a29180dd36a0d436a91&sat=000000000"`
            CHECK_REST=`echo $REST | grep -a -i response | wc -l`
            if [ -z $REST ] || [ $CHECK_REST -eq 0 ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Erro ao conectar Server API_2['$HOST']." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Requisicao encerrado por falta de conexao ['$HOST'] && ['$HOSTCTG']." >> $LOG
                    #exit 1
            else
                    HOSTAPI=$HOSTCTG
            fi
    else
            HOSTAPI=$HOST
    fi

    IFS="|"

    while read shop data codi desc tipoTrat pendenteUnid pendenteKilo stockActUnid stockActKilo;
    do
        ajustDesc=`echo $desc | sed "s/ /%20/g"`
        URL="http://$HOST/restapi/v1/api/source/Controllers/MonitoramentoArtigosPendentes.php?token=7c6ce378b1ef0a29180dd36a0d436a91&shop=$shop&codigo=$codi&descricao=$ajustDesc&tipo=$tipoTrat&pendente=$pendenteUnid&pendenteKilo=$pendenteKilo&stock=$stockActUnid&stockKilo=$stockActKilo"
        echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:URL -> [$URL]" >> $LOG
        RETURN=`curl --connect-timeout 5 --request PUT -H "Content-Type: application/json" "$URL"`
        echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Retorno Application/json -> [$RETURN]" >> $LOG
    done < $OUTFILE

}

exec 1>> /tmp/intstockopendenteonine.log
exec 2>> /tmp/intstockopendenteonine.log
set -x

main