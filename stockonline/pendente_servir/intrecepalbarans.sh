#!/bin/bash


. /confdia/bin/setvari

PATH_USER="/root/outquerypendente"
OUTFILE="$PATH_USER/transacao_41"
NAMEFILE="transacao_41"
SHOP=${NUMETIEN}
PATH_BKP="$PATH_USER/backup"
ARQARRAYFILES="$PATH_USER/.temp_files.txt"
ARQARRAYFILESTMP="$PATH_USER/.temp_files_new.txt"
LOG="$PATH_USER/error.intrecepcaoalbaran.log"


function main(){
    fnGroupFiles
    if [ $RET -eq 0 ]; then
        echo "Array arquivos gerados...."
        i=0
        while [ $i != ${#ARRAYFILES[@]} ]
        do
            tar -xzf ${ARRAYFILES[$i]} -C /tmp/
            cat /tmp/confdia/comunica/*.TMP | fold -bw140 | grep -a -i ^41 > $OUTFILE
            fnReadTransacao
            let "i = i +1"
        done
    else
        echo "Array arquivos nao foram gerados...."
    fi
}

function fnGroupFiles(){

    COUNT_FILE=`ls -ltr /confdia/logscomu/$(date +%Y%m%d)*RE.tgz | wc -l`
    if [ $COUNT_FILE -ge 1 ]; then
        rm -vf $ARQARRAYFILESTMP
        TMP_FILES=`ls -tr /confdia/logscomu/$(date +%Y%m%d)*RE.tgz`
        echo -n "$TMP_FILES" > $ARQARRAYFILES
        for files in $(cat $ARQARRAYFILES)
        do
            printf "%s\t" $files >> $ARQARRAYFILESTMP
        done
        echo "" >> $ARQARRAYFILESTMP
        cat $ARQARRAYFILESTMP > $ARQARRAYFILES
        while read filename
        do
            progressx=("$filename")
        done < $ARQARRAYFILES
        ARRAYFILES=(${progressx[0]})
        RET=0
    else
        RET=1
    fi
}

function fnReadTransacao(){

    for linha in $(cat $OUTFILE)
    do 
        TX=`echo "${linha:0:2}"`
        VS=`echo "${linha:2:1}"`
        SHOP=`echo "${linha:3:5}"`
        FECHA=`echo "${linha:8:8}"`
        ALBARAN=`echo "${linha:16:8}"`
        TP=`echo "${linha:24:1}"`
        COD_COMPLEMENTARIO=`echo "${linha:25:5}"`
        
        ARTIGO_1=`echo "${linha:30:6}"`
        TEMP_ARTIGO_1=`seq $ARTIGO_1 $ARTIGO_1`
        ARTIGO_1=`printf "%d" $TEMP_ARTIGO_1`
        QNTD_UND_ART_1=`echo "${linha:36:6}"`
        QNTD_KG_ART_1=`echo "${linha:42:7}"`
        QNTD_PEDIDA_ART_1=`echo "${linha:49:5}"`
        
        ARTIGO_2=`echo "${linha:55:6}"`
        TEMP_ARTIGO_2=`seq $ARTIGO_2 $ARTIGO_2`
        ARTIGO_2=`printf "%d" $TEMP_ARTIGO_2`
        QNTD_UND_ART_2=`echo "${linha:61:6}"`
        QNTD_KG_ART_2=`echo "${linha:67:7}"`
        QNTD_PEDIDA_ART_2=`echo "${linha:74:5}"`
        
        ARTIGO_3=`echo "${linha:80:6}"`
        TEMP_ARTIGO_3=`seq $ARTIGO_3 $ARTIGO_3`
        ARTIGO_3=`printf "%d" $TEMP_ARTIGO_3`
        QNTD_UND_ART_3=`echo "${linha:86:6}"`
        QNTD_KG_ART_3=`echo "${linha:92:7}"`
        QNTD_PEDIDA_ART_3=`echo "${linha:99:5}"`
        
        HOJA_RUTA=`echo "${linha:105:13}"`
        LIVRE=`echo "${linha:118:22}"`
        #printf "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n" $TX $VS $SHOP $FECHA $ALBARAN $TP $COD_COMPLEMENTARIO $ARTIGO_1 $QNTD_UND_ART_1 $QNTD_KG_ART_1 $QNTD_PEDIDA_ART_1 $HOJA_RUTA $LIVRE
        #printf "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n" $TX $VS $SHOP $FECHA $ALBARAN $TP $COD_COMPLEMENTARIO $ARTIGO_2 $QNTD_UND_ART_2 $QNTD_KG_ART_2 $QNTD_PEDIDA_ART_2 $HOJA_RUTA $LIVRE
        #printf "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n" $TX $VS $SHOP $FECHA $ALBARAN $TP $COD_COMPLEMENTARIO $ARTIGO_3 $QNTD_UND_ART_3 $QNTD_KG_ART_3 $QNTD_PEDIDA_ART_3 $HOJA_RUTA $LIVRE
        if [ $ARTIGO_1 != "999999" ] || [ $ARTIGO_2 != "999999" ] || [ $ARTIGO_3 != "999999" ]; then
            fnSendAPI
        fi
    done
}

function fnSendAPI(){

    HOST="10.106.68.149"
    HOSTCTG="10.106.68.78"

    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Teste conexao Server API_1['$HOST']." >> $LOG
    REST=`curl --connect-timeout 5 --request GET "http://$HOST/restapi/v1/api/source/Controllers/MonitoramentoRecepcaoAlbarans.php?token=7c6ce378b1ef0a29180dd36a0d436a91&shop=09999"`
    CHECK_REST=`echo $REST | grep -a -i response | wc -l`
    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Retorno teste conexao Server API_1['$HOST']:['$REST']:['$CHECK_REST']." >> $LOG

    if [ -z $REST ] || [ $CHECK_REST -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Erro ao conectar Server API_1['$HOST']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Teste conexao Server API_2['$HOSTCTG']." >> $LOG
            REST=`curl --connect-timeout 5 --request GET "http://$HOSTCTG/restapi/v1/api/source/Controllers/MonitoramentoRecepcaoAlbarans.php?token=7c6ce378b1ef0a29180dd36a0d436a91&shop=09999"`
            CHECK_REST=`echo $REST | grep -a -i response | wc -l`
            if [ -z $REST ] || [ $CHECK_REST -eq 0 ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Erro ao conectar Server API_2['$HOST']." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Requisicao encerrado por falta de conexao ['$HOST'] && ['$HOSTCTG']." >> $LOG
                    #exit 1
            else
                    HOSTAPI=$HOSTCTG
            fi
    else
            HOSTAPI=$HOST
    fi

    URL="http://$HOST/restapi/v1/api/source/Controllers/MonitoramentoRecepcaoAlbarans.php?token=7c6ce378b1ef0a29180dd36a0d436a91&shop=$SHOP&dataAlbaran=$FECHA&albaran=$ALBARAN&tipoAlbaran=$TP&codComplementario=$COD_COMPLEMENTARIO&artigo=$ARTIGO_1&qntdUndArtigo=$QNTD_UND_ART_1&qntdKGArtigo=$QNTD_KG_ART_1&qntdPedidaArtigo=$QNTD_PEDIDA_ART_1"
    #echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:URL -> [$URL]" >> $LOG
    RETURN=`curl --connect-timeout 5 --request PUT -H "Content-Type: application/json" "$URL"`
    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Retorno Application/json -> [$RETURN]" >> $LOG

    URL="http://$HOST/restapi/v1/api/source/Controllers/MonitoramentoRecepcaoAlbarans.php?token=7c6ce378b1ef0a29180dd36a0d436a91&shop=$SHOP&dataAlbaran=$FECHA&albaran=$ALBARAN&tipoAlbaran=$TP&codComplementario=$COD_COMPLEMENTARIO&artigo=$ARTIGO_2&qntdUndArtigo=$QNTD_UND_ART_2&qntdKGArtigo=$QNTD_KG_ART_2&qntdPedidaArtigo=$QNTD_PEDIDA_ART_2"
    #echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:URL -> [$URL]" >> $LOG
    RETURN=`curl --connect-timeout 5 --request PUT -H "Content-Type: application/json" "$URL"`
    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Retorno Application/json -> [$RETURN]" >> $LOG

    URL="http://$HOST/restapi/v1/api/source/Controllers/MonitoramentoRecepcaoAlbarans.php?token=7c6ce378b1ef0a29180dd36a0d436a91&shop=$SHOP&dataAlbaran=$FECHA&albaran=$ALBARAN&tipoAlbaran=$TP&codComplementario=$COD_COMPLEMENTARIO&artigo=$ARTIGO_3&qntdUndArtigo=$QNTD_UND_ART_3&qntdKGArtigo=$QNTD_KG_ART_3&qntdPedidaArtigo=$QNTD_PEDIDA_ART_3"
    #echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:URL -> [$URL]" >> $LOG
    RETURN=`curl --connect-timeout 5 --request PUT -H "Content-Type: application/json" "$URL"`
    echo "$(date +%Y%m%d-%H%M%S.%s):API_CURL:Retorno Application/json -> [$RETURN]" >> $LOG

}

rm -vf /tmp/intrecepcaoalbaran.log
exec 1>> /tmp/intrecepcaoalbaran.log
exec 2>> /tmp/intrecepcaoalbaran.log
set -x

main