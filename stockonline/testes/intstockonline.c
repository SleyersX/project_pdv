#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sqlite3.h"

static int selectCb(void *nada, int argc, char **argv, char **colNames){
  
  int i;

  for(i=0; i<argc; i++){
     printf("%s => %s\n", colNames[i], argv[i]);
  }
  printf("\n");

  return 0;
}

int main(int argc, char* argv[]){
    
	sqlite3 *db;
	char *error = 0;
	int res,sku;
	char sql[1024], loja[256];
    

	/* Open database */
	//res = sqlite3_open("/home/wam001br/Documentos/projetos/catalog.db", &db);
    res = sqlite3_open("/confdia/movil/catalog.db", &db);
	if (res){
		fprintf(stderr, "Erro ao abir base de dados: %s\n", sqlite3_errmsg(db));
		exit(0);
	}else{
		fprintf(stderr, "Base de datos OK\n");
	}
	printf("Digite o codigo do produto: ");
	scanf("%d", &sku); 
	if (sku >= 105){
    	/* Create SQL statement */
    	snprintf(sql, sizeof(sql), "SELECT ItemID, LoyaltyDescription, PriceAmount, PriceAmountFid, PriceAmountNoFid, CurrentQuantityInUnits, CurrentQuantityInWeight FROM Item WHERE ItemID = '%d';", sku);
    	/* Execute SQL statement */
    	res = sqlite3_exec(db, sql, selectCb, 0, &error);
    	if (res != SQLITE_OK){
			fprintf(stderr, "Error: %s\n", error);
			sqlite3_free(error);
    	}else{
			fprintf(stdout, "SELECT Ok!\n");
    	}
   	 	sqlite3_close(db);
	}
    return 0;
}