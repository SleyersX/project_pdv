#!/bin/bash
#
# Criado por: Walter Moura(wam001br)
# Data Criação: 27.08.2020
# Modificado por: Walter Moura
# Data moficação: 27.08.2020
#
# Script para consultar arquivo INCD003G enviado pela CISS via FTP para o CWS
# 
#
# Version 1.0
# - Fazer o download dos arquivos DCV03YYYYMMDDHHMMSS do CWS
# - Verificar se as lojas informadas possuem itens duplicados


USERACTUAL=$(grep $EUID /etc/group | awk -F ":" '{print $1}')
SHOP=$1
PATH_USER="/tmp/$USERACTUAL"
PATH_LOG="$PATH_USER/cws/logs_INTCD003G"
LOG="$PATH_LOG/error_1.log"
DEBUG="$PATH_LOG/debug.log"
OUT="$PATH_USER/out.txt"
DATA_FIND=`date "+%Y%m%d"`
HOST_FTP="10.105.186.168"
USER_FTP="userinfoci"
PASSWD_FTP="hostciss"
#HOST_FTP="10.106.68.149"
#USER_FTP="ftpciss"
#PASSWD_FTP="diabrasil"
FILE_DOWNLOAD_FTP="DCV03$DATA_FIND"
TEMP_DOWNLOAD_FTP="$PATH_USER/cws/files_INTCD003G"
DIR_FTP_3G="Ciss/CissHost/in"
SCRIPT=`basename $0`
LIST_SHOP="$PATH_USER/cws/list_shop.txt"
LIST_XML="$PATH_USER/cws/list_xml.txt"
LIST_TEMP_XML="$PATH_USER/cws/temp_list_xml.txt"

if [ -d $TEMP_DOWNLOAD_FTP ]; then
    rm -vf $TEMP_DOWNLOAD_FTP/$FILE_DOWNLOAD_FTP* > /dev/null &2>1
fi

function fnMain(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Executando o programa." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Chamanda função ['fnValidaLog']." >> $LOG
    fnValidaLog
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:MAIN:Chamanda função ['fnCopyFile']." >> $LOG
    fnCopyFile
}

function fnValidaLog(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Iniciando função ['fnValidaLog']." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Verificando se o diretório ['$PATH_LOG'] existe." >> $LOG
    if [ -d $PATH_LOG ]; then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Diretório ['$PATH_LOG'] já existe." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Verificando se o arquivo ['$LOG'] é maior que 200mb." >> $LOG
        KB=`du -hsk $LOG | awk -F " " '{print $1;}'`
        if [ $KB -ge 200000 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Arquivo ['$LOG'] é maior que 200mb ['$KB kb']." >> $LOG
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Verificando a quantidades de arquivos ['$LOG*']." >> $LOG
            COUNT=`ls -ltr $LOG* | wc -l`
            if [ $COUNT -gt 1 ]; then
                if [ $COUNT -gt 5 ]; then
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Quantidades de arquivos ['$LOG*']:['$COUNT']." >> $LOG
                    rm -vf $LOG.1
                    for x in $(seq 2 5);
                    do
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG.$x'] -> ['$LOG.$(( x - 1))']." >> $LOG
                        mv -vf $LOG.$x $LOG.$(( x - 1))
                    done
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG'] -> ['$LOG.5']" >> $LOG
                    mv -vf $LOG $LOG.5
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG'] -> ['$LOG.$COUNT']." >> $LOG
                    mv -vf $LOG $LOG.$COUNT
                fi
            else
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Quantidades de arquivos ['$LOG*']:['$COUNT']." >> $LOG
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Movendo arquivo ['$LOG'] -> ['$LOG.$COUNT']." >> $LOG
                mv -vf $LOG $LOG.$COUNT
            fi
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Arquivo ['$LOG'] é menor que 200mb ['$KB kb']." >> $LOG
        fi
    else
        mkdir -p $PATH_LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:VALIDALOG:Diretório ['$PATH_LOG'] não existe." >> $LOG
    fi
}

function fnCopyFile(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando função ['fnCopyFile']." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Verificando se o diretório ['$TEMP_DOWNLOAD_FTP'] existe." >> $LOG
    if [ -d $TEMP_DOWNLOAD_FTP ];then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Diretório ['$TEMP_DOWNLOAD_FTP'] já existe." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando download arquivos ['$FILE_DOWNLOAD_FTP*.xml']." >> $LOG
        fnGetFtp
        if [ $RET -eq 0 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP'] efetuado com sucesso." >> $LOG
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Verificando se recebemos o 3G da loja ['$SHOP'] ." >> $LOG
            fnGroupShop
            i=0
            while [ $i != ${#ARRAYSHOPS[@]} ]
            do
                SHOP=`printf "%d" ${ARRAYSHOPS[i]}`
                fnVerificaShop
                if [ $RET -eq 0 ]; then
                    if [ $RESULTADO -ge 2 ]; then
                        fnNamesFilesINTCD003G
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:3G loja ['$SHOP'] recebido e add ao arquivo ['$LIST_XML']." >> $LOG
                        fnGroupFilesXML
                        RET=$?
                        if [ $RET -eq 0 ]; then
                            x=0
                            y=`printf "%d" ${#ARRAYXMLS[@]}`
                            while [ $x != $y ]
                            do
                                if [ $x -lt $(( $y - 1 )) ];then
                                    FILE_XML_DUPLICATE="${ARRAYXMLS[$x]}"
                                    fnDelFileFTP
                                    RET=$?
                                    if [ $RET -eq  0 ]; then
                                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Removido ['${ARRAYXMLS[$x]}'] com sucesso." >> $LOG
                                    else
                                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao remover arquivo ['${ARRAYXMLS[$x]}']." >> $LOG
                                    fi
                                fi
                                let "x = x +1"
                            done 
                        else
                            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao array arquivos XMLs." >> $LOG
                        fi
                    elif [ $RESULTADO -eq 1 ]; then
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Sem arquivos duplicados para loja ['$SHOP'] informada." >> $LOG
                    else
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Nenhum arquivo encontrado para loja ['$SHOP'] informada." >> $LOG
                    fi
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao verificar 3G loja ['$SHOP']." >> $LOG    
                fi
                let "i = i +1"
            done
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao realizar download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP']." >> $LOG
        fi
    else
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Diretório ['$TEMP_DOWNLOAD_FTP'] não existe." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Criando diretório ['$TEMP_DOWNLOAD_FTP']." >> $LOG        
        mkdir -p $TEMP_DOWNLOAD_FTP
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Iniciando download arquivos ['$FILE_DOWNLOAD_FTP*.xml']." >> $LOG
        fnGetFtp
        if [ $RET -eq 0 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP'] efetuado com sucesso." >> $LOG
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Verificando se recebemos o 3G da loja ['$SHOP'] ." >> $LOG
            fnGroupShop
            i=0
            while [ $i != ${#ARRAYSHOPS[@]} ]
            do
                SHOP=`printf "%d" ${ARRAYSHOPS[i]}`
                fnVerificaShop
                if [ $RET -eq 0 ]; then
                    if [ $RESULTADO -ge 2 ]; then
                        fnNamesFilesINTCD003G
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:3G loja ['$SHOP'] recebido e add ao arquivo ['$LIST_XML']." >> $LOG
                        fnGroupFilesXML
                        RET=$?
                        if [ $RET -eq 0 ]; then
                            x=0
                            y=`printf "%d" ${#ARRAYXMLS[@]}`
                            while [ $x != $y ]
                            do
                                if [ $x -lt $(( $y - 1 )) ];then
                                    FILE_XML_DUPLICATE="${ARRAYXMLS[$x]}"
                                    fnDelFileFTP
                                    RET=$?
                                    if [ $RET -eq  0 ]; then
                                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Removido ['${ARRAYXMLS[$x]}'] com sucesso." >> $LOG
                                    else
                                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao remover arquivo ['${ARRAYXMLS[$x]}']." >> $LOG
                                    fi
                                fi
                                let "x = x +1"
                            done 
                        else
                            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao array arquivos XMLs." >> $LOG
                        fi
                    elif [ $RESULTADO -eq 1 ]; then
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Sem arquivos duplicados para loja ['$SHOP'] informada." >> $LOG
                    else
                        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Nenhum arquivo encontrado para loja ['$SHOP'] informada." >> $LOG
                    fi
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao verificar 3G loja ['$SHOP']." >> $LOG    
                fi
                let "i = i +1"
            done
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:COPYFILE:Erro ao realizar download arquivos ['$FILE_DOWNLOAD_FTP*.xml'] -> ['$TEMP_DOWNLOAD_FTP']." >> $LOG
        fi
    fi
}

function fnGetFtp(){
    ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
    lcd $TEMP_DOWNLOAD_FTP
    cd $DIR_FTP_3G
    prompt
    mget $FILE_DOWNLOAD_FTP*.xml
    prompt
    quit
END_SCRIPT
    RET=$?
}   

function fnGroupShop(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPSHOP:Inciando função." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPSHOP:Verificamos se arquivo ['$LIST_SHOP'] existe." >> $LOG
    if [ -e $LIST_SHOP ]; then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPSHOP:Arquivo OK." >> $LOG
        while read idshop;
            do
                progress=("$idshop")
        done < $LIST_SHOP
        ARRAYSHOPS=(${progress[0]})
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPSHOP:ARRAY Shops -> ['$ARRAYSHOPS']." >> $LOG
    else
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPSHOP:Arquivo NOK ['$LIST_SHOP']." >> $LOG
    fi
}

function fnVerificaShop(){
    RESULTADO=`grep -a -i "idStore=.*$SHOP" $TEMP_DOWNLOAD_FTP/$FILE_DOWNLOAD_FTP* | wc -l`
    RET=$?
}

function fnNamesFilesINTCD003G(){
    grep -a -i "idStore=.*$SHOP" $TEMP_DOWNLOAD_FTP/$FILE_DOWNLOAD_FTP* | awk -F ":" '{print $1;}' | awk -F "/" '{print $6;}' > $LIST_XML
}

function fnGroupFilesXML(){
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Inciando função." >> $LOG
    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Verificamos se arquivo ['$LIST_XML'] existe." >> $LOG
    if [ -e $LIST_XML ]; then
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Arquivo OK." >> $LOG
        VARTEMP=`while read linha ; do if [ -z "$linha" ]; then ERR=$(echo $linha)  ; else  printf "%s\t" $linha ; fi ; done < $LIST_XML`
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Construímos um array com o nome dos arquivos XML e passamos a variável ['VARTEMP']." >> $LOG
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:VARTEMP -> ['$VARTEMP']." >> $LOG
        if [ $? -eq 0 ]; then
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Passamos os valores da varável ['VARTEMP'] -> ['$LIST_XML'].." >> $LOG
            echo $VARTEMP > $LIST_TEMP_XML
            if [ $? -eq 0 ]; then
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Verificamos se arquivo ['$LIST_TEMP_XML'] existe." >> $LOG
                if [ -e $LIST_TEMP_XML ]; then
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Arquivo OK." >> $LOG
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Iniciamos um while read no arquivo ['$LIST_TEMP_XML'] e gravamos cada valor em um array." >> $LOG
                    while read xml;
                        do
                            progress=("$xml")
                    done < $LIST_TEMP_XML
                    ARRAYXMLS=(${progress[0]})
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:ARRAY XMLs -> ['$ARRAYXMLS']." >> $LOG
                else
                    echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Arquivo NOK." >> $LOG
                fi
            else
                echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Erro ao executar comando VARTEMP -> LIST_TEMP_XML -> ['$?']." >> $LOG
            fi
        else
            echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Erro ao executar comando VARTEMP -> WHILE -> ['$?']." >> $LOG
        fi
    else
        echo "[$(date +%d/%m/%Y" - "%H:%M:%S)]:GROUPFILESXML:Arquivo NOK." >> $LOG
    fi
}

function fnDelFileFTP(){
    ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
    cd $DIR_FTP_3G
    prompt
    del $FILE_XML_DUPLICATE
    prompt
    quit
END_SCRIPT
    RET=$?
}   


fnMain