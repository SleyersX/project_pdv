@echo off

set DIR_CONCLUIDO="C:\SIDCISS\INTCD003G\CONCLUIDO"
set DIR_PENDENTE="C:\SIDCISS\INTCD003G\PENDENTE"

forfiles /p %DIR_CONCLUIDO% /s /m *xml /c "cmd /c xcopy @file %DIR_PENDENTE% /Y" /D 0

pause