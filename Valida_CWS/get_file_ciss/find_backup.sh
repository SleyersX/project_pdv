#!/bin/bash

# Script para validação do ultimo 3G de backup enviado para o CWS

# Autor: Marcos Marinho
# Version: 1.0 - Download dos arquivos de backup D-1 - 31/08/2020;
                # Validação da loja nos arquivos baixados;
                # Envio para o servidor FTP do CWS na pasta in.

# Atualização: Marcos Marinho
# Version: 1.1 - Inclusão de loop para verificação dos dias a validar (limite 7) - 31/08/2020;
                 # Inclusão de logs.

# Atualização: Marcos Marinho
# Version: 1.2 - Inclusão do informe por email

# Atualização: Walter Moura
# Version: 1.3 - Modificação para trabalhar com multiplas lojas
#              - Validação dos diretórios

# Atualização: Walter Moura
# Version: 1.4 - Modificação para renomear o arquivo de backup

# Definindo Constantes
SCRIPT=`basename $0`
PATH_ACTUAL=`pwd`
PATH_BACKUP_TMP="$PATH_ACTUAL/tmp_backup"
USER='userinfoci'
PASS='hostciss'
#USER='ftpciss'
#PASS='diabrasil'
LOG=`echo $PATH_ACTUAL/log/$SCRIPT | sed s/'.sh'/'.log'/g`
FTP="10.105.186.168"
#FTP='10.106.68.149'
DIRECTORY_3G="Ciss/CissHost/proc"
DIRECTORY_IN="Ciss/CissHost/in"
SHOP=$1

if [ ! -d $PATH_BACKUP_TMP ]; then
    mkdir -p $PATH_BACKUP_TMP
fi

# Baixando arquivos do CWS
function download_files(){
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [DOWNLOAD_FILES] Baixando arquivos CWS..." >> $LOG
    DATA_FIND=`date "+%Y%m%d" --date="-$1 day"`
    FILE_DOWNLOAD_FTP="DCV03$DATA_FIND"
    ftp -n $FTP <<END_SCRIPT
    quote user $USER 
    quote PASS $PASS
    lcd $PATH_BACKUP_TMP
    cd $DIRECTORY_3G
    prompt
    mget $FILE_DOWNLOAD_FTP*.xml
    prompt
    quit
END_SCRIPT
    RET=$?
}

# Validando se loja existe
function validating_store8056(){
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE8056] Pesquisando arquivo..." >> $LOG
    file_store=`grep -rl $PATH_BACKUP_TMP -e 'idStore="8056"'`
    if [ $? -eq 0 ]; then
        recept="S"
        name_file=`basename $file_store`
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE8056] Arquivo encontrado, enviando arquivo para o CWS" >> $LOG
        send_proc_for_in
            if [ $RET -eq 0 ]; then
                echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE8056] Arquivo enviado com sucesso !!" >> $LOG
            else
                echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE8056] Erro ao enviar arquivo" >> $LOG
            fi
    else
        recept="N"
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE8056] Não encontrado arquivo em $DATA_FIND" >> $LOG
    fi
}

# Validando se loja existe
function validating_store(){
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE_$SHOP] Pesquisando arquivo..." >> $LOG
    file_store=`grep -rl $PATH_BACKUP_TMP -e "idStore=.*$SHOP"`
    if [ $? -eq 0 ]; then
        recept="S"
        name_file=`basename $file_store`
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE_$SHOP] Arquivo encontrado, enviando arquivo para o CWS" >> $LOG
        send_proc_for_in
            if [ $RET -eq 0 ]; then
                echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE_$SHOP] Arquivo enviado com sucesso !!" >> $LOG
            else
                echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE_$SHOP] Erro ao enviar arquivo" >> $LOG
            fi
    else
        recept="N"
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [VALIDATING_STORE_$SHOP] Não encontrado arquivo em $DATA_FIND" >> $LOG
    fi
}

# Enviando arquivo 3G para CWS
function send_proc_for_in(){
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [SEND_PROC_FOR_IN] Enviando arquivo"  >> $LOG
    REMOTE_DIRECTORY="$DIRECTORY_IN/$name_file"
    cd $PATH_BACKUP_TMP
    DT_ACT=`date +%Y%m%d`
    temp_name=`echo $name_file | sed "s/$DATA_FIND/$DT_ACT/g"`
    cp -vf $name_file $temp_name
    new_file=`basename $temp_name`
    ftp -n $FTP <<END_SCRIPT
    quote user $USER
    quote PASS $PASS
    cd $DIRECTORY_IN
    prompt
    lcd $PATH_BACKUP_TMP
    put $new_file
    prompt
    quit
END_SCRIPT
    RET=$?
}


# Deletando arquivos da pasta de backup temporária
function delete_files(){
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [DELETE_FILES] Removendo arquivos"  >> $LOG
    rm -f $PATH_BACKUP_TMP/*.xml
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [DELETE_FILES] Arquivos removidos" >> $LOG
}

function send_email(){
    if [ $RET -eq 0 ]; then
        echo -e "Olá\n\nArquivo de backup enviado para processamento pelo CWS" |\
        mail -s "Monitoramento 3G CWS" pdv.matriz@diagroup.com suporte.franquias@ciss.com.br 


        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [SEND_EMAIL] Email enviado" >> $LOG
    else
        echo -e "Erro ao processar arquivo de backup\nVerificar com URGÊNCIA" |\
        mail -s "Monitoramento 3G CWS" marcos.lima.marinho@diagroup.com walter.araujo.moura@diagroup.com 
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [SEND_EMAIL] Erro no processamento, email enviado" >> $LOG
    fi
}

# Função main
function main(){
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [MAIN] Inicio de Programa" >> $LOG
    for i in $(seq 1 7); do
        download_files $i
        #validating_store8056
        validating_store
        if [ $recept == "S" ];then
            delete_files
            break
        else
            delete_files
        fi
    done
    send_email $RET
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [MAIN] Fim de Programa" >> $LOG
}

if [ ! -z $SHOP ]; then
    main #&>>/dev/null
else
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [MAIN] Não informado o número da loja" >> $LOG
fi
