#!/bin/bash

# Script para copia dos 3Gs das franquias do servidor FTP do CWS
# ./copy_files_ftp.sh &>>/dev/null

# Autor: Marcos Marinho
# Version: V.0.1 - Script Inicial
# Version: V.0.2 - Inclusão de envio de email
# Version: V.0.3 - Funcionalidades iniciais concluidas
# Version: V.0.4 - Adicionado chamada para script de processamento de arquivo de backup
# Version: V.0.5 - Adicionado função para multiplas lojas
# Version: V.0.6 - Adicionado validação dos diretórios
# Version: V.0.7 - Alteração na lista de lojas, agora obtida do banco de dados

# Definindo Constantes
PATH_ACTUAL=`pwd`
TMP="$PATH_ACTUAL/tmp/"
USER='userinfoci'
PASS='hostciss'
#USER='ftpciss'
#PASS='diabrasil'
LOG="$PATH_ACTUAL/log/$0.log"
BACKUP="$PATH_ACTUAL/files_backup"
LIST_SHOP="$PATH_ACTUAL/list_shop.txt"
#FTP='10.106.68.149'
FTP='10.105.186.168'

MYSQL_HOST="10.106.68.78"
MYSQL_USER="dba"
MYSQL_PASS=""
MYSQL_BD="srvremoto"


# Validação de diretórios
if [ ! -d $TMP ]; then
    mkdir -p $TMP
else
	rm -vf $TMP/*
fi
if [ ! -d $PATH_ACTUAL/log ];then
    mkdir -p $PATH_ACTUAL/log
fi


function baixa_xmls(){
    echo "$(date +%d%m%Y-%H:%M:%S) - Copiando arquivos do FTP" >> $LOG

    wget --user=$USER --password=$PASS ftp://$FTP/Ciss/CissHost/in/D* -P $TMP &>>/dev/null

    if [ $? -eq 0 ];then
        total_ficheiros=`ls -l $TMP | wc -l`
        echo "$(date +%d%m%Y-%H:%M:%S) - Ficheiros copiados com sucesso!!" >> $LOG
        echo "$(date +%d%m%Y-%H:%M:%S) - Total de $total_ficheiros copiados com sucesso!!" >> $LOG
        return 0
    else
        echo "$(date +%d%m%Y-%H:%M:%S) - Erro ao copiar ficheiros!!" >> $LOG
        return 1
    fi
}

# Função Vetor de Lojas
function group_shop(){
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [GROUP_SHOP] Iniciando função" >> $LOG
    echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [GROUP_SHOP] Verificamos se arquivo ['$LIST_SHOP'] existe" >> $LOG
    if [ ! -z $LIST_SHOP ]; then
        rm -vf $LIST_SHOP
    fi
    i=1
    COUNT=`mysql -u dba srvremoto -h 10.106.68.78 -N -e "SELECT COUNT(loja) AS total FROM tb_check_cws WHERE status_loja != 0"`
    VAR=`mysql -u dba srvremoto -h 10.106.68.78 -N -e "SELECT loja FROM tb_check_cws WHERE status_loja != 0" | \
    while read linha 
    do 
        if [ $i -eq $COUNT ]; then 
            printf "%d" $linha
        else  
            printf "%d\t" $linha
        fi
            let "i = i +1" 
    done`
    echo $VAR > $LIST_SHOP
    if [ -e $LIST_SHOP ]; then
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [GROUP_SHOP] Arquivo OK" >> $LOG
        while read idshop;
            do
                progress=("$idshop")
        done < $LIST_SHOP
        ARRAYSHOPS=(${progress[0]})
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [GROUP_SHOP] ARRAY Shops -> ['$ARRAYSHOPS']" >> $LOG
    else
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [GROUP_SHOP] Arquivo NOK ['$LIST_SHOP']" >> $LOG
        echo "$(date +%d/%m/%Y" - "%H:%M:%S) - [GROUP_SHOP] Programa encerrado com erros" >> $LOG
        exit 1
    fi
}

function find_8036(){
    store=`grep -rnw $TMP -e 'idStore="8056"'`
    if [ $? -eq 0 ]; then
        echo "$(date +%d%m%Y-%H:%M:%S) - Arquivo 8056 recebido !!" >> $LOG
        recept=1
        echo "$(date +%d%m%Y-%H:%M:%S) - Enviando email " >> $LOG
		send_email_ok
    else
        echo "$(date +%d%m%Y-%H:%M:%S) - Arquivo 8056 não recebido !!" >> $LOG
        recept=0
        echo "$(date +%d%m%Y-%H:%M:%S) - Enviando email " >> $LOG
		send_email_nok
    fi
}

function find_shop(){
    store=`grep -rnw $TMP -e "idStore=.*$SHOP"`
    if [ $? -eq 0 ]; then
        echo "$(date +%d%m%Y-%H:%M:%S) - Arquivo $SHOP recebido !!" >> $LOG
        recept=1
        echo "$(date +%d%m%Y-%H:%M:%S) - Enviando email " >> $LOG
		send_email_ok
    else
        echo "$(date +%d%m%Y-%H:%M:%S) - Arquivo $SHOP não recebido !!" >> $LOG
        recept=0
        echo "$(date +%d%m%Y-%H:%M:%S) - Enviando email " >> $LOG
		send_email_nok
    fi
}


function cria_backup(){
    cd $TMP
    tar -czvf "$BACKUP/backup_$(date +%d%m%Y).tgz" . &>>/dev/null
    if [ $? -eq 0 ]; then
        echo "$(date +%d%m%Y-%H:%M:%S) - Backup Criado com Sucesso !!" >> $LOG
        echo "$(date +%d%m%Y-%H:%M:%S) - Removendo ficheiros do diretorio [$TMP]" >> $LOG
        rm $TMP/*.xml -f &>>/dev/null
    else
        echo "$(date +%d%m%Y-%H:%M:%S) - Erro ao criar backup [$TMP]" >> $LOG
    fi       
}


function send_email_ok(){
    echo -e "Olá\n\nArquivo da franquia $SHOP recebido!!" |\
    mail -s "Monitoramento 3G CWS" pdv.matriz@diagroup.com suporte.franquias@ciss.com.br 

    echo "$(date +%d%m%Y-%H:%M:%S) - Email enviado [$TMP]" >> $LOG
}

function send_email_nok(){
    echo -e "Olá\n\nArquivo da franquia $SHOP NÃO recebido!!" |\
    mail -s "Monitoramento 3G CWS" pdv.matriz@diagroup.com suporte.franquias@ciss.com.br 

    echo "$(date +%d%m%Y-%H:%M:%S) - Email enviado [$TMP]" >> $LOG
}


function main(){
	cd $PATH_ACTUAL
    echo "$(date +%d%m%Y-%H:%M:%S) - Iniciando Programa" >> $LOG
    baixa_xmls
    #find_8036
    group_shop
    i=0
    while [ $i != ${#ARRAYSHOPS[@]} ]
    do
        SHOP=${ARRAYSHOPS[i]}
        find_shop
        if [ $recept == 0 ];then
            echo "$(date +%d%m%Y-%H:%M:%S) - Enviado email de NOK" >> $LOG
            echo "$(date +%d%m%Y-%H:%M:%S) - Chamado rotina de verificação de backup" >> $LOG
            cd $PATH_ACTUAL	
            bash -x find_backup.sh $SHOP > log/${SHOP}_debug_find_backup.log 2>&1
        else
            echo "$(date +%d%m%Y-%H:%M:%S) - Enviado email de OK" >> $LOG
        fi
        let "i = i +1"
    done
    
    echo "$(date +%d%m%Y-%H:%M:%S) - Finalizando Programa !! [$TMP]" >> $LOG
}

main
