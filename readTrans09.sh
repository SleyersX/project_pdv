#!/bin/bash
# Autor: Walter Moura
# Data Criação: 11/10/2019
# Data Modificação: 16/10/2019
# Modificado por: Walter Moura
#
# Script para conectar a loja especificada, e buscar a transação 09, que é de artigos do surtido do franqueado
# 
# Versão 0.01
# -> Função valida conexao (testa se a loja está up antes de executar as demais funções do script)  
# -> Teste caracteres Data e Loja (teste se valores passados são númericos e atendem os pré-requisitos para que o script execute)
# -> Valida/Read File(verifica se arquivos especifico do GN existe na loja e posteriormento realiza a leitura do mesmo e gera um arquivo de saída)
# -> Tratar/Read File de Saída(ajusta o arquivo de saída, eliminando posições do arquivo não necessária para posteriormente o arquivo ser lido)
#
# Versão 0.02
# -> Get IP(metódo para obter o IP da loja através de um banco de dados)
# -> Trata Error(método para tratar alguns retorno e interpretá-los)
# -> Testar se a base de dados existe

#BD="/root/srv_remoto/srv_remoto.db"
BD="/home/wam001br/Documentos/projetos/srv_remoto.db"

function trata_error(){
	# Codigos de error
	#  0 Programa executado com sucesso 
	# -1 Um dos caracteres informados não é um número inteiro
	# -2 Informe até 8 caracteres
	# -3 Loja não existe no banco de dados
	# -4 Informe até 4 caracteres
	# -5 Erro ao conectar com a loja
	# -6 Não foi encontrado nenhum arquivo com a data informada
	# -127 Error ao ler transação e gerar arquivo de saída/transação 09 não existe para esta data ou loja
	# -27 Erro ao tratar arquivo de saida
	# -51 Erro ao executar while e ler o arquivo de saida
	# -99 Base de dados não existe

	case $RETURN in
		0)
			echo "Programa executado com sucesso[$y]."
		;;
		-1)
			echo "Um dos caracteres informados não é um número inteiro."
		;;
		-2)
			echo "Informe até 8 caracteres."
		;;
		-3)
			echo "Loja não existe no banco de dados."
		;;
		-4)
			echo "Informe até 4 caracteres."
		;;
		-5)
			echo "Erro ao conectar com a loja."
		;;
		-6)
			echo "Não foi encontrado nenhum arquivo com a data informada."
		;;
		-27)
			echo "Erro ao tratar arquivo de saida."
		;;
		-51)
			echo "Erro ao executar while e ler o arquivo de saida."
		;;
		-99)
			echo "Base de dados não existe."
		;;
		-127)
			echo "Error ao ler transação e gerar arquivo de saída/transação 09 não existe para esta data ou loja."
		;;
		*)
			echo ""
		;;
	esac
}

function testa_base_de_dados(){
	if [ -e $BD ]; then
		RETURN=0
	else
		RETURN=-99
	fi
}

function valida_conexao(){
	sshpass -p root ssh -o ConnectTimeout=1 -l root -p10001 $ip exit
	if [ $? != 0 ]; then
		RETURN=-5
	else
		RETURN=0
	fi
}

function testa_caractere_data(){	
	if [[ $data = ?(+|-)+([0-9]) ]]; then
		if [ $sizedata -ge 9 ] || [ $sizedata -le 7 ]; then
			RETURN=-2
		else
			RETURN=0
		fi
	else
		RETURN=-1
	fi
}

function testa_caractere_loja(){	
	if [[ $shop = ?(+|-)+([0-9]) ]]; then
		if [ $sizeloja -ge 5 ]; then
			RETURN=-4
		else
			RETURN=0
		fi
	else
		RETURN=-1
	fi
}

function valida_file(){
	echo "DATAFILE=$data" > .dataFile
	echo "NUMESHOP=$shop" >> .dataFile
	echo "CODISHOP=$bshop" >> .dataFile
	chmod 775 .dataFile
	sshpass -p root scp -P10001 .dataFile root@$ip:/tmp/
	COUNTFILE=`sshpass -p root ssh -p10001 -l root $ip '. /tmp/.dataFile ; ls -ltr /confdia/logscomu/${DATAFILE}*CommsGN.tgz | wc -l'`
	if [ $COUNTFILE -ge 1 ]; then
		RETURN=0
	else
		RETURN=-6
	fi
}

function read_transacao(){
	sshpass -p root ssh -p10001 -l root $ip '. /tmp/.dataFile ; for i in /confdia/logscomu/${DATAFILE}*CommsGN.tgz; do tar xzf $i -O confdia/comunica/${CODISHOP}.DGZ | gunzip | fold  -bw140 | grep -a "^09${NUMESHOP}3" ; done' > .saida.txt
	if [ $? != 0 ]; then
		RETURN=-127
	else
		RETURN=0
	fi
}

function tratar_file_saida(){
	cat .saida.txt | sed 's/.\{8\}//' | cut -c 1-120 > .new_saida.txt
	if [ $? != 0 ]; then
		RETURN=-27
	else
		RETURN=0
	fi
}

function read_file_saida(){
	y=0
	while read linha; 
		do 
			CODI1=$(echo $linha | cut -c 1-6)
			PVP1=$(echo $linha | cut -c 7-15)
			PVP1=$(echo "scale=2; $PVP1/100" | bc)
			if [ $CODI1 -eq 999999 ] ; then
				break
			else
				VAR=$(echo $PVP1 | cut -c 1-1)
				if [ "$VAR" == "." ]; then
					PVP1=0$PVP1
				fi
				printf "CODIGO:%s PVP:%s\n" $CODI1 $PVP1
				let "y = y +1"
			fi 
			CODI2=$(echo $linha | cut -c 41-46)
			PVP2=$(echo $linha | cut -c 47-55)
			PVP2=$(echo "scale=2; $PVP2/100" | bc)
			if [ $CODI2 -eq 999999 ]; then
				break
			else
				VAR=$(echo $PVP2 | cut -c 1-1)
				if [ "$VAR" == "." ]; then
					PVP2=0$PVP2
				fi
				printf "CODIGO:%s PVP:%s\n" $CODI2 $PVP2
				let "y = y +1"
			fi
			CODI3=$(echo $linha |cut -c 81-86 )
			PVP3=$(echo $linha | cut -c 87-95)
			PVP3=$(echo "scale=2; $PVP3/100" | bc)
			if [ $CODI3 -eq 999999 ]; then
				break
			else
				VAR=$(echo $PVP3 | cut -c 1-1)
				if [ "$VAR" == "." ]; then
					PVP3=0$PVP3
				fi
				printf "CODIGO:%s PVP:%s\n" $CODI3 $PVP3
				let "y = y +1"
			fi
		done < .new_saida.txt
	
	if [ $? != 0 ]; then
		RETURN=-51
	else
		RETURN=0
	fi
}

function get_ip(){
	EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
	if [ $EXISTE -eq 1 ]; then
		ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$shop';" | sqlite3 $BD`
		RETURN=0
	else
		RETURN=-3
	fi
}

function main(){
	echo "Ler transacao 09 do GN de um dia especifico"
	read -p "Informe o número da loja para buscar os dados [XXXX]:" shop
	read -p "Informe a data a buscar no arquivo [YYYYYMMDD]:" data

	[ "$shop" ] || { echo "Nao informada a loja a ser pesquisada."; exit 1; }
	[ "$data" ] || { echo "Nao informada a data a ser pesquisada."; exit 1; }

	sizeloja=`echo ${#shop}`
	sizedata=`echo ${#data}`
	testa_caractere_loja
	if [ $RETURN != 0 ]; then
		trata_error
	else
		bshop=$shop
		shop=`printf "%05d" $shop`
		testa_caractere_data
		if [ $RETURN != 0 ]; then
			trata_error
		else
			testa_base_de_dados
			if [ $RETURN != 0 ]; then
				trata_error
			else
				get_ip
				if [ $RETURN != 0 ]; then
					trata_error
				else
					valida_conexao
					if [ $RETURN != 0 ];then
						trata_error
					else
						valida_file
						if [ $RETURN != 0 ]; then
							trata_error
						else
							read_transacao
							if [ $RETURN != 0 ]; then
								trata_error
							else
								tratar_file_saida
								if [ $RETURN != 0 ]; then
									trata_error
								else
									read_file_saida
									if [ $RETURN != 0 ]; then
										trata_error
									else
										RETURN=0
										trata_error
									fi
								fi
							fi
						fi
					fi
				fi
			fi
		fi
	fi	
}

main
