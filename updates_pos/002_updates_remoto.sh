#!/bin/bash -x
#
#
#
#
#
DEBUG=/tmp/002_updates_remoto.log
if [ -e $DEBUG ]; then
	mv $DEBUG $DEBUG.old
fi
exec 2>> $DEBUG

. /confdia/bin/setvari

#PARA HOMOLOGACAO COMENTAR A LINHA DE CIMA, E DESCOMENTAR AS DE BAIXO
#NUMETIEN=09999
#NUMECAJA=01

# Configuracao SSH
SSH_CONFIG="-o StrictHostKeyChecking=no -o ConnectTimeout=3 -o BatchMode=yes -o ServerAliveInterval=5 -o ServerAliveCountMax=1 -o TCPKeepAlive=no -o AddressFamily=inet"
SSH_USER_SERVER="srvremoto"
SSH_HOST="192.168.1.23"
SSH_PASS=""
SSH_PORT="22"
SSH_IP_MASTER="10.10.10.1"
SSH_PORT_MASTER="23"
SSH_USER_MASTER="root"

# Configuracao MYSQL
MYSQL_HOST="192.168.1.22"
MYSQL_USER="dba"
MYSQL_DB="srvremoto"

# Diretorio arquivos de versaoe outros
DIR_CONTROL_VERSION="/pesados/control_updates/"
DIR_MASTER="/pesados/control_updates"
FILE_DEPENDENCY_SERVER="/home/srvremoto/updates_pos/dependency.ini"
FILE_TEMP_ARRAY="/root/.array_dependency.txt"
DIR_SERVER_DOWNLOAD="/home/srvremoto/updates_pos/releases"
DIR_LEVEL_3="/confdia/descargas/autoinstall/level_3/"
FILE_EXE=`basename $0`
DIR_EXE="/confdia/script_app"

# Version PDV
VERSION_POS=`cat /confdia/version`

function fnCheckPendingUpdate(){
                           
	( clear && echo -e "\rBUSCAN.ATUALIZACOES:\r\n(NAO DESLIGUE O PDV)\r\n \r") >> /dev/ttyS2
	sleep 5
	# Verifica se a loja existe no controle de version]
	MYSQL_CMD="SELECT COUNT(id) AS total_registros FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE shop = ${NUMETIEN})"
	EXIST_SHOP=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e '$MYSQL_CMD'"`
	RET=$?
	if [ $RET -eq 0 ]; then
		if [ $EXIST_SHOP -ge 1 ]; then
			# Se a loja existe verificamos a versao a ser instalada
			MYSQL_CMD="SELECT version_update FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE shop = ${NUMETIEN}))"
			VERSION_UPDATE=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e '$MYSQL_CMD'"`
			RET=$?
			if [ $RET -eq 0 ]; then
				# Comprovamos se a versao a ser instalada e diferente da atual no PDV
				if [ "$VERSION_UPDATE" == "$VERSION_POS" ]; then
					echo "INFO   : Versao atual do pdv e a mais atual disponivel."
					( clear && echo -e "\rBUSCAN.ATUALIZACOES:\r\n(NAO DESLIGUE O PDV)\r\nSEM UPDATES DISPON. \r\n \r") >> /dev/ttyS2
					sleep 5
				else
					echo "INFO   : Versao distinta, comprovamos se a necessidade de atualizar."
					TEMP_VERSION_POS=`echo $VERSION_POS | sed -e 's/SA\.//g' -e 's/\.BRA-//g' -e 's/\.//g' -e 's/0//g'`
					TEMP_VERSION_UPDATE=`echo $VERSION_UPDATE | sed -e 's/SA\.//g' -e 's/\.BRA-//g' -e 's/\.//g' -e 's/0//g'`
					if [ $TEMP_VERSION_POS -ge $TEMP_VERSION_UPDATE ]; then
						echo "INFO   : Versao atual do pdv e maior ou igual a do servidor de versao."
						exit 1
					else
						echo "WARNING: Versao inferior."
						# Verificamos se a versao a ser instalada já esta na Master
						if [ -d "$DIR_CONTROL_VERSION" ]; then
							if [ -d "$DIR_CONTROL_VERSION$VERSION_UPDATE" ]; then
								echo "INFO   : Diretorios ja existem."
								# Verficamos as dependecias de versoes anterires
								SSH_CMD="cat $FILE_DEPENDENCY_SERVER | grep -a -i ^$VERSION_UPDATE"
								TEMP_DEPENDENCY=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | awk -F '=' '{print $2;}'`
								echo "WARNING: Dependencias ['$TEMP_DEPENDENCY']."
								echo $TEMP_DEPENDENCY > $FILE_TEMP_ARRAY
								fnArrayFileTempDependency
								RET=$?
								if [ $RET -eq 0 ]; then
									VERSION_CHECK=$VERSION_POS
									fnCheckDependecy
									RET=$?
									if [ $RET -eq 0 ]; then
										# Verificamos precisamos baixar alguma versao
										if [ $DEP -eq 0 ]; then
											while true; 
											do
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													fnCheckPosDependency
												else
													break
												fi
											done
											RET=$?
											if [ $RET -eq 0 ]; then
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												else
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												fi
											else
												echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."
											fi
										else
											echo "INFO   : Nao ha necessidade de baixar nenhuma versao."
											VERSION_DOWNLOAD=$VERSION_UPDATE
											fnDownloadVersionUpdate
										fi
									else
										echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."	
									fi
								else
									echo "ERROR  : Erro durante a execucao do ARRAY_DEPENDENCY."
								fi
							else
								echo "WARNING: Diretorio versao ['VERSION_UPDATE'] nao existe."
								echo "WARNING: Criando diretorio versao ['VERSION_UPDATE']."
								mkdir $DIR_CONTROL_VERSION$VERSION_UPDATE
								# Verficamos as dependecias de versoes anterires
								SSH_CMD="cat $FILE_DEPENDENCY_SERVER | grep -a -i ^$VERSION_UPDATE"
								TEMP_DEPENDENCY=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | awk -F '=' '{print $2;}'`
								echo "WARNING: Dependencias ['$TEMP_DEPENDENCY']."
								echo $TEMP_DEPENDENCY > $FILE_TEMP_ARRAY
								fnArrayFileTempDependency
								RET=$?
								if [ $RET -eq 0 ]; then
									VERSION_CHECK=$VERSION_POS
									fnCheckDependecy
									RET=$?
									if [ $RET -eq 0 ]; then
										# Verificamos precisamos baixar alguma versao
										if [ $DEP -eq 0 ]; then
											while true; 
											do
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													fnCheckPosDependency
												else
													break
												fi
											done
											RET=$?
											if [ $RET -eq 0 ]; then
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												else
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												fi
											else
												echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."
											fi
										else
											echo "INFO   : Nao ha necessidade de baixar nenhuma versao."
											VERSION_DOWNLOAD=$VERSION_UPDATE
											fnDownloadVersionUpdate
										fi
									else
										echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."	
									fi
								else
									echo "ERROR  : Erro durante a execucao do ARRAY_DEPENDENCY."
								fi
							fi
						else
							echo "WARNING: Diretorio ['$DIR_CONTROL_VERSION'] nao existe."
							echo "WARNING: Criando diretorio ['$DIR_CONTROL_VERSION']"
							mkdir $DIR_CONTROL_VERSION
							if [ -d "$DIR_CONTROL_VERSION$VERSION_UPDATE" ]; then
								echo "INFO   : Diretorios ja existem."
								# Verficamos as dependecias de versoes anterires
								SSH_CMD="cat $FILE_DEPENDENCY_SERVER | grep -a -i ^$VERSION_UPDATE"
								TEMP_DEPENDENCY=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | awk -F '=' '{print $2;}'`
								echo "WARNING: Dependencias ['$TEMP_DEPENDENCY']."
								echo $TEMP_DEPENDENCY > $FILE_TEMP_ARRAY
								fnArrayFileTempDependency
								RET=$?
								if [ $RET -eq 0 ]; then
									VERSION_CHECK=$VERSION_POS
									fnCheckDependecy
									RET=$?
									if [ $RET -eq 0 ]; then
										# Verificamos precisamos baixar alguma versao
										if [ $DEP -eq 0 ]; then
											while true; 
											do
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													fnCheckPosDependency
												else
													break
												fi
											done
											RET=$?
											if [ $RET -eq 0 ]; then
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												else
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												fi
											else
												echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."
											fi
										else
											echo "INFO   : Nao ha necessidade de baixar nenhuma versao."
											VERSION_DOWNLOAD=$VERSION_UPDATE
											fnDownloadVersionUpdate
										fi
									else
										echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."	
									fi
								else
									echo "ERROR  : Erro durante a execucao do ARRAY_DEPENDENCY."
								fi
							else
								echo "WARNING: Diretorio versao ['$VERSION_UPDATE'] nao existe."
								echo "WARNING: Criando diretorio versao ['$VERSION_UPDATE']."
								mkdir $DIR_CONTROL_VERSION$VERSION_UPDATE
								# Verficamos as dependecias de versoes anterires
								SSH_CMD="cat $FILE_DEPENDENCY_SERVER | grep -a -i ^$VERSION_UPDATE"
								TEMP_DEPENDENCY=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | awk -F '=' '{print $2;}'`
								echo "WARNING: Dependencias ['$TEMP_DEPENDENCY']."
								echo $TEMP_DEPENDENCY > $FILE_TEMP_ARRAY
								fnArrayFileTempDependency
								RET=$?
								if [ $RET -eq 0 ]; then
									VERSION_CHECK=$VERSION_POS
									fnCheckDependecy
									RET=$?
									if [ $RET -eq 0 ]; then
										# Verificamos precisamos baixar alguma versao
										if [ $DEP -eq 0 ]; then
											while true; 
											do
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													fnCheckPosDependency
												else
													break
												fi
											done
											RET=$?
											if [ $RET -eq 0 ]; then
												if [ $DEP -eq 0 ]; then
													echo "WARNING: E necessario baixar versoes dependentes ['$VERSION_DEP']."
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												else
													VERSION_DOWNLOAD=$VERSION_DEP
													fnDownloadVersionUpdate
												fi
											else
												echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."
											fi
										else
											echo "INFO   : Nao ha necessidade de baixar nenhuma versao."
											VERSION_DOWNLOAD=$VERSION_UPDATE
											fnDownloadVersionUpdate
										fi
									else
										echo "ERROR  : Erro durante a execucao do CHECK_DEPENDENCY."	
									fi
								else
									echo "ERROR  : Erro durante a execucao do ARRAY_DEPENDENCY."
								fi
							fi
						fi
					fi
				fi
			else
				echo "ERROR  : Erro ao executar comando MYSQL_VERSIOB_UPDATE ['$RET']"
			fi
		else
			echo "INFO   : Loja ['${NUMETIEN}'] nao existe no banco de dados."
			( clear && echo -e "\rBUSCAN.ATUALIZACOES:\r\n(NAO DESLIGUE O PDV)\r\nSEM UPDATES DISPON. \r\n \r") >> /dev/ttyS2
			sleep 5
		fi
	else
		echo "ERROR  : Erro ao executar comando MYSQL_EXIST_SHOP ['$RET']"
	fi

}

function fnArrayFileTempDependency(){

	while read version;
	do
		progress=("$version")
	done < $FILE_TEMP_ARRAY
	ARRAY_DEPENDENCY=(${progress[0]})

}

function fnCheckDependecy(){

	DEP=0
	i=0
	while [ $i != ${#ARRAY_DEPENDENCY[@]} ];
	do
		if [ ${ARRAY_DEPENDENCY[$i]} == $VERSION_CHECK ]; then
			DEP=1
		fi
		if [ $i != ${#ARRAY_DEPENDENCY[@]} ] && [ $DEP -eq 0 ]; then
			VERSION_DEP=${ARRAY_DEPENDENCY[$i]}
		fi
		let "i = i +1"
	done

}

function fnCheckPosDependency(){

	SSH_CMD="cat $FILE_DEPENDENCY_SERVER | grep -a -i ^$VERSION_DEP"
	TEMP_POS_DEPENDENCY=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | awk -F '=' '{print $2;}'`
	echo "WARNING: Dependencias ['$TEMP_POS_DEPENDENCY']."
	echo $TEMP_POS_DEPENDENCY > $FILE_TEMP_ARRAY
	VERSION_CHECK=$VERSION_POS
	fnArrayFileTempDependency
	fnCheckDependecy

}

function fnDownloadVersionUpdate(){

	echo "INFO   : Version download ['$VERSION_DOWNLOAD']."
	echo "INFO   : Verificamos a data envio da versao."

	TODAY=`date +%Y%m%d`
	MYSQL_CMD="SELECT date_send FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE shop = ${NUMETIEN}))"
	DATA_SEND=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e '$MYSQL_CMD'"`
	RET=$?
	if [ $RET -eq 0 ]; then
		if [ -n $DATA_SEND ]; then
			if [ $TODAY -eq $DATA_SEND ] || [ $TODAY -ge $DATA_SEND ]; then
	 			if [ -d $DIR_CONTROL_VERSION$VERSION_DOWNLOAD ]; then
					echo "INFO   : Diretorio versao ['$VERSION_DOWNLOAD'] ja existe."
					SSH_CMD="md5sum $DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD"
					MD5_VERSION_DOWNLOAD=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | cut -d " " -f1 | tr -d "\n"`
					RET=$?
					if [ $RET -eq 0 ]; then
						MD5_VERSION_DIR=`md5sum $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD | cut -d " " -f1 | tr -d "\n"`
						RET=$?
						if [ $RET -eq 0 ]; then
							if [ "$MD5_VERSION_DOWNLOAD" == "$MD5_VERSION_DIR" ]; then
								echo "INFO   : MD5 OK ['$MD5_VERSION_DOWNLOAD'] == ['$MD5_VERSION_DIR']."
								echo "INFO   : Verifica data de instalacao."
								MYSQL_CMD="SELECT date_install FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE shop = ${NUMETIEN})"
								DATA_INSTALL=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e '$MYSQL_CMD'"`
								RET=$?
								if [ $RET -eq 0 ]; then
									if [ -n $DATA_INSTALL ]; then
										if [ $TODAY -eq $DATA_INSTALL ] || [ $TODAY -ge $DATA_INSTALL ]; then
											tar -xzvf $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD -C $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/
											RET=$?
											if [ $RET -eq 0 ]; then
												echo "INFO   : Arquivo ['file.$VERSION_DOWNLOAD'] descoprimido com sucesso."
												if [ -d $DIR_LEVEL_3 ]; then
													echo "INFO   : Diretorio ['$DIR_LEVEL_3'] ja existe."
												else
													echo "WARNING: Diretorio ['$DIR_LEVEL_3'] nao existe."
													echo "WARNING: Criando diretorio ['$DIR_LEVEL_3']."
													mkdir /confdia/descargas/autoinstall/
													mkdir $DIR_LEVEL_3
												fi
												echo "INFO   : Copiando arquivo para diretorio de instalacao automatica."
												cp -vf $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/package_ready.rpm $DIR_LEVEL_3
												RET=$?
												if [ $RET -eq 0 ]; then
													echo "INFO   : Copiado com sucesso."
													(/etc/rc.d/install.sh || ( clear && echo -e "\rERRO NO PROCESSO\r\n(INICIANDO CAIXA)\r\n \r") >> /dev/ttyS2)
													RET=$?
													if [ $RET -eq 0 ]; then
														echo "INFO   : Instalacao['$VERSION_DOWNLOAD'] realizada com sucesso."
														echo "INFO   : Vericando se ainda temos instalacoes pendentes."
														bash $DIR_EXE/$FILE_EXE
													else
														( clear && echo -e "\rERRO NO PROCESSO\r\n(INICIANDO CAIXA)\r\n \r") >> /dev/ttyS2
														echo "ERROR  : Erro durante o processo de instalacao versao ['$VERSION_DOWNLOAD']."
													fi
												else
													echo "ERROR  : Erro durante o processo de copia."	
												fi	
											else
												echo "ERROR  : Erro ao descomprimir  arquivo ['file.$VERSION_DOWNLOAD']['$RET']"	
											fi
										else
											echo "WARNING: Data de instalacao maior que a data atual."
											( clear && echo -e "\rBUSCAN.ATUALIZACOES:\r\n(NAO DESLIGUE O PDV)\r\nSEM UPDATES DISPON. \r\n \r") >> /dev/ttyS2
											sleep 5
										fi
									else
										echo "WARNING: Loja ['$NUMETIEN'] sem data para instalacao."
									fi
								else
									echo "ERROR  : Erro ao executar comando MYSQL_EXIST_SHOP ['$RET']"
								fi
							else
								echo "WARNING: MD5 NOK ['$MD5_VERSION_DOWNLOAD'] != ['$MD5_VERSION_DIR']."
								SSH_CMD="ls -ltr $DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD"
								KB_REMOTO=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | awk -F " " '{print $5;}'`
								echo "INFO   : Verificando se e MASTER ou SLAVE."
								if [ $NUMECAJA -eq 1 ]; then
									echo "INFO   : Caixa identificado ['$NUMECAJA']:['MASTER']."
									nohup scp -v $SSH_CONFIG -P$SSH_PORT $SSH_USER_SERVER@$SSH_HOST:$DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &
								else
									echo "INFO   : Caixa identificado ['$NUMECAJA']:['CAJA_${NUMECAJA}']."
									SSH_CMD="ls -ltr $DIR_MASTER/$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD"
									KB_MASTER=`ssh -p $SSH_PORT_MASTER -l $SSH_USER_MASTER $SSH_IP_MASTER "$SSH_CMD" | awk -F " " '{print $5;}'`
									RET=$?
									if [ $RET -eq  0 ]; then
										echo "INFO   : Verificando se os arquivos existem na MASTER para copialos."
										if [ $KB_MASTER -ge $KB_REMOTO ]; then
											echo "INFO   : Iniciacion copia da master."
											nohup scp -v $SSH_CONFIG -P$SSH_PORT_MASTER $SSH_USER_MASTER@$SSH_IP_MASTER:$DIR_MASTER/$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &
										else
											echo "WARNING: Arquivos da master estao incompletos, iniciando copia do servidor remoto."
											nohup scp -v $SSH_CONFIG -P$SSH_PORT $SSH_USER_SERVER@$SSH_HOST:$DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &	
										fi
									else
										echo "ERROR  : Erro ao verificar arquivos na master ['$RET']."
										echo "INFO   : Copiando do servidor remoto."
										nohup scp -v $SSH_CONFIG -P$SSH_PORT $SSH_USER_SERVER@$SSH_HOST:$DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &	
									fi
								fi
								while true; do 
									if [ -e $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD ]; then 
										KB_LOCAL=`ls -ltr $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD | awk -F " " '{print $5;}'`
										if [ $KB_REMOTO -eq $KB_LOCAL ]; then 
											( clear && echo -e "\rTRASNF.FICHERO:100%\r\n(NAO APAGUE O PDV)\r\n \r")
											break
										else 
											( clear && echo -e "\rTRASNF.FICHERO:$(div $KB_LOCAL $KB_REMOTO)\r\n(NAO APAGUE O PDV)\r\n")
										fi 
									else 
										( clear && echo -e "\rTRASNF.FICHERO:0%\r\n(NAO APAGUE O PDV)\r\n")
									fi
									sleep 1
								done > /dev/ttyS2
								RET=$?
								if [ $RET -eq 0 ]; then
									echo "INFO   : Versao ['$VERSION_DOWNLOAD'] baixada com sucesso."
									echo "INFO   : Verifica data de instalacao."
									MYSQL_CMD="SELECT date_install FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE shop = ${NUMETIEN})"
									DATA_INSTALL=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e '$MYSQL_CMD'"`
									RET=$?
									if [ $RET -eq 0 ]; then
										if [ -n $DATA_INSTALL ]; then
											if [ $TODAY -eq $DATA_INSTALL ] || [ $TODAY -ge $DATA_INSTALL ]; then
												tar -xzvf $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD -C $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/
												RET=$?
												if [ $RET -eq 0 ]; then
													echo "INFO   : Arquivo ['file.$VERSION_DOWNLOAD'] descoprimido com sucesso."
													if [ -d $DIR_LEVEL_3 ]; then
														echo "INFO   : Diretorio ['$DIR_LEVEL_3'] ja existe."
													else
														echo "WARNING: Diretorio ['$DIR_LEVEL_3'] nao existe."
														echo "WARNING: Criando diretorio ['$DIR_LEVEL_3']."
														mkdir /confdia/descargas/autoinstall/
														mkdir $DIR_LEVEL_3
													fi
													echo "INFO   : Copiando arquivo para diretorio de instalacao automatica."
													cp -vf $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/package_ready.rpm $DIR_LEVEL_3
													RET=$?
													if [ $RET -eq 0 ]; then
														echo "INFO   : Copiado com sucesso."
														(/etc/rc.d/install.sh || ( clear && echo -e "\rERRO NO PROCESSO\r\n(INICIANDO CAIXA)\r\n \r") >> /dev/ttyS2)
														RET=$?
														if [ $RET -eq 0 ]; then
															echo "INFO   : Instalacao['$VERSION_DOWNLOAD'] realizada com sucesso."
															echo "INFO   : Vericando se ainda temos instalacoes pendentes."
															bash $DIR_EXE/$FILE_EXE
														else
															( clear && echo -e "\rERRO NO PROCESSO\r\n(INICIANDO CAIXA)\r\n \r") >> /dev/ttyS2
															echo "ERROR  : Erro durante o processo de instalacao versao ['$VERSION_DOWNLOAD']."
														fi
													else
														echo "ERROR  : Erro durante o processo de copia."	
													fi	
												else
													echo "ERROR  : Erro ao descomprimir  arquivo ['file.$VERSION_DOWNLOAD']['$RET']"	
												fi
											else
												echo "WARNING: Data de instalacao maior que a data atual."
												( clear && echo -e "\rBUSCAN.ATUALIZACOES:\r\n(NAO DESLIGUE O PDV)\r\nSEM UPDATES DISPON. \r\n \r") >> /dev/ttyS2
												sleep 5
											fi
										else
											echo "WARNING: Loja ['$NUMETIEN'] sem data para instalacao."
										fi
									else
										echo "ERROR  : Erro ao executar comando MYSQL_EXIST_SHOP ['$RET']"
									fi
								else
									echo "ERROR  : Erro ao realizar o download da versao ['$VERSION_DOWNLOAD']['$RET']"
								fi
							fi		
						else
							echo "ERROR  : Erro ao verificar md5sum ['$DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files,$VERSION_DOWNLOAD']['$RET']"	
						fi
					else
						echo "ERROR  : Erro ao verificar md5sum ['$DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD']['$RET']"
					fi
				else
					echo "WARNING: Diretorio versao ['$VERSION_DOWNLOAD'] nao existe."
					echo "WARNING: Criando diretorio versao ['$VERSION_DOWNLOAD']."
					mkdir $DIR_CONTROL_VERSION$VERSION_DOWNLOAD
					SSH_CMD="ls -ltr $DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD"
					KB_REMOTO=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "$SSH_CMD" | awk -F " " '{print $5;}'`
					echo "INFO   : Verificando se e MASTER ou SLAVE."
					if [ $NUMECAJA -eq 1 ]; then
						echo "INFO   : Caixa identificado ['$NUMECAJA']:['MASTER']."
						nohup scp -v $SSH_CONFIG -P$SSH_PORT $SSH_USER_SERVER@$SSH_HOST:$DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &
					else
						echo "INFO   : Caixa identificado ['$NUMECAJA']:['CAJA_${NUMECAJA}']."
						SSH_CMD="ls -ltr $DIR_MASTER/$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD"
						KB_MASTER=`ssh -p $SSH_PORT_MASTER -l $SSH_USER_MASTER $SSH_IP_MASTER "$SSH_CMD" | awk -F " " '{print $5;}'`
						RET=$?
						if [ $RET -eq  0 ]; then
							echo "INFO   : Verificando se os arquivos existem na MASTER para copialos."
							if [ $KB_MASTER -ge $KB_REMOTO ]; then
								echo "INFO   : Iniciacion copia da master."
								nohup scp -v $SSH_CONFIG -P$SSH_PORT_MASTER $SSH_USER_MASTER@$SSH_IP_MASTER:$DIR_MASTER/$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &
							else
								echo "WARNING: Arquivos da master estao incompletos, iniciando copia do servidor remoto."
								nohup scp -v $SSH_CONFIG -P$SSH_PORT $SSH_USER_SERVER@$SSH_HOST:$DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &	
							fi
						else
							echo "ERROR  : Erro ao verificar arquivos na master ['$RET']."
							echo "INFO   : Copiando do servidor remoto."
							nohup scp -v $SSH_CONFIG -P$SSH_PORT $SSH_USER_SERVER@$SSH_HOST:$DIR_SERVER_DOWNLOAD/dia-update-$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD $DIR_CONTROL_VERSION$VERSION_DOWNLOAD > /dev/null 2>&1 &	
						fi
					fi
					while true; do 
						if [ -e $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD ]; then 
							KB_LOCAL=`ls -ltr $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD | awk -F " " '{print $5;}'`
							if [ $KB_REMOTO -eq $KB_LOCAL ]; then 
								( clear && echo -e "\rTRASNF.FICHERO:100%\r\n(NAO APAGUE O PDV)\r\n \r")
								break
							else 
								( clear && echo -e "\rTRASNF.FICHERO:$(div $KB_LOCAL $KB_REMOTO)\r\n(NAO APAGUE O PDV)\r\n")
							fi 
						else 
							( clear && echo -e "\rTRASNF.FICHERO:0%\r\n(NAO APAGUE O PDV)\r\n")
						fi
						sleep 1
					done > /dev/ttyS2
					RET=$?
					if [ $RET -eq 0 ]; then
						echo "INFO   : Verifica data de instalacao."
						MYSQL_CMD="SELECT date_install FROM tb_control_version WHERE id=(SELECT MAX(id) FROM tb_control_version WHERE shop = ${NUMETIEN})"
						DATA_INSTALL=`ssh $SSH_CONFIG -p $SSH_PORT -l $SSH_USER_SERVER $SSH_HOST "mysql -u $MYSQL_USER $MYSQL_DB -h $MYSQL_HOST -N -e '$MYSQL_CMD'"`
						RET=$?
						if [ $RET -eq 0 ]; then
							if [ -n $DATA_INSTALL ]; then
								if [ $TODAY -eq $DATA_INSTALL ] || [ $TODAY -ge $DATA_INSTALL ]; then
									tar -xzvf $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/files.$VERSION_DOWNLOAD -C $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/
									RET=$?
									if [ $RET -eq 0 ]; then
										echo "INFO   : Arquivo ['file.$VERSION_DOWNLOAD'] descoprimido com sucesso."
										if [ -d $DIR_LEVEL_3 ]; then
											echo "INFO   : Diretorio ['$DIR_LEVEL_3'] ja existe."
										else
											echo "WARNING: Diretorio ['$DIR_LEVEL_3'] nao existe."
											echo "WARNING: Criando diretorio ['$DIR_LEVEL_3']."
											mkdir /confdia/descargas/autoinstall/
											mkdir $DIR_LEVEL_3
										fi
										echo "INFO   : Copiando arquivo para diretorio de instalacao automatica."
										cp -vf $DIR_CONTROL_VERSION$VERSION_DOWNLOAD/package_ready.rpm $DIR_LEVEL_3
										RET=$?
										if [ $RET -eq 0 ]; then
											echo "INFO   : Copiado com sucesso."
											(/etc/rc.d/install.sh || ( clear && echo -e "\rERRO NO PROCESSO\r\n(INICIANDO CAIXA)\r\n \r") >> /dev/ttyS2)
											RET=$?
											if [ $RET -eq 0 ]; then
												echo "INFO   : Instalacao['$VERSION_DOWNLOAD'] realizada com sucesso."
												echo "INFO   : Vericando se ainda temos instalacoes pendentes."
												bash $DIR_EXE/$FILE_EXE
											else
												( clear && echo -e "\rERRO NO PROCESSO\r\n(INICIANDO CAIXA)\r\n \r") >> /dev/ttyS2
												echo "ERROR  : Erro durante o processo de instalacao versao ['$VERSION_DOWNLOAD']."
											fi
										else
											echo "ERROR  : Erro durante o processo de copia."	
										fi	
									else
										echo "ERROR  : Erro ao descomprimir  arquivo ['file.$VERSION_DOWNLOAD']['$RET']"	
									fi
								else
									echo "WARNING: Data de instalacao maior que a data atual."
									( clear && echo -e "\rBUSCAN.ATUALIZACOES:\r\n(NAO DESLIGUE O PDV)\r\nSEM UPDATES DISPON. \r\n \r") >> /dev/ttyS2
									sleep 5
								fi
							else
								echo "WARNING: Loja ['$NUMETIEN'] sem data para instalacao."
							fi
						else
							echo "ERROR  : Erro ao executar comando MYSQL_EXIST_SHOP ['$RET']"
						fi
					else
						echo "ERROR  : Erro ao realizar o download da versao ['$VERSION_DOWNLOAD']['$RET']"
					fi
				fi
			else
				echo "WARNING: Data de envio maior que a data atual."	
			fi
		else
			echo "WARNING: Loja ['$NUMETIEN'] sem data para envio."
		fi
	else
		echo "ERROR  : Erro ao executar comando MYSQL_EXIST_SHOP ['$RET']"
	fi

}

function main(){

	fnCheckPendingUpdate

}

main
