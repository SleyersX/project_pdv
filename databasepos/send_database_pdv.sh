#!/bin/bash

# Programa 'send_database_pdv.sh' para captura de informações do PDV e envio para base de dados
# Autores: Marcos Marinho & Walter Moura
# Versão: 0.1 - Inclusão dos escopos das principais funções - 13/02/2020

# Constantes
IP_SERVER='10.106.68.149'
CPU=`/usr/bin/detectpos`

. /confdia/bin/setvari

NUM_SHOP=${NUMETIEN}
NUM_CHECKOUT=${NUMECAJA}

# Pegando informações de Hardware
function get_info_hardware(){

	
	if [ "$CPU" == "F2" ]; then
		MEMORY_0=`dmidecode -t memory | grep -a "Memory Device" -A 17 | grep "Locator: M1" -B3 | sed "s/.\{1\}//" | awk -F "Size:" '{printf $2;}'`
		MEMORY_1=`dmidecode -t memory | grep -a "Memory Device" -A 17 | grep "Locator: M2" -B3 | sed "s/.\{1\}//" | awk -F "Size:" '{printf $2;}'`
		DISK_0=``
		DISK_1=``
		PROCESSOR=`dmidecode -t processor | grep -a "Manufacturer:" | sed "s/.\{1\}//" | cut -d ":" -f2 | sed "s/.\{1\}//"`
	elif [ "$CPU" == "G41" ]; then
		MEMORY_0=`dmidecode -t memory | grep -a "Memory Device" -A 17 | grep "Locator: DIMM0" -B3 | sed "s/.\{1\}//" | awk -F "Size:" '{printf $2;}'`
		MEMORY_1=`dmidecode -t memory | grep -a "Memory Device" -A 17 | grep "Locator: DIMM1" -B3 | sed "s/.\{1\}//" | awk -F "Size:" '{printf $2;}'`
		DISK_0=``
		DISK_1=``
		PROCESSOR=`dmidecode -t processor | grep -a "Version" | sed "s/.\{1\}//" | cut -d ":" -f2 | sed "s/.\{1\}//"`
	elif [ "$CPU" == "K2" ]; then
		MEMORY_0=`dmidecode -t memory | grep -a "Memory Device" -A 17 | grep "Locator: ChannelA-DIMM0" -B3 | sed "s/.\{1\}//" | awk -F "Size:" '{printf $2;}'`
		MEMORY_1=`dmidecode -t memory | grep -a "Memory Device" -A 17 | grep "Locator: ChannelB-DIMM0" -B3 | sed "s/.\{1\}//" | awk -F "Size:" '{printf $2;}'`
		DISK_0=``
		DISK_1=``
		PROCESSOR=`dmidecode -t processor | grep -a "Version" | sed "s/.\{1\}//" | cut -d ":" -f2 | sed "s/.\{1\}//"`
	fi
	
	SERIAL_NUMBER_CPU=`dmidecode -t 3 | grep -a "Serial Number:" | sed "s/.\{16\}//" | awk -F " " '{printf $2;}'`
	if [ "$TIPO_IMPR" == "DR700" ]; then 
	VAR=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-2`
		if [ "$VAR" ==  "D" ] ; then 
			PRINTER=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | cut -d "|" -f1` 
		else 
			PRINTER=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" |  cut -d "|" -f1`
		fi
	elif [ "$TIPO_IMPR" == "TM88" ]; then 
		PRINTER=`echo "TM88"`
	else 
		PRINTER=$(echo "FS700") 
	fi
	RAID=`lspci | grep -a RAID | wc -l | tr -d " "`
	DISK_SIZE=`dmesg | grep -a "sd.*.0.*sda.*logical" | awk -F ":" '{printf $6;}' | sed "s/.\{1\}//"`
	
	#echo "$NUM_SHOP""|""$NUM_CHECKOUT""|""$CPU""|""$SERIAL_NUMBER_CPU""|""$RAID""|""$DISK_0""|""$DISK_SIZE""|""$PROCESSOR""|""$MEMORY_0""|""$MEMORY_1""|""$PRINTER"
	return 0

}


# Pegando informações de Software
function get_info_software(){
	VERSION_POS=`cat /confdia/version`
	VERSION_BIOS=`dmidecode -t bios | grep -a "Version" | sed "s/.\{1\}//" | cut -d ":" -f2`
	VERSION_KERNEL=`uname -r`
	if [ "$TIPO_IMPR" == "DR700" ]; then 
	VAR2=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/Auditoria_DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-2`
		if [ "$VAR2" ==  "D" ] ; then 
			FIRMWARE_PRINTER=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" | cut -d "|" -f2` 
		else 
			FIRMWARE_PRINTER=`cat /srv/Debian6.0/srv/servidorDFW/ficcaje/*DUAL.txt | grep "#### IMPRESSORA:"  | tail -n1 | sed "s/.\{28\}//;s/#### IMPRESSORA://;s/VERSAO:/|/;s/ ####//" | cut -c 2-30 | sed "s/ //g" |  cut -d "|" -f2`
		fi
	elif [ "$TIPO_IMPR" == "TM88" ]; then 
		FIRMWARE_PRINTER=0
	else 
		FIRMWARE_PRINTER=0 
	fi

	# echo "$VERSION_BIOS""|""$VERSION_POS""|""$VERSION_KERNEL""|""$VAR2""|""$FIRMWARE_PRINTER"
	return 0
}


# Capturando informações sobre o status da porta COM
function get_info_ports(){
	return 0
}


# Capturando informações de configuração
function get_info_configuration(){
	. /confdia/bin/setvari
	STORE_NUMBER=`echo $NUMETIEN`

	PDV_NUMBER=`echo $PDV_NUMBER` # 01 é Master
	if [ $PDV_NUMBER -eq 01 ]; then
		TOTAL_PDVS=`echo $NUMETPVS`
	fi

	COMPANY_CODE=`echo $CODIEMPR`
	FILIAL_CODE=`echo $CODIFILIAL`
	PDV_CODE=`echo $CODIPDV`
	SAT=`echo $SAT` # 0 não tem SAT / 1 tem SAT
	TYPE_IMPR=`echo $TIPO_IMPR`

	STORE_TYPE=`echo $MASTERFRANQUICIA` # 0 para Proprias e SAN / 1 para CISS
	if [ $STORE_TYPE -eq 1 ]; then
		RETAGUARDA=`echo $NUM_FRANQUICIADO`
	fi

	IMPRCUPO=`echo $IMPRCUPO`
	CUPOPARA=`echo $CUPOPARA`
	VISOCLIE=`echo $VISOCLIE`

	RETORNO="$STORE_NUMBER""|""$PDV_NUMBER""|""$TOTAL_PDVS""|""$COMPANY_CODE""|""$FILIAL_CODE""|""$PDV_CODE""|""$SAT""|""$TYPE_IMPR""|""$STORE_TYPE""|""$RETAGUARDA""|""$IMPRCUPO""|""$CUPOPARA""|""$VISUCLIE"
	return 0
}


# Consolidando as informações e enviando para o servidor
function send_info_to_server(){
	return 0
}


# Função de inicio
function main(){
	# Execução dos programas
	get_info_hardware
	if [ $? -eq 0 ]; then
		get_info_software
		if [ $? -eq 0 ]; then
			echo "SHOP=$NUM_SHOP , CHECKOUT=$NUM_CHECKOUT, CPU=$CPU, SERIAL_CPU=$SERIAL_NUMBER_CPU, RAID=$RAID, DISK_0=$DISK_0, DISK_SIZE=$DISK_SIZE, PROCESSOR=$PROCESSOR, MEMORY_0=$MEMORY_0, MEMERY_1=$MEMORY_1, PRINTER=$PRINTER, FW_PRINTER=$FIRMWARE_PRINTER, VS_POS=$VERSION_POS, VS_BIOS=$VERSION_BIOS,VS_KERNEL=$VERSION_KERNEL"
		else
			echo "Erro ao executar função ['get_info_software']."
		fi
	else
		echo "Erro ao executar função ['get_info_hardware']."
	fi
}

main

# Export Variable
# "$NUM_SHOP""|""$NUM_CHECKOUT""|""$CPU""|""$SERIAL_NUMBER_CPU""|""$RAID""|""$DISK_0""|""$DISK_SIZE""|""$PROCESSOR""|""$MEMORY_0""|""$MEMORY_1""|""$PRINTER"
# "$VERSION_BIOS""|""$VERSION_POS""|""$VERSION_KERNEL""|""$VAR2""|""$FIRMWARE_PRINTER"
# "$STORE_NUMBER""|""$PDV_NUMBER""|""$TOTAL_PDVS""|""$COMPANY_CODE""|""$FILIAL_CODE""|""$PDV_CODE""|""$SAT""|""$TYPE_IMPR""|""$STORE_TYPE""|""$RETAGUARDA""|""$IMPRCUPO""|""$CUPOPARA""|""$VISUCLIE"

