#!/bin/bash -x
# Programa para verificar as vendas dos caixas e fita fim dia nas lojas
# 
# 
# Author: Walter Moura
# Data Criação: 04/08/2020
# Modificado por: Walter Moura
# Data Modificação: 04/08/2020
# Versão 2.0

# Definições de variaveis 'globais'
USERACTUAL=$(grep $EUID /etc/group | awk -F ":" '{print $1}')
PATH_USER="/home/$USERACTUAL/programanalisefimdia"
PATH_BKP="$PATH_USER/backup"
PATH_LOG="$PATH_USER/log"
BD="$PATH_USER/.baseDeDados.db"
ARQRET="$PATH_USER/.analise_fitas.log"
ARQNAME=".analise_fitas.log"
ARQARRAYFILES="$PATH_USER/.array_files.txt"
ARQARRAYFILESLOG="$PATH_USER/.array_files_log.txt"
LOG="$PATH_LOG/error.programanalisefimdia.log"
LOGNAME="error.programanalisefimdia.log"
ARQTEMP="$PATH_USER/.temp_list.txt"
ARQSHOPS="$PATH_USER/.list_shop.txt"
USER="root"
PASS="root"
DTATUAL="$(date +%Y%m%d)"
MYSQL_HOST="10.106.68.78"
MYSQL_BD="srvremoto"
MYSQL_USER="dba"
MYSQL_PASS=""
IMPRESSORA_LOG="$PATH_USER/.impressora.log"
FIMDIA_LOG="$PATH_USER/.fimdia.log"

function fnMenu(){

	option=$(dialog --no-cancel  --backtitle "Analise Fitas" --title "Analise Fitas - 1.0" --menu "Escolha opção: " 0 0 0 \
			01 "Valor Fita FIM DIA" \
			02 "Valor Fecho dos caixas" \
			99 "Exit\Quit" --stdout)
	case $option in
		"1"|"01")
			echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Inciando função ['fnFimDia']." >> $LOG
			fnFimDia
		;;
		"2"|"02")
			echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Inciando função ['fnFechoDosCaixas']." >> $LOG
			fnFechoDosCaixas
		;;
		"99")
			echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Saindo da aplicação." >> $LOG
			dialog --ok-label "OK" --title "Remote System" --msgbox "Saindo da aplicação." 5 70
			sleep 1
			exit
		;;
		*)
			echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Opção inválida." >> $LOG
			dialog --ok-label "OK" --title "Remote System" --msgbox "Opção inválida." 5 70
			sleep 1
			exit
		;;
	esac

}

function fnOptions(){

	SHOP=$(dialog --stdout --backtitle "Analise Fitas" --title "Analise Fitas - 1.0" --form "" 11 50 0 "Informe a loja:" 1 1 "" 1 16 6 4)
	[ "$SHOP" ] || { dialog --ok-label "OK" --title "Remote System" --msgbox "ERROR: Não foi informado a loja." 5 70 ; sleep 1 ; exit ; }

	if [ $? != 0 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):OPTIONS:Aplicação encerrada." >> $LOG
		dialog --ok-label "OK" --title "Remote System" --msgbox "Aplicação encerrada." 5 70
		sleep 1
		exit 
	fi
	LJ=`echo ${#SHOP}`
	if [ $LJ -ge 5 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):OPTIONS:Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165 ." >> $LOG
		dialog --ok-label "OK" --title "Remote System" --msgbox "Informe até 4 caracteres númericos. Ex.: 2, 1000, 9527, 165 ." 5 70
		sleep 1
		exit
	fi
	if [[ $SHOP = ?(+|-)+([0-9]) ]]; then
		echo ""
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):OPTIONS:Ops, valor informado não é númerico." >> $LOG
		dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, valor informado não é númerico." 5 70
		sleep 1
		exit
	fi
	SHOP=`printf "%05d" $SHOP`

	DATA_FILTRO=`dialog --backtitle "Analise Fitas - 1.0" --title "Calendário" --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data para gerar o relatório:" 0 0 --stdout`
	if [ $? != 0 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):OPTIONS:Aplicação encerrada." >> $LOG
		dialog --ok-label "OK" --title "Remote System" --msgbox "Aplicação encerrada." 5 70
		sleep 1
		exit 
	fi
	DT=`echo ${#DATA_FILTRO}`
	if [ $DT -ge 9 ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):OPTIONS:Informe até 8 caracteres númericos. Ex.: 20200101 ." >> $LOG
		dialog --ok-label "OK" --title "Remote System" --msgbox "Informe até 8 caracteres númericos. Ex.: 20200101 ." 5 70
		sleep 1
		exit
	fi
	if [[ $DATA_FILTRO = ?(+|-)+([0-9]) ]]; then
		echo ""
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):OPTIONS:Ops, erro nos valores informado como data." >> $LOG
		dialog --ok-label "OK" --title "Remote System" --msgbox "Ops, erro nos valores informado como data." 5 70
		sleep 1
		exit
	fi
}

function fnGetIP(){
	
	echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`mysql --connect-timeout=5 -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP'"`
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
            ip=`mysql --connect-timeout=5 -u $MYSQL_USER $MYSQL_BD -h $MYSQL_HOST -N -e "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP'"`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$ip']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Erro ao obter IP loja ['$SHOP'] problema ao acessar banco de dados MySQL." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Saindo por contigencia." >> $LOG
        EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$ip']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    fi
}

function fnFileJob(){
	echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FILEJOB:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FILEJOB:Verificamos se ['$SHOP']:['$PORT'], os arquivos estão no backup." >> $LOG
	SSH_CONFIG="-oConnectTimeout=1 -oKexAlgorithms=+diffie-hellman-group1-sha1 -oStrictHostKeyChecking=no"
	COUNT=`sshpass -p $PASS ssh $SSH_CONFIG -l$USER $ip -p$PORT "cat /var/log/impresora.log | grep ${DATA_FILTRO}" | wc -l`

	if [ $COUNT -ge 1 ]; then
		FILE_JOB="0"
		echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FILEJOB:Verificamos se ['$SHOP']:['$PORT'], arquivos não estão no backup." >> $LOG
	else
		FILE_JOB="1"
		echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:FILEJOB:Verificamos se ['$SHOP']:['$PORT'], arquivos estão no backup." >> $LOG
	fi
}

function fnFimDia(){
	fnOptions
	fnGetIP
	SSH_CONFIG="-oConnectTimeout=1 -oKexAlgorithms=+diffie-hellman-group1-sha1 -oStrictHostKeyChecking=no"
	sshpass -p $PASS ssh $SSH_CONFIG -l$USER $ip -p10001 "cat /confdia/DE/D_E_${DATA_FILTRO}*.log | grep -a ESTADO.CAIXAS.EM.FECHO.DIA -A165" > $FIMDIA_LOG
	TOTAL_FATURADO=`cat $FIMDIA_LOG | grep -a ".*TOTAIS DA CMOS ***" -A10 | grep -a -i "TOTAL FATURADO..:" | sed -e 's/\r//' | tr -d " " | cut -d ":" -f2`
	TOTAL_DINHEIRO=`cat $FIMDIA_LOG | grep -a ".*TOTAIS DA CMOS ***" -A10 | grep -a -i "   DINHEIRO   :" | sed -e 's/\r//' | tr -d " " | cut -d ":" -f2 `
	TOTAL_CARTAO=`cat $FIMDIA_LOG | grep -a ".*TOTAIS DA CMOS ***" -A10 | grep -a -i "    CARTAO     :" | sed -e 's/\r//' | tr -d " " | cut -d ":" -f2`
	NUMERO_CLIENTES=`cat $FIMDIA_LOG | grep -a ".*TOTAIS DA CMOS ***" -A30 | grep -a -i "NUMERO CLIENTES .:" | sed -e 's/\r//' | tr -d " " | cut -d ":" -f2`
	if [ -z $TOTAL_FATURADO ]; then
		TOTAL_FATURADO="0,00"
	fi
	if [ -z $TOTAL_DINHEIRO ]; then
		TOTAL_DINHEIRO="0,00"
	fi
	if [ -z $TOTAL_CARTAO ]; then
		TOTAL_CARTAO="0,00"
	fi
	if [ -z $NUMERO_CLIENTES ]; then
		NUMERO_CLIENTES="0"
	fi
	
	FORMAT_DATA=`date "+%d/%m/%Y" --date="$DATA_FILTRO"`

	TEXTO="MASTER"
	
	echo "************************************* FECHO DE CAIXA [$SHOP]:[$FORMAT_DATA] *************************************" > $ARQNAME
	printf '\n' >> $ARQNAME
	printf '+--------------------+--------------------+--------------------+--------------------+-------------------------+\n' >> $ARQNAME
	printf '|%-20s|%-20s|%-20s|%-20s|%-25s|\n' "PDV" "TOTAL FATURADO" "TOTAL DINHEIRO" "TOTAL CARTAO" "NUMERO DE CLIENTES" >> $ARQNAME
	printf '+--------------------+--------------------+--------------------+--------------------+-------------------------+\n' >> $ARQNAME
	printf '|%+20s|%+20s|%+20s|%+20s|%+25s|\n' $TEXTO $TOTAL_FATURADO $TOTAL_DINHEIRO $TOTAL_CARTAO $NUMERO_CLIENTES >> $ARQNAME
	printf '+--------------------+--------------------+--------------------+--------------------+-------------------------+\n' >> $ARQNAME

	#Convertendo para UNIX
	dos2unix $ARQNAME

	dialog --backtitle "Analise Fitas - 1.0" --title "Arquivo de Análise" --textbox $ARQNAME 25 115

}

function fnFechoDosCaixas(){
	
	fnOptions
	fnGetIP
	SUB_TOTAL_FATURADO="0.00"
	SUB_TOTAL_DINHEIRO="0.00"
	SUB_TOTAL_CARTAO="0.00"
	SUB_TOTAL_CLIENTES="0"

	SSH_CONFIG="-oConnectTimeout=1 -oKexAlgorithms=+diffie-hellman-group1-sha1 -oStrictHostKeyChecking=no"
	N_TPVS=`sshpass -p $PASS ssh $SSH_CONFIG -p10001 -l $USER $ip '. /confdia/bin/setvari; echo ${NUMETPVS}'`
	FORMAT_DATA=`date "+%d/%m/%Y" --date="$DATA_FILTRO"`
	
	echo "************************************* FECHO DE CAIXA [$SHOP]:[$FORMAT_DATA] *************************************" > $ARQNAME
	printf '\n' >> $ARQNAME
	printf '+--------------------+--------------------+--------------------+--------------------+-------------------------+\n' >> $ARQNAME
	printf '|%-20s|%-20s|%-20s|%-20s|%-25s|\n' "PDV" "TOTAL FATURADO" "TOTAL DINHEIRO" "TOTAL CARTAO" "NUMERO DE CLIENTES" >> $ARQNAME
	printf '+--------------------+--------------------+--------------------+--------------------+-------------------------+\n' >> $ARQNAME

	for x in $(seq 1 $N_TPVS);do
		PORT="1000$x"
		fnFileJob
		if [ $FILE_JOB -eq 1 ]; then 
			sshpass -p $PASS ssh $SSH_CONFIG -l$USER $ip -p$PORT "zcat /confdia/backup/impresora.log.gz.* | grep ${DATA_FILTRO}" > $IMPRESSORA_LOG 
		else
			sshpass -p $PASS ssh $SSH_CONFIG -l$USER $ip -p$PORT "cat /var/log/impresora.log | grep ${DATA_FILTRO}" > $IMPRESSORA_LOG
		fi
		TOTAL_FATURADO=`cat $IMPRESSORA_LOG | grep -a "CIERRTEF:             FECHO DE CAIXA" -A200 | grep -a "CIERRTEF:.*TOTAIS DA CMOS ***" -A10 | grep -a -i "CIERRTEF: TOTAL FATURADO..:" | cut -d ":" -f4 | tr -d " "`
		TOTAL_DINHEIRO=`cat $IMPRESSORA_LOG | grep -a "CIERRTEF:             FECHO DE CAIXA" -A200 | grep -a "CIERRTEF:.*TOTAIS DA CMOS ***" -A10 | grep -a -i "CIERRTEF:    DINHEIRO   :" | cut -d ":" -f4 | tr -d " "`
		TOTAL_CARTAO=`cat $IMPRESSORA_LOG | grep -a "CIERRTEF:             FECHO DE CAIXA" -A200 | grep -a "CIERRTEF:.*TOTAIS DA CMOS ***" -A10 | grep -a -i "CIERRTEF:    CARTAO     :" | cut -d ":" -f4 | tr -d " "`
		NUMERO_CLIENTES=`cat $IMPRESSORA_LOG | grep -a "CIERRTEF:             FECHO DE CAIXA" -A200 | grep -a "CIERRTEF:.*TOTAIS DA CMOS ***" -A30 | grep -a -i  "CIERRTEF:NUMERO CLIENTES .:"  | cut -d ":" -f4 | tr -d " "`
		if [ -z $TOTAL_FATURADO ]; then
			TOTAL_FATURADO="0,00"
		fi
		if [ -z $TOTAL_DINHEIRO ]; then
			TOTAL_DINHEIRO="0,00"
		fi
		if [ -z $TOTAL_CARTAO ]; then
			TOTAL_CARTAO="0,00"
		fi
		if [ -z $NUMERO_CLIENTES ]; then
			NUMERO_CLIENTES="0"
		fi
		TEMP_TOTAL_FATURADO=`echo $TOTAL_FATURADO | sed -e "s/,/./"`
		TEMP_TOTAL_DINHEIRO=`echo $TOTAL_DINHEIRO | sed -e "s/,/./"`
		TEMP_TOTAL_CARTAO=`echo $TOTAL_CARTAO | sed -e "s/,/./"`
		SUB_TOTAL_FATURADO=`echo "scale=2;($SUB_TOTAL_FATURADO + $TEMP_TOTAL_FATURADO)" | bc`
		SUB_TOTAL_DINHEIRO=`echo "scale=2;($SUB_TOTAL_DINHEIRO + $TEMP_TOTAL_DINHEIRO)" | bc`
		SUB_TOTAL_CARTAO=`echo "scale=2;($SUB_TOTAL_CARTAO + $TEMP_TOTAL_CARTAO)" | bc`
		SUB_TOTAL_CLIENTES=`expr $SUB_TOTAL_CLIENTES + $NUMERO_CLIENTES`
		printf '|%+20s|%+20s|%+20s|%+20s|%+25s|\n' $x $TOTAL_FATURADO $TOTAL_DINHEIRO $TOTAL_CARTAO $NUMERO_CLIENTES >> $ARQNAME
		
	done
	SUB_TOTAL_FATURADO=`echo $SUB_TOTAL_FATURADO | sed -e "s/\./,/"`
	SUB_TOTAL_DINHEIRO=`echo $SUB_TOTAL_DINHEIRO | sed -e "s/\./,/"`
	SUB_TOTAL_CARTAO=`echo $SUB_TOTAL_CARTAO | sed -e "s/\./,/"`
	TEXT="SUBTOTAL"
	
	printf '+--------------------+--------------------+--------------------+--------------------+-------------------------+\n' >> $ARQNAME
	printf '|%-20s|%+20s|%+20s|%+20s|%+25s|\n' $TEXT $SUB_TOTAL_FATURADO $SUB_TOTAL_DINHEIRO $SUB_TOTAL_CARTAO $SUB_TOTAL_CLIENTES >> $ARQNAME
	printf '+--------------------+--------------------+--------------------+--------------------+-------------------------+\n' >> $ARQNAME

	#Convertendo para UNIX
	dos2unix $ARQNAME

	dialog --backtitle "Analise Fitas - 1.0" --title "Arquivo de Análise" --textbox $ARQNAME 25 115
	
}

function main(){

	echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicio programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inciando os menus." >> $LOG
    fnMenu

}

main

